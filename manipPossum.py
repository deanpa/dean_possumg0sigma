#!/usr/bin/env python


import os
import pickle
import numpy as np
from scipy import stats
from datetime import datetime
from osgeo import osr


def decodeBytes(byteArray, nArray):
    """
    ## loop to decode byte to string
    """
    strArray = str(byteArray[0], 'utf-8')
    for i in range(1, nArray):
        strArr_i = str(byteArray[i], 'utf-8')
        strArray = np.append(strArray, strArr_i)
    return(strArray)

def formatDate(inArray):
    n = len(inArray)
    outArray = np.empty(n, dtype = datetime)
    for i in range(n):
        date_i = inArray[i]
        if date_i != '':
            outArray[i] = datetime.strptime(date_i, '%d/%m/%Y').date()
        else:
            outArray[i] = datetime(1999, 1, 1).date()
    return(outArray)


def distFX(x1, y1, x2, y2):
    """
    calc distance matrix between 2 sets of points
    resulting matrix dimensions = (len(x1), len(x2))
    """
    deltaX = np.add.outer(x1, -x2)
    deltaX_sq = deltaX**2
    deltaY = np.add.outer(y1, -y2)
    deltaY_sq = deltaY**2
    dist_2D = np.sqrt(deltaX_sq + deltaY_sq)
    return dist_2D

 

class ManipData():
    def __init__(self, gpsFname, gpsOutFname, trapFname, trapOutFname):
        """
        Object to read in  data
        """
        #############################################
        ############ Run basicdata functions
        self.readData(gpsFname, trapFname)
        self.getEastingsNorthings()
        self.cullNoData()     
        self.correctLocError()   
        self.fixTypos()
        self.fixTrapNumber()
        self.remGPSOutliers()
        self.checkGPS()
        self.makeJulianDay()
        self.writeToFileFX(gpsOutFname, trapOutFname)
        ############## End run of basicdata functions
        #############################################

    def readData(self, gpsFname, trapFname):
        """
        ## read and transfer GPS and trapping data to manipdata class
        """
        gpsDat = np.genfromtxt(gpsFname,  delimiter=',', names=True,
            dtype=['i8', 'S10', 'S32', 'S10', 'i8', 'i8', 'i8', 'S10', 'S10', 'i8', 
                'S10', 'f8', 'f8', 'i8', 'f8', 'f8', 'f8', 'f8', 'f8'])
        self.areagps = gpsDat['area']
        self.blockgps = gpsDat['block']
        self.sitegps = gpsDat['site']
        self.animalgps = gpsDat['animal']
        self.sexgps = gpsDat['sex']
        self.agegps = gpsDat['age']
        self.dategps = gpsDat['date']
        self.latgps = gpsDat['lat']
        self.longgps = gpsDat['long']
        self.xtmgps = gpsDat['nztmE']
        self.ytmgps = gpsDat['nztmN']
        self.xmggps = gpsDat['nzmgE']
        self.ymggps = gpsDat['nzmgN']
        self.nGPS = len(self.xtmgps)
        ## convert bytes to string and format Dates
        self.areagps = decodeBytes(self.areagps, self.nGPS)
        self.dategps = decodeBytes(self.dategps, self.nGPS)
        self.dategps = formatDate(self.dategps)
        self.agegps = decodeBytes(self.agegps, self.nGPS)
        self.sexgps = decodeBytes(self.sexgps, self.nGPS)
        self.blockgps = decodeBytes(self.blockgps, self.nGPS)
        ##############################################################################
        ## Read in trapping data
        trapDat = np.genfromtxt(trapFname,  delimiter=',', names=True,
            dtype=['i8', 'S32', 'S32', 'S32', 'S32', 'S32', 'S32', 'S32', 'i8', 'f8', 
                    'i8', 'f8', 'i8', 'i8', 'S32', 'S32', 'i8', 'S32', 
                'S32', 'S32', 'i8', 'S32', 'S32', 'S32',
                'f8', 'f8', 'f8', 'f8', 'f8', 'f8'])
        self.trapnumber = trapDat['trapNumber']   
        self.ntrap = len(self.trapnumber)
        self.trapnumber = decodeBytes(self.trapnumber, self.ntrap)
#        trapMask = self.trapnumber != 'NA'
        self.traptype = trapDat['trapType']     #[trapMask]
        self.setType = trapDat['setType']
        self.projecttrap = trapDat['project']   #[trapMask]
        self.sitetrap = trapDat['site']         #[trapMask]
        self.areatrap = trapDat['area']         #[trapMask]
        self.trapLayout = trapDat['trapLayout']
        self.initialtrap = trapDat['initial']
        self.nighttrap = trapDat['night']       #[trapMask]
        self.trapstatus = trapDat['status']     #[trapMask]
        self.animaltrap = trapDat['animal']     #[trapMask]
        self.sextrap = trapDat['sex']           #[trapMask]
        self.agetrap = trapDat['age']           #[trapMask]
        self.trapNightIndicator = trapDat['trapNightIndicator']
        self.inoculatedTB = trapDat['inoculatedTB']
        self.lattrap = trapDat['lat']
        self.longtrap = trapDat['long']
        self.xmgTrap = trapDat['mgE']
        self.ymgTrap = trapDat['mgN']
        self.xtmTrap = trapDat['tmE']
        self.ytmTrap = trapDat['tmN']
        self.yeartrap = trapDat['year']         #[trapMask]
        self.monthtrap = trapDat['month']       #[trapMask]
        self.daytrap = trapDat['day']           #[trapMask]
        ## convert bytes to string
        self.agetrap = decodeBytes(self.agetrap, self.ntrap)
        self.sextrap = decodeBytes(self.sextrap, self.ntrap)
        self.traptype = decodeBytes(self.traptype, self.ntrap)
        self.projecttrap = decodeBytes(self.projecttrap, self.ntrap)
        self.areatrap = decodeBytes(self.areatrap, self.ntrap)
#        self.sitetrap = decodeBytes(self.sitetrap, self.ntrap)
        self.trapLayout = decodeBytes(self.trapLayout, self.ntrap)
        self.trapstatus = decodeBytes(self.trapstatus, self.ntrap)
        self.initialtrap = decodeBytes(self.initialtrap, self.ntrap)
        self.setType = decodeBytes(self.setType, self.ntrap)
        

    def getEastingsNorthings(self):
        """
        get all locations in NZTM projection
        """
        tmHaveMask = ~np.isnan(self.xtmgps)
        mgHaveMask = ~np.isnan(self.xmggps)
        latHaveMask = ~np.isnan(self.latgps)
        ## No data mask
        self.noLocMask = ~tmHaveMask & ~mgHaveMask & ~latHaveMask
        # Lat long ref
        latlongsr = osr.SpatialReference()
        latlongsr.ImportFromEPSG(4979)          #4329)
        # nz map grid ref
        nzmgsr = osr.SpatialReference()
        nzmgsr.ImportFromEPSG(27200)
        # nz transverse mercator ref
        nztmsr = osr.SpatialReference()
        nztmsr.ImportFromEPSG(2193)
        # masks for transformations ################################
        lat2tmMask = latHaveMask & ~tmHaveMask
        mg2tmMask = mgHaveMask & ~tmHaveMask & ~latHaveMask
        # empty new arrays to populate
        self.xgps = self.xtmgps.copy()     # np.zeros(self.nGPS)
        self.ygps = self.ytmgps.copy()     # np.zeros(self.nGPS)
        # transform function lat long to nztm
        latlong2nztm = osr.CoordinateTransformation(latlongsr, nztmsr)
        # transform function map grid to nztm
        nzmg2nztm = osr.CoordinateTransformation(nzmgsr, nztmsr)
        # Loop thru points
        for i in range(self.nGPS):
            if lat2tmMask[i]:
                (self.xgps[i], self.ygps[i], z) = latlong2nztm.TransformPoint(self.longgps[i], 
                    self.latgps[i])
            elif mg2tmMask[i]:
                (self.xgps[i], self.ygps[i], z) = nzmg2nztm.TransformPoint(self.xmggps[i], self.ymggps[i])
        ############
        ##  GET TRAP LOCATION DATA
        tmHaveMask = ~np.isnan(self.xtmTrap)
        mgHaveMask = ~np.isnan(self.xmgTrap)
        latHaveMask = ~np.isnan(self.lattrap)
        ## No data mask
        self.noLocMaskTrap = ~tmHaveMask & ~mgHaveMask & ~latHaveMask
        # masks for transformations ################################
        lat2tmMask = latHaveMask & ~tmHaveMask
        mg2tmMask = mgHaveMask & ~tmHaveMask & ~latHaveMask
        # empty new arrays to populate
        self.xtrap = self.xtmTrap.copy()    #   np.zeros(self.ntrap)
        self.ytrap = self.ytmTrap.copy()    #   np.zeros(self.ntrap)
        # Loop thru points
        for i in range(self.ntrap):
            if lat2tmMask[i]:
                (self.xtrap[i], self.ytrap[i], z) = latlong2nztm.TransformPoint(self.longtrap[i], 
                    self.lattrap[i])
            elif mg2tmMask[i]:
                (self.xtrap[i], self.ytrap[i], z) = nzmg2nztm.TransformPoint(self.xmgTrap[i], 
                    self.ymgTrap[i])
#            if (i>10955) & (i<10961):
#                print('i', i, self.lattrap[i], self.longtrap[i], self.xtrap[i], self.ytrap[i])


    def cullNoData(self):
        """
        # Remove data where we don't have location data
        """
        self.xgps = self.xgps[~self.noLocMask]
        self.ygps = self.ygps[~self.noLocMask]
        self.areagps = self.areagps[~self.noLocMask]        
        self.blockgps = self.blockgps[~self.noLocMask]
        self.sitegps = self.sitegps[~self.noLocMask]
        self.animalgps = self.animalgps[~self.noLocMask]
        self.sexgps = self.sexgps[~self.noLocMask]
        self.agegps = self.agegps[~self.noLocMask]
        self.dategps = self.dategps[~self.noLocMask]
        self.nGPS = len(self.xgps)
        # get year, month and day
        self.yeargps = np.zeros(self.nGPS, dtype = int)
        self.monthgps = np.zeros(self.nGPS, dtype = int)
        self.daygps = np.zeros(self.nGPS, dtype = int)
        for i in range(self.nGPS):
            self.yeargps[i] = self.dategps[i].year
            self.monthgps[i] = self.dategps[i].month
            self.daygps[i] = self.dategps[i].day
#            if i < 100:
#                print('date', self.dategps[i], self.yeargps[i], self.monthgps[i], self.daygps[i])
        ############################################################
        ##  CULL TRAP DATA
        ## REMOVE TARGETED TRAPPING and traps without locations
        noLocTargetMask = (self.trapLayout == 'Targeted') | self.noLocMaskTrap
        ## remove one point at Aldinga of infant capture with mother
        aldingaMask = ((self.areatrap == 'Aldinga_Pilot') & (self.nighttrap == 9) &
                        (self.agetrap == 'I'))
        noLocTargetMask = noLocTargetMask | aldingaMask 
        aldingaMask = ((self.areatrap == 'Aldinga_Pilot') & (self.trapnumber == '46') &
                    (self.nighttrap == 5) & (self.agetrap == 'I'))
        noLocTargetMask = noLocTargetMask | aldingaMask 
        aldingaMask = ((self.areatrap == 'Aldinga_Pilot') & (self.trapnumber == '51') &
                    (self.nighttrap == 10) & (self.agetrap == 'I'))
        noLocTargetMask = noLocTargetMask | aldingaMask 
        aldingaMask = ((self.areatrap == 'Aldinga_Pilot') & (self.trapnumber == '66') &
                    (self.nighttrap == 2) & (self.trapstatus == 'SS'))
        noLocTargetMask = noLocTargetMask | aldingaMask 
        aldingaMask = ((self.areatrap == 'Aldinga_Pilot') & (self.trapnumber == '68') &
                    (self.nighttrap == 4) & (self.trapstatus == 'ESC'))
        noLocTargetMask = noLocTargetMask | aldingaMask 
        aldingaMask = ((self.areatrap == 'Aldinga_Pilot') & (self.trapnumber == '29') &
                    (self.nighttrap == 8) & (self.agetrap == 'I'))
        noLocTargetMask = noLocTargetMask | aldingaMask 
        



        self.trapnumber = self.trapnumber[~noLocTargetMask]
        self.traptype = self.traptype[~noLocTargetMask]
        self.setType = self.setType[~noLocTargetMask]
        self.projecttrap = self.projecttrap[~noLocTargetMask]
        self.sitetrap = self.sitetrap[~noLocTargetMask]
        self.areatrap = self.areatrap[~noLocTargetMask]
        self.trapLayout = self.trapLayout[~noLocTargetMask]
        self.initialtrap = self.initialtrap[~noLocTargetMask]
        self.nighttrap = self.nighttrap[~noLocTargetMask]
        self.trapstatus = self.trapstatus[~noLocTargetMask]
        self.animaltrap = self.animaltrap[~noLocTargetMask]
        self.sextrap = self.sextrap[~noLocTargetMask]
        self.agetrap = self.agetrap[~noLocTargetMask]
        self.trapNightIndicator = self.trapNightIndicator[~noLocTargetMask]
        self.inoculatedTB = self.inoculatedTB[~noLocTargetMask]
        self.xtrap = self.xtrap[~noLocTargetMask]
        self.ytrap = self.ytrap[~noLocTargetMask]
        self.yeartrap = self.yeartrap[~noLocTargetMask]
        self.monthtrap = self.monthtrap[~noLocTargetMask]
        self.daytrap = self.daytrap[~noLocTargetMask]
        self.ntrap = len(self.daytrap)
#        print('xy', self.xgps[:10], self.ygps[:10])


    def correctLocError(self):
        """
        swap easting and northings for Orari Gorge
        """
        mask = self.xgps > self.ygps
        newX = self.ygps[mask]
        newY = self.xgps[mask]
        self.xgps[mask] = newX
        self.ygps[mask] = newY




    def fixTrapNumber(self):
        """
        ## Trap numbers are repeated within a site, despite different x y values
        ## Make new trap numeric trap identifier
        """
        roundX = np.round(self.xtrap, 0)
        roundY = np.round(self.ytrap, 0)
        uArea = np.unique(self.areatrap)
        cc = 0
        self.trapID = np.zeros(self.ntrap, dtype = int)
        for i in range(len(uArea)):
            areaM = self.areatrap == uArea[i]
            trap_i = self.trapnumber[areaM]
            uTrap_i = np.unique(trap_i)

            print('areaM', uArea[i], 'ntraps', len(uTrap_i))

            for j in range(len(uTrap_i)):
                trap_ij = uTrap_i[j]
                trapM = self.trapnumber == trap_ij
                areaTrapM = areaM & trapM
                uX_ij = np.unique(roundX[areaTrapM])
                for k in range(len(uX_ij)):
                    x_ijk = uX_ij[k]
                    Xmask = roundX == x_ijk
                    areaTrapXMask = areaTrapM & Xmask
                    uY = np.unique(roundY[areaTrapXMask])
                    for l in range(len(uY)):
                        Ymask = roundY == uY[l]
                        areaTrapXYMask = areaTrapXMask & Ymask
#                        if np.sum(areaTrapXYMask) > 1:
#                            print('Problem area', uArea[i], 'trap', trap_ij, 'x', x_ijk, 'y',uY[l])
                        self.trapID[areaTrapXYMask] = cc
                        cc += 1


    def remGPSOutliers(self):
        """
        remove outlier location data
        """
        ## ok: 101
        # animal 106 was targeted trapping only; should be good for gps
        # 117 no trap data - good for sigma
        # 107 good gps data for sigma, but trap data is an error( need to remove)
            # change trapID 1843 to SS from 107
            # change trapID 1836 to SS from 107
        maskSS = (self.trapID == 1843) & (self.animaltrap == 107)
        mask = (self.trapID == 1836) & (self.animaltrap == 107)
        maskSS = maskSS | mask
        # 120 rem x data > 1879300.
        removeMask = (self.animalgps == 120) & (self.xgps >1879300)
        # 130 remove ygps data < 5682400 - dispersal movement.
        mask =  (self.animalgps == 130) & (self.ygps < 5682400)
        removeMask = removeMask | mask
        # 168 remove xgps data < 1408000.
        mask =  (self.animalgps == 168) & (self.xgps < 1408000)
        removeMask = removeMask | mask
        # 208 dispersing??? remove x data > 1517500.
        mask =  (self.animalgps == 208) & (self.xgps > 1517500)
        removeMask = removeMask | mask
        # 214 dispersing individual - remove all gps data; change 214 to POS ???
            # change trapID 6264 to SS from 214
        mask =  (self.animalgps == 214)
        removeMask = removeMask | mask
        mask = (self.trapID == 6264) & (self.animaltrap == 214)
        maskSS = maskSS | mask
        # 218 is OK
        # 224 is OK
        # 233 dispersal or error at end of data - remove x data < 1505400
        mask =  (self.animalgps == 233) & (self.xgps < 1505400)
        removeMask = removeMask | mask
        # 237 bimodal HR use Aldinga...
        mask =  (self.animalgps == 237) & (self.xgps > 1314000)
        removeMask = removeMask | mask
        # 238 remove xdat > 1314000 : must be error > 6 km awa
        mask =  (self.animalgps == 238) & (self.xgps > 1314000)
        removeMask = removeMask | mask
        # 239 Remove yday > 4892000 and xdat > 1311000; must be err at start and end.
        mask =  (self.animalgps == 239) & (self.ygps > 4892000)
        mask2 =  (self.animalgps == 239) & (self.xgps > 1311000)
        removeMask = removeMask | mask | mask2
        # 240 Remove xdat > 1314000 : must be error > 6 km away
        mask =  (self.animalgps == 240) & (self.xgps > 1314000)
        removeMask = removeMask | mask
        # 241 Remove ydat > 5724300 : must be error of one point.
        mask =  (self.animalgps == 241) & (self.ygps > 5724300)
        removeMask = removeMask | mask
        # 242 Remove ydat < 5726700 : either dispersal or error.
        mask =  (self.animalgps == 242) & (self.ygps < 5726700)
        removeMask = removeMask | mask
        # 243 is OK
        # 244 Remove ydat > 5729349 and xdat < 1834350 - Error in transporting
        mask =  (self.animalgps == 244) & (self.ygps > 5729349)
        mask2 =  (self.animalgps == 244) & (self.xgps < 1834350)
        removeMask = removeMask | mask | mask2
        # 245 is OK
        # 252 is OK
        # 264 is OK
        # 265 is OK
        # 266 Remove xday > 1640550 and ydat < 5285660 - outlier movements > 2 km???
        mask =  (self.animalgps == 266) & (self.ygps < 5285660)
        mask2 =  (self.animalgps == 266) & (self.xgps > 1640550)
        removeMask = removeMask | mask | mask2
        # 267 is OK
        # 268 Remove ydat > 5289000
        mask =  (self.animalgps == 268) & (self.ygps > 5289000)
        removeMask = removeMask | mask 
        # 269 is Ok
        # 270 Remove xday > 1641694 ; outlier or error
        mask =  (self.animalgps == 270) & (self.xgps > 1641694)
        removeMask = removeMask | mask 
        # 272 Remove ydat < 5334344
        mask =  (self.animalgps == 272) & (self.ygps < 5334344)
        removeMask = removeMask | mask 
        # 274 Trap data wrong!! therefore remove trap. Also remove gps xdat <1646108 and ydat>5335845
               # dispersal or error
               # change trapID 4174 from 274 to SS. good sigma data, but not capture
        mask =  (self.animalgps == 274) & (self.ygps > 5335845)
        mask2 =  (self.animalgps == 274) & (self.xgps < 1646108)
        removeMask = removeMask | mask | mask2
        mask = (self.trapID == 4174) & (self.animaltrap == 274)
        maskSS = maskSS | mask
        # 275 Remove ydat > 5325361; dispersal or error or retrieving collar.
        mask =  (self.animalgps == 275) & (self.ygps > 5325361)
        removeMask = removeMask | mask 
        # 276 Remove xdat < 1646500; error > 12 km
        mask =  (self.animalgps == 276) & (self.xgps < 1646500)
        removeMask = removeMask | mask 
        # 277 Remove xdat < 1651840; error
        mask =  (self.animalgps == 277) & (self.xgps < 1651840)
        removeMask = removeMask | mask 
        # 278 Remove xdat < 1637850 AND xday > 1641550; errors
        mask =  (self.animalgps == 278) & (self.xgps > 1641550)
        mask2 =  (self.animalgps == 278) & (self.xgps < 1637850)
        removeMask = removeMask | mask | mask2
        # 279 remove ydat > 5325300; error
        mask =  (self.animalgps == 279) & (self.ygps > 5325300)
        removeMask = removeMask | mask 
        # 280 remove xdat < 1642460; error
        mask =  (self.animalgps == 280) & (self.xgps < 1642460)
        removeMask = removeMask | mask 
        # 283 is OK
        # 284 is OK
        # 287 Remove ydat > 4846134
        mask =  (self.animalgps == 287) & (self.ygps < 4846134)
        removeMask = removeMask | mask 
        # 290 Remove ydat > 4844706
        mask =  (self.animalgps == 290) & (self.ygps > 4844706)
        removeMask = removeMask | mask 
        # 291 change trapID 745 to animalTrap not 291 ('POS' or nan or SS). Has another trap!!!
        mask = (self.trapID == 745) & (self.animaltrap == 291)
        maskSS = maskSS | mask
        # 293 Remove ydat > 4845377; big error
        mask =  (self.animalgps == 293) & (self.ygps > 4845377)
        removeMask = removeMask | mask 
        # 297 Change trapID 4521 to SS and not animalTrap 297... Error. Has another trap!!!
        mask = (self.trapID == 4521) & (self.animaltrap == 297)
        maskSS = maskSS | mask
        # 298 is OK
        # 299 trapID or location has an error. Change trapID 4002 to SS away from 299. This poss 
            # doesn't have a trap.
        mask = (self.trapID == 4002) & (self.animaltrap == 299)
        maskSS = maskSS | mask
        # 300 TrapID or location has an error. Change trapID 4036 to SS away from 300. This poss
            # doesn't have a trap.
        mask = (self.trapID == 4036) & (self.animaltrap == 300)
        maskSS = maskSS | mask
        # 301 TrapID or location has an error. Change trapID 3961 to SS away from 301. This poss
            # doesn't have a trap.
        mask = (self.trapID == 3961) & (self.animaltrap == 301)
        maskSS = maskSS | mask
        # 306 Remove ydat > 5419000; retrieval error.
        mask =  (self.animalgps == 306) & (self.ygps > 5419000)
        removeMask = removeMask | mask 
#        # 308 Remove animal - very small home range -
#        mask =  (self.animalgps == 308)
#        removeMask = removeMask | mask 
        ## Fix trap status and animaltrap arrays
        self.trapstatus[maskSS] = 'SS'
        self.animaltrap[maskSS] = np.nan
        ## Fix GPS data
        self.xgps = self.xgps[~removeMask]
        self.ygps = self.ygps[~removeMask]
        self.areagps = self.areagps[~removeMask]        
        self.blockgps = self.blockgps[~removeMask]
        self.sitegps = self.sitegps[~removeMask]
        self.animalgps = self.animalgps[~removeMask]
        self.sexgps = self.sexgps[~removeMask]
        self.agegps = self.agegps[~removeMask]
        self.dategps = self.dategps[~removeMask]
        self.nGPS = len(self.xgps)
        self.yeargps = self.yeargps[~removeMask]
        self.monthgps = self.monthgps[~removeMask]
        self.daygps = self.daygps[~removeMask]



    def checkGPS(self):
        """
        ## look for gps outliers
        """
        uAnimal = np.unique(self.animalgps)
        o1000 = []
        for i in range(len(uAnimal)):
            ## get trap info for animal i
            trapAniMask = self.animaltrap == uAnimal[i]
            trapx = self.xtrap[trapAniMask]
            trapy = self.ytrap[trapAniMask]

            # get gps data
            animalMask = self.animalgps == uAnimal[i]
            x = self.xgps[animalMask]
            y = self.ygps[animalMask]
            d = distFX(trapx, trapy, x, y)
#            d = d[np.triu_indices(nx, k = 1)]
            d200 = len(d[d>200])
            d400 = len(d[d>400])
            d600 = len(d[d>600])
            d800 = len(d[d>800])
            d1000 = len(d[d>1000])
#            print('uAni', uAnimal[i], 'shp', np.shape(d) , d200, d400, d600, d800, d1000)
            if d1000 > 0:
                o1000 = np.append(o1000, uAnimal[i])
        print('o1000', o1000)


    def fixTypos(self):
        """
        fix typos in raw trap data
        """
        self.trapstatus = np.where(self.trapstatus == 'POS ', 'POS', self.trapstatus)
        self.trapstatus = np.where(self.trapstatus == 'POS RECAP ', 'POS RECAP', self.trapstatus)
        self.trapLayout = np.where(self.trapLayout == 'Random ', 'Random', self.trapLayout)
        mask = (self.trapstatus == 'POS RECAP') & (np.isnan(self.animaltrap))
        self.trapstatus[mask] = 'POS'
        self.trapstatus = np.where(self.trapstatus == 'NOT SET ', 'Not set', self.trapstatus)
        self.trapstatus = np.where(self.trapstatus == 'SP ', 'SP', self.trapstatus)
        mask = (self.setType == 'Ground?') | (self.setType == 'Ground ')
        self.setType[mask] = 'Ground'
        self.traptype = np.where(self.traptype == 'Leghold ', 'Leghold', self.traptype)
        self.initialtrap = np.where(self.initialtrap == 'TrapOut ', 'TrapOut', self.initialtrap)
        self.areatrap = np.where(self.areatrap == 'KmwaVaccinatedBuffer ', 'KmwaVaccinatedBuffer',
            self.areatrap)
        self.areatrap = np.where(self.areatrap == 'Leader Valley', 'Leader',
            self.areatrap)
        self.areatrap = np.where(self.areatrap == 'PuhiPuhi Peaks ', 'Puhi Puhi Peaks',
            self.areatrap)
        self.areatrap = np.where(self.areatrap == 'Waitiki Hut', 'Waikiti Hut',
            self.areatrap)
        self.areatrap = np.where(self.areatrap == 'Catlins ', 'Catlins',
            self.areatrap)
        self.areatrap = np.where(self.areatrap == 'McQueens Valley ', 'McQueens Valley',
            self.areatrap)
        self.areatrap = np.where(self.areatrap == 'Muzzle ', 'Muzzle',
            self.areatrap)
        dayMask = (self.monthtrap == 4) & (self.daytrap == 31)
        self.daytrap[dayMask] = 30
        mask = ((self.areatrap == 'KmwaNonvaccinatedBuffer') &
            (self.trapnumber == '2C70') & (self.daytrap == 24) & (self.nighttrap == 2))
        self.daytrap[mask] = 23
        mask = ((self.areatrap == 'Claverly') &
            (self.trapnumber == 'T067') & (self.daytrap == 3) & (self.nighttrap == 4))
        self.daytrap[mask] = 2
        mask = ((self.areatrap == 'Claverly') &
            (self.trapnumber == 'T068') & (self.nighttrap == 3))
        self.monthtrap[mask] = 5
        self.daytrap[mask] = 1
        mask = ((self.areatrap == 'Claverly') &
            (self.trapnumber == 'T068') & (self.nighttrap == 4))
        self.monthtrap[mask] = 5
        self.daytrap[mask] = 2

        mask = ((self.areatrap == 'Claverly') &
            (self.trapnumber == 'T069') & (self.nighttrap == 3))
        self.monthtrap[mask] = 5
        self.daytrap[mask] = 1

        mask = ((self.areatrap == 'Claverly') &
            (self.trapnumber == 'T069') & (self.nighttrap == 4))
        self.monthtrap[mask] = 5
        self.daytrap[mask] = 2

        mask = ((self.areatrap == 'Claverly') &
            (self.trapnumber == 'T070') & (self.nighttrap == 3))
        self.monthtrap[mask] = 5
        self.daytrap[mask] = 1

        mask = ((self.areatrap == 'Claverly') &
            (self.trapnumber == 'T070') & (self.nighttrap == 4))
        self.monthtrap[mask] = 5
        self.daytrap[mask] = 2

        mask = ((self.areatrap == 'Claverly') &
            (self.trapnumber == 'T071') & (self.nighttrap == 3))
        self.monthtrap[mask] = 5
        self.daytrap[mask] = 1

        mask = ((self.areatrap == 'Claverly') &
            (self.trapnumber == 'T071') & (self.nighttrap == 4))
        self.monthtrap[mask] = 5
        self.daytrap[mask] = 2

        mask = ((self.areatrap == 'Claverly') &
            (self.trapnumber == 'T072') & (self.nighttrap == 3))
        self.monthtrap[mask] = 5
        self.daytrap[mask] = 1

        mask = ((self.areatrap == 'Claverly') &
            (self.trapnumber == 'T072') & (self.nighttrap == 4))
        self.monthtrap[mask] = 5
        self.daytrap[mask] = 2







        ### GPS data typo fix
        self.agegps = np.where((self.agegps == 'A '), 'A', self.agegps)
        self.areagps = np.where(self.areagps == 'KmwaVaccinatedBuffer ', 'KmwaVaccinatedBuffer',
            self.areagps)

    def makeJulianDay(self):
        """
        ## make julian day for trap data
        """
#        self.datetrap = np.empty(self.ntrap, dtype = datetime)
        self.julianDay = np.zeros(self.ntrap, dtype = int)
        ## make min Date 1 Jan 2007
        minDate = datetime(2007, 1, 1).date()
        ## loop thru all trap entries
        for i in range(self.ntrap):
            date_i = datetime(int(self.yeartrap[i]), int(self.monthtrap[i]), 
                int(self.daytrap[i])).date()
            dateDiff = date_i - minDate
            self.julianDay[i] = dateDiff.days
        ## LOOP THRU AGAIN TO FIX DATE ERROR - SOME TRAPS HAVE SAME DATE ACROSS NIGHTS
        uniqueTrap = np.unique(self.trapID)
        nUniqueTrap = len(uniqueTrap)
        for i in range(nUniqueTrap):
            ## TRAP i MASK
            tmask = self.trapID == uniqueTrap[i]
            ## NIGHT MASK FOR TRAP i
            nights_i = self.nighttrap[tmask]
            uNights = np.unique(nights_i)
            ## FIRST NIGHT FOR TRAP i
            refJul = np.min(self.julianDay[tmask])

            ntMask0  = (self.nighttrap == uNights[0]) & tmask

            if np.sum(ntMask0) > 1:
                print('long mask', 'i', i, 'trap', uniqueTrap[i], 
                    'NTRAP', np.sum(ntMask0), 'area', self.areatrap[ntMask0], 
                    'day', self.daytrap[ntMask0])


            prevDate = datetime(self.yeartrap[ntMask0], self.monthtrap[ntMask0],
                    self.daytrap[ntMask0]).date()
            for j in range(1, len(uNights)):
                nmask = self.nighttrap == uNights[j]
                ntmask = tmask & nmask
                date_i = datetime(self.yeartrap[ntmask], self.monthtrap[ntmask],
                    self.daytrap[ntmask]).date()
                if np.in1d(date_i, prevDate):
                    self.julianDay[ntmask] = refJul + 1
                prevDate = np.append(prevDate, date_i)
                refJul = self.julianDay[ntmask]





    def writeToFileFX(self, gpsOutFname, trapOutFname):
        """
        # Write result table to file
        """
        # create new structured array with columns of different types
        structured = np.empty((self.nGPS,), dtype=[('areagps', 'U32'), ('blockgps', 'U12'), 
                    ('sitegps', 'i8'),
                    ('animalgps', 'i8'), ('sexgps', 'U12'), ('agegps', 'U12'), 
                    ('dategps', 'U12'),
                    ('yeargps', 'i8'), ('monthgps', 'i8'), ('daygps', 'i8'), ('xgps', np.float),
                    ('ygps', np.float)])
        # copy data over
        structured['areagps'] = self.areagps
        structured['blockgps'] = self.blockgps
        structured['sitegps'] = self.sitegps
        structured['animalgps'] = self.animalgps
        structured['sexgps'] = self.sexgps
        structured['agegps'] = self.agegps
        structured['dategps'] = self.dategps
        structured['yeargps'] = self.yeargps
        structured['monthgps'] = self.monthgps
        structured['daygps'] = self.daygps
        structured['xgps'] = self.xgps
        structured['ygps'] = self.ygps
        fmts = ['%s', '%s', '%i', '%i', '%s', '%s', '%s','%i', '%i', 
                    '%i', '%.4f', '%.4f']
        np.savetxt(gpsOutFname, structured, comments = '', delimiter = ',', fmt = fmts,
                    header='areagps, blockgps, sitegps, animalgps, sexgps, agegps, dategps, yeargps, monthgps, daygps, xgps, ygps')
        ###############################################################################
        ## SAVE TRAP TEXT FILE TO DIRECTORY
        structured = np.empty((self.ntrap,), dtype=[('project', 'U32'), ('site', 'U32'),
            ('area', 'U32'), ('trapid', 'i8'), ('trapnumber', 'U32'), ('traptype', 'U32'), 
            ('setType', 'U32'),
            ('layout', 'U32'), ('initial', 'U32'), ('night', 'i8'), ('status', 'U32'),
            ('animal', 'f8'), ('sex', 'U32'), ('age', 'U32'), ('trapNightIndicator', 'i8'),
            ('inoculated', 'i8'), ('xtm', np.float), ('ytm', np.float), ('year', 'i8'),
            ('month', 'i8'), ('day', 'i8'), ('julianDay', 'i8')]) 

        structured['project'] = self.projecttrap 
        structured['site'] = self.sitetrap
        structured['area'] = self.areatrap
        structured['trapid'] = self.trapID
        structured['trapnumber'] = self.trapnumber
        structured['traptype'] = self.traptype 
        structured['setType'] = self.setType 
        structured['layout'] = self.trapLayout 
        structured['initial'] = self.initialtrap
        structured['night'] = self.nighttrap 
        structured['status'] = self.trapstatus 
        structured['animal'] = self.animaltrap 
        structured['sex'] = self.sextrap 
        structured['age'] = self.agetrap 
        structured['trapNightIndicator'] = self.trapNightIndicator 
        structured['inoculated'] = self.inoculatedTB 
        structured['xtm'] = self.xtrap 
        structured['ytm'] = self.ytrap 
        structured['year'] = self.yeartrap 
        structured['month'] = self.monthtrap 
        structured['day'] = self.daytrap 
        structured['julianDay'] = self.julianDay
        fmts = ['%s', '%s', '%s', '%i', '%s', '%s', '%s', '%s', '%s', '%i', '%s', '%.0f', '%s', '%s', 
                '%i', '%i', '%.4f', '%.4f', '%i','%i', '%i', '%i']
        np.savetxt(trapOutFname, structured, comments = '', delimiter = ',', fmt = fmts,
            header='projecttrapp, sitetrap, areatrap, trapID, trapnumber, traptype, setType, trapLayout, initialtrap, nighttrap, trapstatus, animaltrap, sextrap, agetrap, tnIndicator, inoculatedTB, xtrap, ytrap, yeartrap, monthtrap, daytrap, julianDay')

        


######################
# Main function
def main():
    # set paths to scripts and data
    inputDataPath = os.path.join(os.getenv('POFPROJDIR', default = '.'), 
            'possumg0sigma', 'g0sigmaData')
    outputDataPath = os.path.join(os.getenv('POFPROJDIR', default = '.'), 
            'possumg0sigma','g0sigmaData')
#    if not os.path.isdir(outputDataPath):
#        os.mkdir(outputDataPath)

    ## set Data names
    gpsFname = os.path.join(inputDataPath, 'MASTERFILE_Movement_Cut.csv')
    gpsOutFname = os.path.join(inputDataPath, 'moveData.csv')
    trapFname = os.path.join(inputDataPath, 'MASTERFILE_Trap_Cut2.csv')
    trapOutFname = os.path.join(inputDataPath, 'trapData.csv')

    manipdata = ManipData(gpsFname, gpsOutFname, trapFname, trapOutFname)



if __name__ == '__main__':
    main()





