#!/usr/bin/env python

########################################
########################################
# This file is part of TBfree funded project to quantify g0 and
# sigma parameters for possums across diverse habitats
# Copyright (C) 2016 Dean Anderson 
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# ADDITIONAL NOTES ABOUT THIS SCRIPT.
########################################
########################################

import os
import pickle
#import initiateModel
#import mcmcRun
import numpy as np


class ModelParams(object):
    """
    Contains the parameters for the mcmc run. This object is also required 
    for the pre-processing. 
    """
    def __init__(self):
        ############################################################
        ###################################### USER MODIFY HERE ONLY
        #################################
        
        ###################################################
        ## Set number of MCMC iterations, thin rate and burn in
        self.ngibbs = 3000   # 3000   #3000  # number of estimates to save for each parameter
        self.thinrate = 30   # 30   #30   # 200      # thin rate
        self.burnin = 8000          # burn in number of iterations

        ## If have not initiated parameters, set to 'True'   
        self.firstRun = True    ## True or False         
        print('First Run:', self.firstRun)

        ## Model number
        self.modelID = 26

        ###################################################


        ## array of iteration id to keep
        self.keepseq = np.arange(self.burnin, ((self.ngibbs * self.thinrate) + self.burnin),
            self.thinrate)

        # set paths to scripts and data
        self.inputDataPath = os.path.join(os.getenv('POFPROJDIR', default = '.'), 
            'possumg0sigma/g0sigmaData')
        self.outputDataPath = os.path.join(os.getenv('POFPROJDIR', default = '.'), 
            'possumg0sigma/mod26_Results')

        ## set Data names
        self.basicdataFname = os.path.join(self.outputDataPath, 'basicdataMod26.pkl')
        self.gibbsFname = os.path.join(self.outputDataPath, 'gibbsMod26.pkl')
        self.paramsResFname = os.path.join(self.outputDataPath, 'parameterTableMod26.csv')
        self.sigG0TableFname = os.path.join(self.outputDataPath, 'goSigmaTableMod26.csv')
        self.densitySigPlotFname = os.path.join(self.outputDataPath, 'densitySigPlot.png')
        self.individAppendixFname = os.path.join(self.outputDataPath, 'individAppendix.csv')
        self.g0SigPlotFname = os.path.join(self.outputDataPath, 'g0SigPlot.png')
        self.onePossSseFname = os.path.join(self.outputDataPath, 'onePossSsePlot.png')
        self.onePossTrapPCaptFname = os.path.join(self.outputDataPath, 'onePossTrapPCaptPlot.png')
        self.individHRCentreFname = os.path.join(self.outputDataPath, 'individHRCentre.csv')

        ## Input data
        self.inputGPSFname = os.path.join(self.inputDataPath, 'moveData.csv')
        self.inputTrapFname = os.path.join(self.inputDataPath, 'trapData.csv')
        self.inputHabitatFname = os.path.join(self.inputDataPath, 'areaHabitats.csv')
        self.inputHRHabitatsFname = os.path.join(self.inputDataPath, 'HR_HabitatCovar.csv')

        print('params basic path', self.basicdataFname) 
        print('ngibbs', self.ngibbs, 'thin', self.thinrate, 'burnin', self.burnin)

        ## Could add in a data dictionary for defining covariates - later
        
        ## Set initial parameter values
        ### sigma parameters: intercept, grass, NO AGE and SEX (female, juvenile) 
        self.b = np.array([5.0])  # parameters on sigma
#        self.b = np.array([3.8, 1.0, -1.0, -0.5,  0.5])  # parameters on sigma
        self.nBeta = len(self.b)

        self.noAgeSex = True

        # alpha para: intercept (cage), raised leg, ground leg
        self.alpha = np.array([4.0, -1.5, -0.2, 0.2])           # alpha para for a0
        self.nAlpha = len(self.alpha)            

        # prev capt parameter - tau
        self.tau = 1.7
        self.tauPriors = np.array([9.3333, 0.12])     # gamma priors
        self.tauSearch = 0.08    

        self.D = np.array([0.5, 1.5, 0.5, 7.0, 1.4, 5., 1.5, 2.5, 0.75,
                0.5, 0.75, 9.0, 5.0, .2, 7.0, 7.0, 7.0, 8.0])
        self.E = 50.0        # intial variance on sigma prediction

        ## Sigma Covariate Priors
        self.b_Priors = np.array([0.0, 10])
        self.searchBeta = 0.07
        self.alphaMeanPriors = [0.0, 0.0, 0.0, 0.0]
        self.alphaSDPriors = [10.0, 10.0, 10.0, 10.0]
#        self.alphaPriors = [0.0, 10.0]
#        self.searchAlpha = 0.03    # [116.0, 35.0, 35.0]

        ## COVARIANCE MATRIX FOR MULTIVARIATE NORMAL JUMP DISTRIBUTION
        self.alphaCmat = 0.01 * np.array([[ 1. , -0.5,  0., 0. ],
                                       [-0.5,  1. ,  0., 0.],
                                       [ 0. ,  0., 1 , 0],
                                       [ 0. ,  0., 0., 1]])
        self.searchSigma = 0.3
        self.E_Priors = [.01, 100.]                 # gamma priors on sigma error
        ###
        self.searchDelta = 0.5
        self.deltaPriors = [0.0, np.sqrt(0.3)]

        ## Density GAMMA priors by area
        self.D_Priors = np.array([[3.0, 10.0], [4.0, 0.5], [3.0, 10.0], [18.5, 0.4],
            [4.0, 0.5], [11.0, 0.5], [4.0, 0.5], [6.0, 0.5], [4.33, 0.3], [2.2, 0.5],
            [11.0, 0.5], [23.5, 0.4], [11.0, 0.5], [3.0, 10.0], [18.5, 0.4], 
            [18.5, 0.4], [18.5, 0.4], [18.5, 0.4]])
        ## DENSITY NORMAL PRIORS
        self.dNormPrior = np.array([[0.5, 0.75], [1.5, 0.5], [0.5, 0.75], [7.0, 0.2],
            [1.4, 0.6], [5.0, 0.2], [1.5, 0.5], [2.5, 0.4], [0.75, 0.75], [0.5, 0.75],
            [0.75, 0.75], [9.0, .2], [5.0, 0.2], [0.2, 1.0], [7.0, .2], 
            [7.0, 0.2], [7.0, 0.2], [8.0, .2]])
        ## MAX SIGMA FOR MAKING BLANK RASTERS FOR CALCULATING DENSITY
        self.maxSigma = 340 
        
        ## INITIAL AREA SIGMAS - FOR MAKING AREA RASTERS
        self.areaSigma = np.array([160,111,232,78,110,53,62,109,135,162,69,58,175,
            228,88,118,76,77])

        



        # minimum number of gps fixes per individual
        self.minNumber = 12
        self.maxTrapDist = 1000.0  #550.0        
        # min time between 'independent trapping events'
        self.minTimeIndep = 20      # 2.5 weeks
        ## PROPORTION OF UNOBSERVED INDIVID TO UPDATE
        self.propPresAbs = 0.20
        #################################
        ####################################### END USER MODIFICATION
        ##############################################################

