#!/bin/bash

#SBATCH --job-name=g0_mod1
#SBATCH --account=landcare00045 
#SBATCH --mail-type=end
#SBATCH --mail-user=deanpa@gmail.com
#SBATCH --time=23:00:00

#SBATCH --mem-per-cpu=3000  
#SBATCH --cpus-per-task=1

module load Python-Geo

./startMod1.py