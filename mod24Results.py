#!/usr/bin/env python


from paramsMod24 import ModelParams
import numpy as np
import pylab as P
from scipy.stats.mstats import mquantiles
import prettytable
import pickle
import os
#import datetime
#from numba import jit
import empiricalResults

def main():
    params = ModelParams()
    print('########################')
    print('########################')
    print('###')
    print('#    Model 24')
    print('###')
    print('########################')
    print('########################')

    # paths and data to read in
    possumpath = os.path.join(os.getenv('POFPROJDIR', default = '.'), 
            'possumg0sigma/mod24_Results')

#    inputGibbs = os.path.join(predatorpath, 'out_gibbs.pkl')
    fileobj = open(params.gibbsFname, 'rb')
    gibbsobj = pickle.load(fileobj)
    fileobj.close()
    print('gibbsResults Name:', params.gibbsFname)    

    resultsobj = empiricalResults.ResultsProcessing(gibbsobj, params, possumpath)

    resultsobj.makeTableFX()

#    resultsobj.tracePlotFX()

#    resultsobj.traceDeltaPlot()

#    resultsobj.traceDensityPlot()

    resultsobj.writeToFileFX()

    resultsobj.siteTableFX()

    resultsobj.plotDensitySigma()

    resultsobj.plotG0_Sigma()

    resultsobj.onePossSensitivity()

##  Don't use
######    resultsobj.onePossTrapPCapt()

if __name__ == '__main__':
    main()


