#!/usr/bin/env python



import os
import pickle
import numpy as np
from scipy import stats
import pylab as P

def logit(x):
    return np.log(x) - np.log(1 - x)

def inv_logit(x):
    return np.exp(x) / (1 + np.exp(x))


r = .025
b = np.array([-1., -.18])


def plotfx(b):
    """
    ## plot relationship between g0 and sigma
    """
    n = [10, 10, 11, 10]
    meanSig = [32.0, 40.0, 71.0, 69.0]
    sdSig = 5.0
    randSig = np.empty(0)
    for i in range(len(n)):
        rr = np.random.normal(meanSig[i], sdSig, size = n[i])
        randSig = np.append(randSig, rr)
    print('randsig', randSig)
    sigmaDom = np.arange(30., 200.) / 10
    lg0 = np.random.normal(b[0] + (b[1] * randSig / 10.), .25)
#    print('b', b)
    g0 = inv_logit(lg0)
#    g0 = np.exp(-(sigmaDom * r))
#    print('g0', g0)
    P.figure()
    P.plot(randSig, g0, 'ko')
    P.ylabel('Estimated g0', fontsize = 14)
    P.xlabel('Estimated sigma', fontsize = 14)
    figFname = 'kaimanaSigG0.png'
    P.savefig(figFname, format='png', dpi = 1000)
    P.show()

    ii = np.arange(np.sum(n))
    sigG0Results = np.zeros((2, 4), dtype = float)
    for i in range(2):
        if i == 0:
            samps = np.arange(20)
        else:
            samps = np.arange(20, 41)
        mask = np.in1d(ii, samps)
        print('samps', samps)
        meansig = np.mean(randSig[mask])
        print('meansig', meansig, 'randmask', randSig[mask], 'mask', mask )
        stdsig = np.std(randSig[mask])
        meanG0 = np.mean(g0[mask])
        stdG0 = np.std(g0[mask])
        sigG0Results[i, 0] = meansig
        sigG0Results[i, 1] = stdsig
        sigG0Results[i, 2] = meanG0
        sigG0Results[i, 3] = stdG0
    print('sigG0Results', sigG0Results)






######################
# Main function
def main():

    plotfx(b)




if __name__ == '__main__':
    main()

