#!/usr/bin/env python

########################################
########################################
# This file is part of OSPRI possum g0 and sigma project
# Copyright (C) 2018 Dean Anderson 
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# ADDITIONAL NOTES ABOUT THIS SCRIPT.
########################################
########################################

### Import modules: ###
import os
from scipy import stats
from scipy.special import gammaln
from scipy.special import gamma
import numpy as np
from numba import jit
import pickle
from copy import deepcopy

def gamma_pdf(xx, shape, scale):
    gampdf = 1.0 / (scale**shape) / gamma(shape) * xx**(shape - 1) * np.exp(-(xx/scale))
    return gampdf


def logit(x):
    return np.log(x) - np.log(1 - x)

def inv_logit(x):
    return np.exp(x) / (1 + np.exp(x))



class MCMC(object):
    def __init__(self, params, basicdata):

        self.basicdata = basicdata
        self.params = params

        self.bgibbs = np.zeros((self.params.ngibbs, self.params.nBeta))             # beta parameters
        self.agibbs = np.zeros((self.params.ngibbs, self.params.nAlpha))
        self.siggibbs = np.zeros((self.params.ngibbs, self.basicdata.nUniqueAnimal))                                # home range decay
        self.Dgibbs = np.zeros((self.params.ngibbs, self.basicdata.nUniqueArea))        
        self.Egibbs = np.zeros(self.params.ngibbs)
        self.tgibbs = np.zeros(self.params.ngibbs)
#        self.tgibbs = np.zeros((self.params.ngibbs, len(self.basicdata.tau)))
        self.deltagibbs = np.zeros((self.params.ngibbs, self.basicdata.nCaptureInd)) 
        self.dicgibbs = np.zeros(self.params.ngibbs)
#        self.k = np.zeros(self.basicdata.nUniqueAnimal)
#        self.k = np.exp(np.dot(self.basicdata.sigmaCovar, self.basicdata.b))

        print('alpha Priors', self.params.alphaMeanPriors, self.params.alphaSDPriors)        


        ## run mcmcFX - gibbs loop
        self.mcmcFX()



    def sig_UpdateFX(self):
        """
        update the latent sigma parameter across individuals.
        """
        ## SIGMA BY INDIVIDUAL
        sigma_s = np.exp(np.random.normal(np.log(self.basicdata.sigma), 
                self.params.searchSigma))
        ## DIC VALUE FOR ITERATION G OF MCMC
        self.dic_g = 0.0
        ## LOOP THROUGH UNIQUE COLLARED POSSUMS
        cc = 0
        for i in range(self.basicdata.nUniqueAnimal):
            ## SET llCapt TO ZERO IN CASE INDIVID NOT CAPTURED
            llCapt = 0.0
            llCapt_s = 0.0
            prior = stats.norm.logpdf(np.log(self.basicdata.sigma[i]), 
                np.log(self.basicdata.predSigma[i]), self.basicdata.E) 
            prior_s = stats.norm.logpdf(np.log(sigma_s[i]), 
                np.log(self.basicdata.predSigma[i]), self.basicdata.E)
            llik = (stats.norm.logpdf(self.basicdata.deltaXHRC[i], 0.0,
                self.basicdata.sigma[i]) +
                stats.norm.logpdf(self.basicdata.deltaYHRC[i], 0.0,
                self.basicdata.sigma[i]))
            llik_s = (stats.norm.logpdf(self.basicdata.deltaXHRC[i], 0.0,
                sigma_s[i]) + stats.norm.logpdf(self.basicdata.deltaYHRC[i], 0.0,
                sigma_s[i]))

            # calc capt likelihood if have captured individual
            if self.basicdata.moveCaptMask[i]:
                sig2_s = (sigma_s[i])**2
                logsigma_s = np.log(sigma_s[i])
                # lambda0
                if (self.params.modelID >= 20):
                    lambda0_s = inv_logit(self.basicdata.alpha[0] + 
                        (logsigma_s * self.basicdata.alpha[1]) + 
                        (self.basicdata.raisedLegList[cc] * self.basicdata.alpha[2]) + 
                        (self.basicdata.groundLegList[cc] * self.basicdata.alpha[3]) +
                        self.basicdata.delta[cc])
                elif (self.params.modelID == 3) | (self.params.modelID == 6):
                    lambda0_s = inv_logit(self.basicdata.alpha[0] + 
                        (logsigma_s * self.basicdata.alpha[1]) + 
                        (self.basicdata.raisedLegList[cc] * self.basicdata.alpha[2]) + 
                        self.basicdata.delta[cc])
                elif (self.params.modelID == 4) | (self.params.modelID == 5):
                    lambda0_s = inv_logit(self.basicdata.alpha[0] + 
                        (logsigma_s * self.basicdata.alpha[1]) + 
                        self.basicdata.delta[cc])
                ## 2-d prob of capture, ind, trap and night
                pcapt_s = (lambda0_s * 
                    np.exp(-self.basicdata.distByIndividList[cc] / 2.0 / sig2_s))

####                pcapt_s = (lambda0_s * 
####                    np.exp(-self.basicdata.distByIndividList[cc] / 2.0 / sig2_s) * 
####                    self.basicdata.statusList[cc] ) 

                self.basicdata.preTauPCAPT_s[cc] = pcapt_s
                ## STATUS AFTER TAU EFFECT



                pcapt_s = (1 - 
                    (1- (pcapt_s**(self.basicdata.tau * 
                    self.basicdata.prevCaptList[cc])) *
                    (pcapt_s**(1.0 - self.basicdata.prevCaptList[cc])))**
                    self.basicdata.statusList[cc])
#                pcapt_s = (1 - 
#                    (1- (pcapt_s**(self.basicdata.tau[self.basicdata.tauIndxList[cc]] * 
#                    self.basicdata.prevCaptList[cc])) *
#                    (pcapt_s**(1.0 - self.basicdata.prevCaptList[cc])))**
#                    self.basicdata.statusList[cc])




####                pcapt_s = (self.basicdata.statusList[cc] *
####                    (pcapt_s**(self.basicdata.tau * self.basicdata.prevCaptList[cc])) *
####                    (pcapt_s**(1.0 - self.basicdata.prevCaptList[cc])))

                self.basicdata.pCaptList_s[cc] = pcapt_s  

                ## GET MULTINOMIAL PROBABILITIES
                # Total prob TP
                TP = np.expand_dims(1.0 - np.prod((1.0 - pcapt_s), axis = 1), 1)
                # Normalised probabilities
                sumPCapt = np.sum(pcapt_s, axis =1)
                sumPCapt = np.where(sumPCapt < 1E-280, 1E-280, sumPCapt)
                # sum to one
                NP = pcapt_s / np.expand_dims(sumPCapt, 1)
                # Multinomial probabilities
                MNP = NP * TP
                # Prob of non-capture
                pNC = 1.0 - TP
                # populate array
                self.basicdata.MNP_s[cc][:, :-1] = MNP
                self.basicdata.MNP_s[cc][:, -1] = pNC[:, 0]
                # probs of captures
                captProbs = self.basicdata.MNP_s[cc][self.basicdata.captDataList[cc] == 1]
                # fix for super low probabilities - poor parameter values.
                captProbs = np.where(captProbs < 1E-280, 1E-280, captProbs)
                # proposed likelihood
                llCapt = self.basicdata.alphaLPMF[cc]
                llCapt_s = np.sum(np.log(captProbs))
                self.basicdata.alphaLPMF_s[cc]= llCapt_s
                cc += 1
            ## CALC IMPORTANCE RATIO 
            pnow = np.sum(llik) + llCapt + prior
            pnew = np.sum(llik_s) + llCapt_s + prior_s
            pdiff = pnew - pnow
            if pdiff > 1.0:
                rValue = 1.0
                zValue = 0.0
            elif pdiff < -12.0:
                rValue = 0.0
                zValue = 1.0
            else:
                rValue = np.exp(pdiff)        # calc importance ratio
                zValue = np.random.uniform(0.0, 1.0, size = None)
            if (rValue > zValue):
                self.basicdata.sigma[i] = sigma_s[i]
#                self.basicdata.predSig[i] = predSigma_s[i]
#                self.basicdata.densityByIndivid[i] = denByIndivid_s[i] 
                if self.basicdata.moveCaptMask[i]:
                    self.basicdata.lambda0[cc - 1] = lambda0_s
                    self.basicdata.pCaptList[cc - 1] = pcapt_s
                    self.basicdata.alphaLPMF[cc - 1] = llCapt_s
                    self.basicdata.preTauPCAPT[cc- 1] = deepcopy(
                        self.basicdata.preTauPCAPT_s[cc - 1])
                ## ADD DIC FOR THIS ITERATION
                self.dic_g += (pnew - prior_s) 
            elif (zValue >= rValue):
                self.dic_g += (pnow - prior) 
        # sigma squared for captured individuals
        self.basicdata.sigmaTrapSq = (self.basicdata.sigma[self.basicdata.moveCaptMask])**2
        self.basicdata.logsigma = np.log(self.basicdata.sigma[self.basicdata.moveCaptMask])




    def E_UpdateFX(self):
        """
        ## Update the sigma error (E) parameter; conjugate with known mean
        """
    #    pred = np.dot(x, para)
    #    pred = pred.transpose()
        predDiff = np.log(self.basicdata.sigma) - np.log(self.basicdata.predSigma)
    #    mufx = self.params.y - pred
    #    muTranspose = mufx.transpose()
    #    sx = np.dot(mufx, muTranspose)
        sx = np.sum(predDiff**2.0)
        u1 = self.params.E_Priors[0] + np.multiply(.5, self.basicdata.nUniqueAnimal)
        u2 = self.params.E_Priors[1] + np.multiply(.5, sx)               # rate parameter    
        isg = np.random.gamma(u1, 1./u2, size = None)            # formulation using shape and scale
        self.basicdata.E = np.sqrt(1.0/isg)


    def b_UpdateFX(self):
        """
        ## Update beta parameters for predicting sigma
        """
        b_s = np.random.normal(self.basicdata.b, self.params.searchBeta)
        k_s = np.exp(np.dot(self.basicdata.sigmaCovar, b_s))
        predSig_s = k_s / np.sqrt(self.basicdata.densityByIndivid)
        prior = stats.norm.logpdf(self.basicdata.b, self.params.b_Priors[0], 
                self.params.b_Priors[1])
        prior_s = stats.norm.logpdf(b_s, self.params.b_Priors[0], 
                self.params.b_Priors[1])
        llik = stats.norm.logpdf(np.log(self.basicdata.sigma), 
                np.log(self.basicdata.predSigma), self.basicdata.E)
        llik_s = stats.norm.logpdf(np.log(self.basicdata.sigma), np.log(predSig_s), 
                self.basicdata.E)
        pnow = np.sum(llik) + np.sum(prior)
        pnew = np.sum(llik_s) + np.sum(prior_s)

#        print('now', np.sum(prior), np.sum(llik), pnow, 'predsig', 
#            self.basicdata.predSigma[10])
#        print('new', np.sum(prior_s), np.sum(llik_s), pnew, 'predsig_S',
#            predSig_s[10])
#        rValue = np.exp(pnew - pnow)        # calc importance ratio
#        zValue = np.random.uniform(0.0, 1.0, size = None)
        pdiff = pnew - pnow
        if pdiff > 1.0:
            rValue = 1.0
            zValue = 0.0
        elif pdiff < -12.0:
            rValue = 0.0
            zValue = 1.0
        else:
            rValue = np.exp(pdiff)        # calc importance ratio
            zValue = np.random.uniform(0.0, 1.0, size = None)
        if (rValue > zValue):
            self.basicdata.b = b_s.copy()
            self.basicdata.predSigma = predSig_s.copy()
            self.basicdata.k = k_s.copy()


    def delta_Update(self):
        """
        update latent delta - Individual effect on g0
        """
        delta_s = np.random.normal(self.basicdata.delta, self.params.searchDelta)
        ## calculate proposed prob of capture
        for i in range(self.basicdata.nCaptureInd):
            # get proposed lambda0 by subtracting delta and add delta_s
            lambda0_s = inv_logit(logit(self.basicdata.lambda0[i]) -
                self.basicdata.delta[i] + delta_s[i])

            ## 2-d prob of capture, ind, trap and night
            pcapt_s = (lambda0_s * np.exp(-self.basicdata.distByIndividList[i] / 
                    2.0 / self.basicdata.sigmaTrapSq[i])) 
####            pcapt_s = (lambda0_s * np.exp(-self.basicdata.distByIndividList[i] / 
####                    2.0 / self.basicdata.sigmaTrapSq[i]) * self.basicdata.statusList[i] ) 
            self.basicdata.preTauPCAPT_s[i] = pcapt_s

            pcapt_s = (1 - 
                (1- (pcapt_s**(self.basicdata.tau * 
                self.basicdata.prevCaptList[i])) *
                (pcapt_s**(1.0 - self.basicdata.prevCaptList[i])))**
                self.basicdata.statusList[i])
#            pcapt_s = (1 - 
#                (1- (pcapt_s**(self.basicdata.tau[self.basicdata.tauIndxList[i]] * 
#                self.basicdata.prevCaptList[i])) *
#                (pcapt_s**(1.0 - self.basicdata.prevCaptList[i])))**
#                self.basicdata.statusList[i])

####            pcapt_s = (self.basicdata.statusList * 
####                (pcapt_s**(self.basicdata.tau * self.basicdata.prevCaptList[i])) *
####                (pcapt_s**(1.0 - self.basicdata.prevCaptList[i])))
####            pcapt_s = ((pcapt_s**(self.basicdata.tau * self.basicdata.prevCaptList[i])) *
####                (pcapt_s**(1.0 - self.basicdata.prevCaptList[i])))
            self.basicdata.pCaptList_s[i] = pcapt_s
            ## GET MULTINOMIAL PROBABILITIES
            # Total prob TP
            TP = np.expand_dims(1.0 - np.prod((1.0 - pcapt_s), axis = 1), 1)
            # Normalised probabilities
            sumPCapt = np.sum(pcapt_s, axis =1)
            sumPCapt = np.where(sumPCapt < 1E-280, 1E-280, sumPCapt)
            # sum to one
            NP = pcapt_s / np.expand_dims(sumPCapt, 1)
            # Multinomial probabilities
            MNP = NP * TP
            # Prob of non-capture
            pNC = 1.0 - TP
            # populate array
            self.basicdata.MNP_s[i][:, :-1] = MNP
            self.basicdata.MNP_s[i][:, -1] = pNC[:, 0]
            # probs of captures
            captProbs = self.basicdata.MNP_s[i][self.basicdata.captDataList[i] == 1]
            # fix for super low probabilities - poor parameter values.
            captProbs = np.where(captProbs < 1E-280, 1E-280, captProbs)
            # proposed likelihood
            self.basicdata.alphaLPMF_s[i] = np.sum(np.log(captProbs))
            # calculate Importance ratio
            prior = stats.norm.logpdf(self.basicdata.delta[i], self.params.deltaPriors[0], 
                    self.params.deltaPriors[1])
            prior_s = stats.norm.logpdf(delta_s[i], self.params.deltaPriors[0], 
                    self.params.deltaPriors[1])
            pnow = self.basicdata.alphaLPMF[i] + prior
            pnew = self.basicdata.alphaLPMF_s[i] + prior_s
#            pnow = np.sum(self.basicdata.alphaLPMF) + prior
#            pnew = np.sum(self.basicdata.alphaLPMF_s) + prior_s
            pdiff = pnew - pnow
            if pdiff > 1.0:
                rValue = 1.0
                zValue = 0.0
            elif pdiff < -5.0:
                rValue = 0.0
                zValue = 1.0
            else:
                rValue = np.exp(pdiff)        # calc importance ratio
                zValue = np.random.uniform(0.0, 1.0, size = None)
            if (rValue > zValue):
                self.basicdata.delta[i] = delta_s[i]
                self.basicdata.lambda0[i] = lambda0_s
                self.basicdata.pCaptList[i] = self.basicdata.pCaptList_s[i]
                self.basicdata.alphaLPMF[i] = self.basicdata.alphaLPMF_s[i]
                self.basicdata.preTauPCAPT[i] = self.basicdata.preTauPCAPT_s[i]


    def alphaBlock_updateFX(self):
        """
        update alpha parameters for predicting a0 (block updating with MH)
        """
        alpha_s = np.random.multivariate_normal(self.basicdata.alpha, 
                self.params.alphaCmat)
#        alpha_s = np.random.normal(self.basicdata.alpha, self.params.searchAlpha)
        # lambda0
        ## calculate proposed prob of capture
        for i in range(self.basicdata.nCaptureInd):
            if (self.params.modelID >=20):
                ll0_s = (alpha_s[0] + (self.basicdata.logsigma[i] * alpha_s[1]) + 
                    (self.basicdata.raisedLegList[i] * alpha_s[2]) + 
                    (self.basicdata.groundLegList[i] * alpha_s[3]) +
                    self.basicdata.delta[i])
            elif (self.params.modelID == 3) | (self.params.modelID == 6):
                ll0_s = (alpha_s[0] + (self.basicdata.logsigma[i] * alpha_s[1]) + 
                    (self.basicdata.raisedLegList[i] * alpha_s[2]) + 
                    self.basicdata.delta[i])
            elif (self.params.modelID == 4) | (self.params.modelID == 5):
                ll0_s = (alpha_s[0] + (self.basicdata.logsigma[i] * alpha_s[1]) + 
                    self.basicdata.delta[i])
            # lambda0_s
            self.basicdata.lambda0_s[i] = inv_logit(ll0_s)
            ## 2-d prob of capture, ind, trap and night
            pcapt_s = (self.basicdata.lambda0_s[i] * 
                    np.exp(-self.basicdata.distByIndividList[i] / 
                    2.0 / self.basicdata.sigmaTrapSq[i])) 
####            pcapt_s = (self.basicdata.lambda0_s[i] * 
####                    np.exp(-self.basicdata.distByIndividList[i] / 
####                    2.0 / self.basicdata.sigmaTrapSq[i]) * self.basicdata.statusList[i] ) 
            self.basicdata.preTauPCAPT_s[i] = pcapt_s
            ## prevCaptList is 2D nights by nTraps - binary
            ## pcapt_s is 2D nights by ntraps



            pcapt_s = (1 - 
                (1- (pcapt_s**(self.basicdata.tau * 
                self.basicdata.prevCaptList[i])) *
                (pcapt_s**(1.0 - self.basicdata.prevCaptList[i])))**
                self.basicdata.statusList[i])
#            pcapt_s = (1 - 
#                (1- (pcapt_s**(self.basicdata.tau[self.basicdata.tauIndxList[i]] * 
#                self.basicdata.prevCaptList[i])) *
#                (pcapt_s**(1.0 - self.basicdata.prevCaptList[i])))**
#                self.basicdata.statusList[i])


####            pcapt_s = (self.basicdata.statusList[i] * 
####                (pcapt_s**(self.basicdata.tau * self.basicdata.prevCaptList[i])) *
####                (pcapt_s**(1.0 - self.basicdata.prevCaptList[i])))
            self.basicdata.pCaptList_s[i] = pcapt_s
            ## GET MULTINOMIAL PROBABILITIES
            # Total prob TP; 'OR' probability for each night across traps
            TP = np.expand_dims(1.0 - np.prod((1.0 - pcapt_s), axis = 1), 1)
            # Normalised probabilities
            sumPCapt = np.sum(pcapt_s, axis =1)
            sumPCapt = np.where(sumPCapt < 1E-280, 1E-280, sumPCapt)
            # sum to one
            NP = pcapt_s / np.expand_dims(sumPCapt, 1)
            # Multinomial probabilities
            MNP = NP * TP
            # Prob of non-capture
            pNC = 1.0 - TP
            # populate array
            self.basicdata.MNP_s[i][:, :-1] = MNP
            self.basicdata.MNP_s[i][:, -1] = pNC[:, 0]
            # probs of captures
            captProbs = self.basicdata.MNP_s[i][self.basicdata.captDataList[i] == 1]
            # fix for super low probabilities - poor parameter values.
            captProbs = np.where(captProbs < 1E-280, 1E-280, captProbs)
            # proposed likelihood
            self.basicdata.alphaLPMF_s[i] = np.sum(np.log(captProbs))
        # calculate Importance ratio
        prior = stats.norm.logpdf(self.basicdata.alpha, self.params.alphaMeanPriors, 
            self.params.alphaSDPriors)
        prior_s = stats.norm.logpdf(alpha_s, self.params.alphaMeanPriors, 
            self.params.alphaSDPriors)
#        prior = stats.norm.logpdf(self.basicdata.alpha, self.params.alphaPriors[0], 
#            self.params.alphaPriors[1])
#        prior_s = stats.norm.logpdf(alpha_s, self.params.alphaPriors[0], 
#            self.params.alphaPriors[1])
        pnow = np.sum(self.basicdata.alphaLPMF) + np.sum(prior)
        pnew = np.sum(self.basicdata.alphaLPMF_s) + np.sum(prior_s)
        pdiff = pnew - pnow
        if pdiff > 1.0:
            rValue = 1.0
            zValue = 0.0
        elif pdiff < -12.0:
            rValue = 0.0
            zValue = 1.0
        else:
            rValue = np.exp(pdiff)        # calc importance ratio
            zValue = np.random.uniform(0.0, 1.0, size = None)
        if (rValue > zValue):
            self.basicdata.alpha = alpha_s.copy()
#            self.basicdata.a0 = a0_s.copy()
            self.basicdata.lambda0 = deepcopy(self.basicdata.lambda0_s)
            self.basicdata.pCaptList = deepcopy(self.basicdata.pCaptList_s)
            self.basicdata.alphaLPMF = self.basicdata.alphaLPMF_s.copy()
            self.basicdata.preTauPCAPT = deepcopy(self.basicdata.preTauPCAPT_s)

    def tau_updateFX(self):
        """
        ## update tau for previous capture effect
        """
#        ## 1-d lambda0 with new Sigmas
#        self.basicdata.lambda0 = a0_s / 2.0 / np.pi / self.basicdata.sigmaTrapSq
        # propose tau_s
        tau_s = np.exp(np.random.normal(np.log(self.basicdata.tau), self.params.tauSearch))
        ## calculate proposed prob of capture
        for i in range(self.basicdata.nCaptureInd):
            ## 2-d prob of capture, ind, trap and night
            PCAPT = self.basicdata.preTauPCAPT[i]




            pcapt_s = (1 - 
                (1- (PCAPT**(tau_s * self.basicdata.prevCaptList[i])) *
                (PCAPT**(1.0 - self.basicdata.prevCaptList[i])))**
                self.basicdata.statusList[i])
#            pcapt_s = (1 - 
#                (1- (PCAPT**(tau_s[self.basicdata.tauIndxList[i]] * 
#                self.basicdata.prevCaptList[i])) *
#                (PCAPT**(1.0 - self.basicdata.prevCaptList[i])))**
#                self.basicdata.statusList[i])



            self.basicdata.pCaptList_s[i] = pcapt_s
            ## GET MULTINOMIAL PROBABILITIES
            # Total prob TP
            TP = np.expand_dims(1.0 - np.prod((1.0 - pcapt_s), axis = 1), 1)
            # Normalised probabilities
            sumPCapt = np.sum(pcapt_s, axis =1)
            sumPCapt = np.where(sumPCapt < 1E-280, 1E-280, sumPCapt)
            # sum to one
            NP = pcapt_s / np.expand_dims(sumPCapt, 1)
            # Multinomial probabilities
            MNP = NP * TP
            # Prob of non-capture
            pNC = 1.0 - TP
            # populate array
            self.basicdata.MNP_s[i][:, :-1] = MNP
            self.basicdata.MNP_s[i][:, -1] = pNC[:, 0]
            # probs of captures
            captProbs = self.basicdata.MNP_s[i][self.basicdata.captDataList[i] == 1]
            # fix for super low probabilities - poor parameter values.
            captProbs = np.where(captProbs < 1E-280, 1E-280, captProbs)
            # proposed likelihood
            self.basicdata.alphaLPMF_s[i]= np.sum(np.log(captProbs))
        # calculate Importance ratio
        prior = np.log(gamma_pdf(self.basicdata.tau, self.params.tauPriors[0], 
            self.params.tauPriors[1]))
        prior_s = np.log(gamma_pdf(tau_s, self.params.tauPriors[0], 
            self.params.tauPriors[1]))
        pnow = np.sum(self.basicdata.alphaLPMF) + prior
        pnew = np.sum(self.basicdata.alphaLPMF_s) + prior_s
#        pnow = np.sum(self.basicdata.alphaLPMF) + np.sum(prior)
#        pnew = np.sum(self.basicdata.alphaLPMF_s) + np.sum(prior_s)
        pdiff = pnew - pnow
        if pdiff > 1.0:
            rValue = 1.0
            zValue = 0.0
        elif pdiff < -12.0:
            rValue = 0.0
            zValue = 1.0
        else:
            rValue = np.exp(pdiff)        # calc importance ratio
            zValue = np.random.uniform(0.0, 1.0, size = None)
        if (rValue > zValue):
            self.basicdata.tau = tau_s
            self.basicdata.pCaptList = deepcopy(self.basicdata.pCaptList_s)
            self.basicdata.alphaLPMF = self.basicdata.alphaLPMF_s.copy()

    def updateArea(self):
        for i in range(self.basicdata.nUniqueArea):
            sig_i = self.basicdata.sigma[self.basicdata.individAreaIndx == i]
            self.basicdata.areaMeanSigma[i] = np.mean(sig_i)
            dist_i = 4.0 * self.basicdata.areaMeanSigma[i]
            ha_i = np.sum(self.basicdata.areaDistances[i] <= dist_i)
            self.basicdata.areaBySite[i] = ha_i

    def D_UpdateFX(self):
        """
        ## UPDATE DENSITY AND NUMBER OF UNOBSERVED INDIVIDUALS BY SITE
        """
        ## LOOP THRU SITES
        for i in range(self.basicdata.nUniqueArea):
            ## LOG SIGMA FOR AREA I
            sig_i = self.basicdata.areaMeanSigma[i]
            sig2_i = sig_i**2
            logsig_i = np.log(sig_i)

            logCombinTerm = (gammaln(self.basicdata.nPresentBySite[i] + 1) -
                gammaln(self.basicdata.nCapturedAreaJul[i] + 1) -
                gammaln(self.basicdata.nPresentBySite[i] - 
                self.basicdata.nCapturedAreaJul[i] + 1)) 

            ## LOOP INDIVIDUALS AND AND GET DISTANCES
            for j in range(self.basicdata.nTotalUnObserved[i]):
                test_i = np.random.binomial(1, self.params.propPresAbs)
                if test_i == 0:
                    continue
                ## CURRENT PRESENT DATA FOR INDIVID J
                pres_ij = self.basicdata.presIndicator[i][j]
                llNoCapt_i = 0.0
                raisedLeg_ij = self.basicdata.raisedLegUnObserved[i][j]
                groundLeg_ij = self.basicdata.groundLegUnObserved[i][j]
                dist_ij = self.basicdata.distance2UnObserved[i][j]
#                tn_ij = self.basicdata.trapNightUnObs[i][j]
                indJulStatusMask = self.basicdata.indJulMask[i][j]
                ## IF IND J IS PRESENT, CALC PROB CAPTURE
                if pres_ij == 1:
                    ## LOGIT LAMBDA 0 for
                    if (self.params.modelID >= 20): 
                        ll0 = np.array(self.basicdata.alpha[0] + 
                            (logsig_i * self.basicdata.alpha[1]) + 
                            (raisedLeg_ij * self.basicdata.alpha[2]) + 
                            (groundLeg_ij * self.basicdata.alpha[3]))
                    elif (self.params.modelID == 3) | (self.params.modelID == 6):
                        ll0 = np.array(self.basicdata.alpha[0] + 
                            (logsig_i * self.basicdata.alpha[1]) + 
                            (raisedLeg_ij * self.basicdata.alpha[2]))
                    elif (self.params.modelID == 4) | (self.params.modelID == 5):
                        ll0 = np.array(self.basicdata.alpha[0] + 
                            (logsig_i * self.basicdata.alpha[1]))
                    ## MAKE INTO A PROBABILITY
                    lambda_0 = np.exp(ll0) / (1.0 + np.exp(ll0))
                    ## P NO CAPT ACROSS ALL TRAPS FOR INDIVIDUAL I
                    pNoCapt = ((1.0 - (lambda_0 * np.exp(-dist_ij / 2.0 / 
                        sig2_i)))**self.basicdata.statusUnObserved[i][j])
                    pNoCapt = np.prod(pNoCapt)
#                    llNoCapt_i = np.log(pNoCapt)

                    if pNoCapt < 1E-80:
                        llNoCapt_i = 0.0
                    else:
                        llNoCapt_i = np.log(pNoCapt)


                combinTerm = np.sum(logCombinTerm[indJulStatusMask])

#                combinTerm = (gammaln(self.basicdata.nPresentBySite[i] + 1) - 
#                    gammaln(self.basicdata.nPresUnObserved[i] + 1))
#                prior_ij = stats.norm.logpdf(np.log(self.basicdata.D[i]), 
#                    np.log(self.params.dNormPrior[i,0]), self.params.dNormPrior[i,1])
                ## LIKELIHOOD OF CURRENT PRES/ABSENCE
                pnow = llNoCapt_i + combinTerm
#                pnow = llNoCapt_i + combinTerm + prior_ij
                ## GET PROPOSED PRES/ABS
                pres_s = np.abs(pres_ij - 1)
                ## DELTA N FOR CHANGING N PRESENT AND DENSITY IF ACCEPT PROPOSAL
                deltaN = pres_s - pres_ij
                N_s = self.basicdata.nPresentBySite[i] + deltaN
                nUnObs_s = self.basicdata.nPresUnObserved[i] + deltaN
                D_s = N_s / self.basicdata.areaBySite[i] 
#                prior_s = stats.norm.logpdf(np.log(D_s), 
#                        np.log(self.params.dNormPrior[i,0]),
#                        self.params.dNormPrior[i,1])


                logCombinTerm_s = (gammaln(N_s + 1) -
                    gammaln(self.basicdata.nCapturedAreaJul[i] + 1) -
                    gammaln(N_s - self.basicdata.nCapturedAreaJul[i] + 1)) 

                combinTerm_s = np.sum(logCombinTerm_s[indJulStatusMask])



#                combinTerm_s = (gammaln(N_s + 1) - gammaln(nUnObs_s + 1))
                ## IF PROPOSE NEW PRESENCE, THEN HAVE TO GET pNoCapt_s   
                if pres_s == 1:
                    ## LOGIT LAMBDA 0 for 
                    if (self.params.modelID >= 20): 
                        ll0 = np.array(self.basicdata.alpha[0] + 
                            (logsig_i * self.basicdata.alpha[1]) + 
                            (raisedLeg_ij * self.basicdata.alpha[2]) + 
                            (groundLeg_ij * self.basicdata.alpha[3]))
                    elif (self.params.modelID == 3) | (self.params.modelID == 6):
                        ll0 = np.array(self.basicdata.alpha[0] + 
                            (logsig_i * self.basicdata.alpha[1]) + 
                            (raisedLeg_ij * self.basicdata.alpha[2]))
                    elif (self.params.modelID == 4) | (self.params.modelID == 5):
                        ll0 = np.array(self.basicdata.alpha[0] + 
                            (logsig_i * self.basicdata.alpha[1]))
                    ## MAKE INTO A PROBABILITY
                    lambda_0 = np.exp(ll0) / (1.0 + np.exp(ll0))
                    ## P NO CAPT ACROSS ALL TRAPS FOR INDIVIDUAL I
                    pNoCapt_s = ((1.0 - (lambda_0 * np.exp(-dist_ij / 2.0 / 
                        sig2_i)))**self.basicdata.statusUnObserved[i][j])
#                    pNoCapt_s = ((1.0 - (lambda_0 * np.exp(-dist_ij / 
#                        2.0 / sig2_i)))**tn_ij)
                    pNoCapt_s = np.prod(pNoCapt_s)
                    if pNoCapt_s < 1E-80:
                        llNoCapt_s = 0.0
                    else:
                        llNoCapt_s = np.log(pNoCapt_s)
                ## IF PROPOSE ABSENCE   
                else:
                    llNoCapt_s = 0.0                                
                pnew = llNoCapt_s + combinTerm_s
#                pnew = llNoCapt_s + combinTerm_s + prior_s 
                pdiff = pnew - pnow
                if pdiff > 1.0:
                    rValue = 1.0
                    zValue = 0.0
                elif pdiff < -5.0:
                    rValue = 0.0
                    zValue = 1.0
                else:
                    rValue = np.exp(pdiff)        # calc importance ratio
                    zValue = np.random.uniform(0.0, 1.0, size = None)
                if (rValue > zValue):
                    self.basicdata.nPresentBySite[i] = N_s
                    self.basicdata.nPresUnObserved[i] = nUnObs_s
                    self.basicdata.presIndicator[i][j] = pres_s
                    self.basicdata.D[i] = D_s
        ## UPDATE DENSITY BY INDIVID FOR USE IN SIGMA UPDATE
        self.densityByIndivid = self.basicdata.D[self.basicdata.individAreaIndx]
        ## PREDICTED SIGMA FROM DENSITY
        self.basicdata.predSigma = (self.basicdata.k / 
            np.sqrt(self.basicdata.densityByIndivid))





    ########            Main mcmc function
    ########
    def mcmcFX(self):

        cc = 0
        for g in range(self.params.ngibbs * self.params.thinrate + self.params.burnin):
#            print('g', g)
            # betas for predicting sigma
            self.b_UpdateFX()

            self.sig_UpdateFX()
            self.E_UpdateFX()
#            self.updateOneAlpha()
            self.alphaBlock_updateFX()
            self.tau_updateFX()
            self.delta_Update()

            self.updateArea()            

#            print('Begin Dupdate ###############')
#            D_UpdateFX(self.basicdata.nUniqueArea, self.basicdata.nTotalUnObserved, 
#                self.basicdata.nPresentBySite, self.basicdata.nPresUnObserved, 
#                self.basicdata.presIndicator, 
#                self.params.dNormPrior, self.basicdata.D, self.basicdata.areaBySite, 
#                self.basicdata.areaMeanSigma, self.basicdata.alpha, 
#                self.basicdata.raisedLegUnObserved, self.basicdata.groundLegUnObserved, 
#                self.basicdata.distance2UnObserved, self.basicdata.trapNightUnObs)
            self.D_UpdateFX()
#            print('end dupdate')

            if g in self.params.keepseq:
                self.bgibbs[cc] = self.basicdata.b
                self.Dgibbs[cc] = self.basicdata.D
#                self.agibbs[cc] = self.basicdata.a0
                self.agibbs[cc] = self.basicdata.alpha
                self.siggibbs[cc] = self.basicdata.sigma
                self.Egibbs[cc] = self.basicdata.E
                self.tgibbs[cc] = self.basicdata.tau
                self.deltagibbs[cc] = self.basicdata.delta
                self.dicgibbs[cc] = -2.0 * self.dic_g
                cc = cc + 1

#        print('beta', self.bgibbs[-10:], 'sig', self.siggibbs[-10:,:4], 
#            'E', self.Egibbs[-10:], 'agibbs', self.agibbs[-10:], 
#            'tgibbs', self.tgibbs[-10:])






###########################################
######### UN-USED FUNCTIONS
###
    def alpha_updateFX_XXX(self):
        """
        update alpha parameters for predicting a0 (block updating with MH)
        """
#        alpha_s = np.zeros(3)
        for j in range(3):
            alpha_s = self.basicdata.alpha.copy()
            alpha_s[j] = np.random.normal(self.basicdata.alpha[j], self.params.searchAlpha[j])
            ## 1-d a0
            a0_s = (alpha_s[0] + (self.basicdata.raisedLegList * alpha_s[1]) + 
                (self.basicdata.groundLegList * alpha_s[2]))
            ## 1-d lambda0
            lambda0_s = a0_s / 2.0 / np.pi / self.basicdata.sigmaTrapSq
            ## calculate proposed prob of capture
            for i in range(self.basicdata.nCaptureInd):
                ## 2-d prob of capture, ind, trap and night
                pcapt_s = (lambda0_s[i] * np.exp(-self.basicdata.distByIndividList[i] / 
                        2.0 / self.basicdata.sigmaTrapSq[i]) * self.basicdata.statusList[i] ) 
                self.basicdata.preTauPCAPT_s[i] = pcapt_s
                pcapt_s = ((pcapt_s**(self.basicdata.tau * self.basicdata.prevCaptList[i])) *
                    (pcapt_s**(1.0 - self.basicdata.prevCaptList[i])))
                self.basicdata.pCaptList_s[i] = pcapt_s
                ## GET MULTINOMIAL PROBABILITIES
                # Total prob TP
                TP = np.expand_dims(1.0 - np.prod((1.0 - pcapt_s), axis = 1), 1)
                # Normalised probabilities
                sumPCapt = np.sum(pcapt_s, axis =1)
                sumPCapt = np.where(sumPCapt < 1E-280, 1E-280, sumPCapt)
                # sum to one
                NP = pcapt_s / np.expand_dims(sumPCapt, 1)
                # Multinomial probabilities
                MNP = NP * TP
                # Prob of non-capture
                pNC = 1.0 - TP
                # populate array
                self.basicdata.MNP_s[i][:, :-1] = MNP
                self.basicdata.MNP_s[i][:, -1] = pNC[:, 0]
                # probs of captures
                captProbs = self.basicdata.MNP_s[i][self.basicdata.captDataList[i] == 1]
                # fix for super low probabilities - poor parameter values.
                captProbs = np.where(captProbs < 1E-280, 1E-280, captProbs)
                # proposed likelihood
                self.basicdata.alphaLPMF_s[i] = np.sum(np.log(captProbs))
            # calculate Importance ratio
            prior = stats.norm.logpdf(self.basicdata.alpha[j], self.params.alphaMeanPriors[j], 
                self.params.alphaSDPriors[j])
            prior_s = stats.norm.logpdf(alpha_s[j], self.params.alphaMeanPriors[j], 
                self.params.alphaSDPriors[j])
            pnow = np.sum(self.basicdata.alphaLPMF) + (prior)
            pnew = np.sum(self.basicdata.alphaLPMF_s) + (prior_s)
            pdiff = pnew - pnow
            if pdiff > 1.0:
                rValue = 1.0
                zValue = 0.0
            elif pdiff < -12.0:
                rValue = 0.0
                zValue = 1.0
            else:
                rValue = np.exp(pdiff)        # calc importance ratio
                zValue = np.random.uniform(0.0, 1.0, size = None)
            if (rValue > zValue):
                self.basicdata.alpha = alpha_s.copy()
                self.basicdata.a0 = a0_s.copy()
                self.basicdata.lambda0 = lambda0_s.copy()
                self.basicdata.pCaptList = self.basicdata.pCaptList_s.copy()
                self.basicdata.alphaLPMF = self.basicdata.alphaLPMF_s.copy()
                self.basicdata.preTauPCAPT = self.basicdata.preTauPCAPT_s.copy()




    def updateOneAlpha(self):
        """
        update alpha parameters for predicting a0 (block updating with MH)
        """
#        alpha_s = np.zeros(3)
#        for i in range(3):
#            alpha_s = np.random.normal(self.basicdata.alpha[i], self.params.searchAlpha[i])
        a0_s = np.random.normal(self.basicdata.a0, self.params.searchAlpha)
        ## 1-d a0
#        a0_s = (alpha_s[0] + (self.basicdata.raisedLegList * alpha_s[1]) + 
#                (self.basicdata.groundLegList * alpha_s[2]))
        ## 1-d lambda0
        lambda0_s = a0_s / 2.0 / np.pi / self.basicdata.sigmaTrapSq
        ## calculate proposed prob of capture
        for i in range(self.basicdata.nCaptureInd):
           ## 2-d prob of capture, ind, trap and night
            pcapt_s = (lambda0_s[i] * np.exp(-self.basicdata.distByIndividList[i] / 
                    2.0 / self.basicdata.sigmaTrapSq[i]) * self.basicdata.statusList[i] ) 
            self.basicdata.preTauPCAPT_s[i] = pcapt_s
            pcapt_s = ((pcapt_s**(self.basicdata.tau * self.basicdata.prevCaptList[i])) *
                    (pcapt_s**(1.0 - self.basicdata.prevCaptList[i])))
            self.basicdata.pCaptList_s[i] = pcapt_s
            ## GET MULTINOMIAL PROBABILITIES
            # Total prob TP
            TP = np.expand_dims(1.0 - np.prod((1.0 - pcapt_s), axis = 1), 1)
            # Normalised probabilities
            sumPCapt = np.sum(pcapt_s, axis =1)
            sumPCapt = np.where(sumPCapt < 1E-280, 1E-280, sumPCapt)
            # sum to one
            NP = pcapt_s / np.expand_dims(sumPCapt, 1)
            # Multinomial probabilities
            MNP = NP * TP
            # Prob of non-capture
            pNC = 1.0 - TP
            # populate array
            self.basicdata.MNP_s[i][:, :-1] = MNP
            self.basicdata.MNP_s[i][:, -1] = pNC[:, 0]
            # probs of captures
            captProbs = self.basicdata.MNP_s[i][self.basicdata.captDataList[i] == 1]
            # fix for super low probabilities - poor parameter values.
            captProbs = np.where(captProbs < 1E-280, 1E-280, captProbs)
            # proposed likelihood
            self.basicdata.alphaLPMF_s[i] = np.sum(np.log(captProbs))
        # calculate Importance ratio
        prior = stats.norm.logpdf(self.basicdata.a0, self.params.alphaMeanPriors[0], 
            self.params.alphaSDPriors[0])
        prior_s = stats.norm.logpdf(a0_s, self.params.alphaMeanPriors[0], 
            self.params.alphaSDPriors[0])
        pnow = np.sum(self.basicdata.alphaLPMF) + (prior)
        pnew = np.sum(self.basicdata.alphaLPMF_s) + (prior_s)
        pdiff = pnew - pnow
        if pdiff > 1.0:
            rValue = 1.0
            zValue = 0.0
        elif pdiff < -12.0:
            rValue = 0.0
            zValue = 1.0
        else:
            rValue = np.exp(pdiff)        # calc importance ratio
            zValue = np.random.uniform(0.0, 1.0, size = None)
        if (rValue > zValue):
#            self.basicdata.alpha = alpha_s.copy()
            self.basicdata.a0 = a0_s
            self.basicdata.lambda0 = lambda0_s.copy()
            self.basicdata.pCaptList = self.basicdata.pCaptList_s.copy()
            self.basicdata.alphaLPMF = self.basicdata.alphaLPMF_s.copy()
            self.basicdata.preTauPCAPT = self.basicdata.preTauPCAPT_s.copy()

