#!/usr/bin/env python

import os
from scipy import stats
#from scipy.special import gammaln
#from scipy.special import gamma
import numpy as np
from numba import jit
#import pickle
#import datetime
from osgeo import gdal
from osgeo import ogr
from osgeo import osr
from osgeo import gdalconst
import pandas as pd


COMPRESSED_HFA = ['COMPRESSED=YES']


@jit
def loopCovarRaster(ndat, x, y, rast, ulx, uly, cov_i, xres, yres):
    """
    ## LOOP THRU RASTER I AND GET DATA FOR ALL POINTS
    """ 
    for j in range(ndat):
        col = np.int32((x[j] - ulx) / xres)
        ## HAVE TO MAKE yres POSITIVE; NORMALLY IT IS NEGATIVE
        row = np.int32((uly - y[j]) / -yres)
        cov_i[j] = rast[row, col]
    return(cov_i)


@jit
def makeDistArray(RES, neighDist):
    """
    Pre calculate this so we don't have to do it each time
    """
    winsize = int((neighDist / RES * 2) + 1)
    halfwinsize = int(winsize / 2)
    distarray = np.empty((winsize, winsize))   #, np.int32)
    for x in range(winsize):
        for y in range(winsize):
            distx = (x - halfwinsize) * RES
            disty = (y - halfwinsize) * RES
            dist = np.sqrt(distx*distx + disty*disty)
            distarray[y, x] = dist
    return(distarray, halfwinsize, winsize)

@jit
def neighborhoodValues(nDat, x, y, rastData, ulx, uly, 
                cov_i, xres, yres, halfwinsize, distArray, neighborDist):
    (ysize, xsize) = rastData.shape
    for i in range(nDat):
        cc = 0
        xdistoff = 0
        ydistoff = 0
        col = np.int32((x[i] - ulx) / xres)
        row = np.int32((uly - y[i]) / -yres)
        tlx = col - halfwinsize
        if tlx < 0:
            xdistoff = -tlx
            tlx = 0    
        tly = row - halfwinsize
        if tly < 0:
            ydistoff = -tly
            tly = 0
        brx = col + halfwinsize
        if brx > xsize - 1:
            brx = xsize - 1
        bry = row + halfwinsize
        if bry > ysize - 1:
            bry = ysize - 1
        for cx in range(tlx, brx+1):
            for cy in range(tly, bry+1):
                dist = distArray[ydistoff + cy - tly, xdistoff + cx - tlx]                
                if dist <= neighborDist:
                    rastValue = rastData[cy, cx]
                    if rastValue > 1:
                        cov_i[i, (rastValue - 2)] += 1
                    cc += 1
        cov_i[i] = cov_i[i] / cc
        print('i', i, 'cc', cc, cov_i[i])
    return(cov_i)



class InterceptData(object):
    def __init__(self, ptInputFname, rastFname, ptOutputFname, xName, yName,
                RES, neighborDist):
        """
        Object to extract data from raster 
        """
        self.ptInputFname = ptInputFname
        self.rastFname = rastFname
        self.ptOutputFname= ptOutputFname
        self.xName = xName
        self.yName = yName
        self.res = RES
        self.neighborDist = neighborDist

        ################ RUN FUNCTIONS
        self.readPtData()
        (self.distArray, self.halfwinsize, self.winsize) = makeDistArray(self.res, 
            self.neighborDist)

        print('winsize', self.winsize, 'halfwinsize', self.halfwinsize)

        self.getRasterData()
        self.writePandasFile()
        self.corrLoop()

    def readPtData(self):
        """
        ## READ IN DATA FROM CSV
        """
        self.locDat = pd.read_csv(self.ptInputFname, delimiter=',')

        print('locdat', self.locDat[[self.xName]])

        self.x = np.array(self.locDat[[self.xName]]).flatten()
        self.y = np.array(self.locDat[[self.yName]]).flatten()
        self.nDat = len(self.x)

    def getRasterData(self):
        """
        ## GET DATA FROM RASTERS 
        """
        ## OPEN RASTER AS ARRAY
        src = gdal.Open(self.rastFname)
        (ulx, xres, xskew, uly, yskew, yres) = src.GetGeoTransform()
        rastData = src.ReadAsArray()
        print(np.shape(rastData), 'res', xres, yres)
        ## EMPTY ARRAY TO POPULATE
        self.cov_i = np.zeros((self.nDat, 3))
        ## LOOP THRU DATA TO GET RASTER VALUES
        neighborhoodValues(self.nDat, self.x, self.y, rastData, ulx, uly, 
           self.cov_i, xres, yres, self.halfwinsize, self.distArray, self.neighborDist)
#        for i in range(self.nDat):
#            print('i', i, 'cov', self.cov_i[i])
        


    def writePandasFile(self):
        """
        write pandas df to directory
        """
#        base = os.path.basename(self.rastFname)
#        fName = os.path.splitext(base)[0]
#        print('fname', fName)
#        self.locDat.loc[:, fName] = self.cov_i
        self.locDat['pFarm'] = self.cov_i[:,0]
        self.locDat['pScrub'] = self.cov_i[:,1]
        self.locDat['pForest'] = self.cov_i[:,2]
        self.locDat.to_csv(self.ptOutputFname, sep=',', header=True, index_label = 'did')


    def corrLoop(self):
        names = ['farm', 'scrub', 'forest']
        ncov = np.shape(self.cov_i)[1]
        for i in range(ncov):
            cov_i = self.cov_i[:, i]
            name_i = names[i]
            for j in range((i + 1), ncov):
                name_j = names[j]
                corr_ij = stats.pearsonr(cov_i, self.cov_i[:, j])
                print('Corr ' + name_i + ':' + name_j, corr_ij)

        farmScrub = np.sum(self.cov_i[:, :2], axis = 1)
        
        print('corr farmScrub : forest', stats.pearsonr(farmScrub, self.cov_i[:, 2]))







######################
# Main function
def main():
    # set paths to scripts and data
    inputDataPath = os.path.join(os.getenv('POFPROJDIR', default = '.'), 
            'possumg0sigma/g0sigmaData')


    ## SET X AND Y COORDINATE NAMES AS IN POINT DATA
    xName = 'xHR'
    yName = 'yHR'

    ## SET RASTER RESOLUTION AND NEIGHBORHOOD DISTANCE
    RES = 25
    neighborDist = 200.0

    ## set Data names
    ptInputFname = os.path.join(inputDataPath, 'individHRCentre.csv')   
    rastFname = os.path.join(inputDataPath, 'habitat.img')
    ptOutputFname = os.path.join(inputDataPath, 'HR_HabitatCovar.csv')    #'pigs_100MCP_clip.

    interceptdata = InterceptData(ptInputFname, rastFname, ptOutputFname,
        xName, yName, RES, neighborDist)

if __name__ == '__main__':
    main()


