#!/usr/bin/env python

########################################
########################################
# This file is part of Possum g0 and sigma analysis
# Copyright (C) 2016 Dean Anderson 
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# ADDITIONAL NOTES ABOUT THIS SCRIPT.
########################################
########################################

import os
import pickle
import numpy as np
from scipy import stats
from copy import deepcopy

def decodeBytes(byteArray, nArray):
    """
    ## loop to decode byte to string
    """
    strArray = str(byteArray[0], 'utf-8')
    for i in range(1, nArray):
        strArr_i = str(byteArray[i], 'utf-8')
        strArray = np.append(strArray, strArr_i)
    return(strArray)


### Define global functions: ###
def logit(x):
    return np.log(x) - np.log(1 - x)


def inv_logit(x):
    return np.exp(x) / (1 + np.exp(x))



def distFX(x1, y1, x2, y2):
    """
    calc distance matrix between 2 sets of points
    resulting matrix dimensions = (len(x1), len(x2))
    """
    deltaX = np.add.outer(x1, -x2)
    deltaX_sq = deltaX**2
    deltaY = np.add.outer(y1, -y2)
    deltaY_sq = deltaY**2
    dist_2D = np.sqrt(deltaX_sq + deltaY_sq)
    return dist_2D

def scaleX(a):
    nCov = np.shape(a)[1] - 2
    for i in range(1, nCov):
        a[:, i] = (a[:, i] - np.mean(a[:, i])) / np.std(a[:, i])
    return(a) 

class BasicData(object):
    def __init__(self, params):
        """
        Object to read in  data
        Import updatable params from params
        set initial values
        """
#        np.warnings.filterwarnings('ignore', category=np.VisibleDeprecationWarning)
        #############################################
        ############ Run basicdata functions
        self.getParams(params)
        self.readData()
        self.cullTooFewFixes()      
        self.makeSigmaCovariates()
        self.getHRCentre()
        self.makeDistList()
        self.makeCaptureArrays()
        self.getNonCaptData()
        self.predictLambda0()
        print('predict lambda')        
        self.getNIndividByArea()
        print('getNIndividByArea')
        self.makeAreaRasters()
        print('Area rasters')
        self.getInitialAreaBySite()
        print('INITIAL AREA')
        self.getTrapDistUnObserved()
        print('trap dist unobser')
        self.getInitialPresence()
        self.getMeanSigmaSites()
        print('initial presence')
        self.getIndJulStatusMask()
        print('Get Individ Jul Status Mask')
        self.getMeanSigmaSites()
        print('mean sigma site')
        self.initialPCaptUnObserved()
        print('initialPcapt unobser')
        ############## End run of basicdata functions
        #############################################

    def getParams(self, params):
        """
        # move params parameters into basicdata
        """
        self.params = params
        self.b = self.params.b                  
        self.D = self.params.D
        self.E = self.params.E
        self.alpha = self.params.alpha
        self.tau = self.params.tau

    def readData(self):
        """
        ## read and transfer GPS and trapping data to basicdata class
        """
        gpsDat = np.genfromtxt(self.params.inputGPSFname,  delimiter=',', names=True,
            dtype=['S32','i8', 'i8', 'f8', 'S10', 'S10', 'S10', 'i8', 'i8', 'i8', 
                'f8', 'f8'])
        self.areagps = gpsDat['areagps']
#        self.blockgps = gpsDat['blockgps']
        self.sitegps = gpsDat['sitegps']
        self.animalgps = gpsDat['animalgps']
        self.sexgps = gpsDat['sexgps']
        self.agegps = gpsDat['agegps']
        self.yeargps = gpsDat['yeargps']
        self.monthgps = gpsDat['monthgps']
        self.daygps = gpsDat['daygps']
        self.Xgps = gpsDat['xgps']
        self.Ygps = gpsDat['ygps']
        self.nGPS = len(self.Xgps)
        ## convert bytes to string
        self.areagps = decodeBytes(self.areagps, self.nGPS)
#        self.blockgps = decodeBytes(self.blockgps, self.nGPS)
        self.agegps = decodeBytes(self.agegps, self.nGPS)
        self.sexgps = decodeBytes(self.sexgps, self.nGPS)
#        print('area', self.areagps[:10], self.sitegps[:15])

        ## Read in trapping data
        trapDat = np.genfromtxt(self.params.inputTrapFname,  delimiter=',', names=True,
            dtype=['S32', 'i8', 'S32', 'i8', 'S10', 'S10', 'S10', 'S10', 'S10', 'i8', 'S10', 
                'f8', 'S10', 'S10', 'i8', 'i8', 'f8', 'f8', 'i8', 'i8', 'i8', 'i8'])
        self.sitetrap = trapDat['sitetrap']
#        self.trapnumber = trapDat['trapnumber']   
        self.trapID = trapDat['trapID']
        self.traptype = trapDat['traptype']
        self.setType = trapDat['setType']
        self.trapLayout = trapDat['trapLayout']
        self.initialTrap = trapDat['initialtrap']
#        self.projecttrap = trapDat['project']
        self.areatrap = trapDat['areatrap']
        self.nighttrap = trapDat['nighttrap']
        self.trapstatus = trapDat['trapstatus']
        self.animaltrap = trapDat['animaltrap']
        self.sextrap = trapDat['sextrap']
        self.agetrap = trapDat['agetrap']
        self.yeartrap = trapDat['yeartrap']
        self.monthtrap = trapDat['monthtrap']
        self.daytrap = trapDat['daytrap']
        self.Xtrap = trapDat['xtrap']
        self.Ytrap = trapDat['ytrap']
        self.julianDay = trapDat['julianDay']
        self.ntrap = len(self.Xtrap)
        ## convert bytes to string
#        self.blocktrap = decodeBytes(self.blocktrap, self.ntrap)
        self.agetrap = decodeBytes(self.agetrap, self.ntrap)
        self.sextrap = decodeBytes(self.sextrap, self.ntrap)
        self.traptype = decodeBytes(self.traptype, self.ntrap)
#        self.projecttrap = decodeBytes(self.projecttrap, self.ntrap)
        self.areatrap = decodeBytes(self.areatrap, self.ntrap)
        self.trapstatus = decodeBytes(self.trapstatus, self.ntrap)
        self.trapLayout = decodeBytes(self.trapLayout, self.ntrap)
#        self.trapnumber = decodeBytes(self.trapnumber, self.ntrap)
        self.setType = decodeBytes(self.setType, self.ntrap)
        self.initialTrap = decodeBytes(self.initialTrap, self.ntrap)
        ## make numeric variables of set and trap type
#        self.cage = np.where(self.traptype == 'Cage', 1.0, 0.0)     # cage vs leg
#        self.raised = np.where(self.setType == 'Raised', 1.0, 0.0)  # ground vs raised
        self.tauIndxFull = np.zeros(self.ntrap, dtype = int)
        mask = (self.setType == 'Raised ') & (self.traptype == 'Leghold')
        self.tauIndxFull[mask] = 2
        self.raisedLeg = np.where(mask, 1.0, 0.0)
        mask = (self.setType == 'Ground') & (self.traptype == 'Leghold')
        self.groundLeg = np.where(mask, 1.0, 0.0)
        self.tauIndxFull[mask] = 1

        ## READ IN HABITAT CLASS DATA
        habitatDat = np.genfromtxt(self.params.inputHabitatFname,  delimiter=',', names=True,
            dtype=['S32', 'S10'])
        self.areaHabitat = habitatDat['Area']
        self.habitatHabitat = habitatDat['Habitat']
        self.nArea = len(self.areaHabitat)
        self.areaHabitat = decodeBytes(self.areaHabitat, self.nArea)
        self.habitatHabitat = decodeBytes(self.habitatHabitat, self.nArea)
#        print('areaHabitat', self.areaHabitat)

        #########################################################
        ## READ IN HABITAT BY INDIVIDUAL; BIT CIRCULAR AS HR CENTRES CALC BEFORE
        hrHabitat = np.genfromtxt(self.params.inputHRHabitatsFname,  delimiter=',', 
            names=True, dtype=['i8', 'f8', 'f8', 'f8', 'f8', 'f8', 'f8'])
        self.pFarm = hrHabitat['pFarm']
        self.pScrub = hrHabitat['pScrub']
        self.pForest = hrHabitat['pForest']
        self.hrPossum = hrHabitat['Possum']


    def cullTooFewFixes(self):
        """
        cull out individuals with too few fixes - see model params file for number
        """
        uniqueAnimalGPS = np.unique(self.animalgps)
        self.nFix = np.zeros(len(uniqueAnimalGPS), dtype = int)
        keepMask = np.ones(self.nGPS, dtype = bool)
        cc = 0
        for i in uniqueAnimalGPS:
            animalMask = self.animalgps == i
            self.nFix[cc] = np.sum(animalMask)
            if self.nFix[cc] < self.params.minNumber:
                keepMask[animalMask] = 0
            cc += 1
        self.Xgps = self.Xgps[keepMask]
        self.Ygps = self.Ygps[keepMask]
        self.areagps = self.areagps[keepMask]        
        self.sitegps = self.sitegps[keepMask]
        self.animalgps = self.animalgps[keepMask]
        self.sexgps = self.sexgps[keepMask]
        self.agegps = self.agegps[keepMask]
        self.yeargps = self.yeargps[keepMask]
        self.monthgps = self.monthgps[keepMask]
        self.daygps = self.daygps[keepMask]
        self.nGPS = len(self.Xgps)
        self.uniqueArea = np.unique(self.areagps)
        self.nUniqueArea = len(self.uniqueArea)
        rmIndMask = self.nFix < self.params.minNumber
        self.nFix = self.nFix[~rmIndMask]
        print('nFix', self.nFix, 'nIndivid', len(self.nFix))
        print('uarea', self.uniqueArea)


    def makeSigmaCovariates(self):
        """
        make covariates for sigma model
        """
        self.uniqueAnimalGPS = np.unique(self.animalgps)
        self.nUniqueAnimal = len(self.uniqueAnimalGPS)
        self.nPossumByArea = np.zeros(self.nUniqueArea, dtype = int)
        self.pFarmScaled = (self.pFarm - np.mean(self.pFarm)) / np.std(self.pFarm)
        self.pForestScaled = (self.pForest - np.mean(self.pForest)) / np.std(self.pForest)
        self.pScrubScaled = (self.pScrub - np.mean(self.pScrub)) / np.std(self.pScrub)

        if (self.params.modelID == 20):
            self.sigmaCovar = np.zeros((self.nUniqueAnimal, 4))
            self.sigmaCovar[:, 1] = self.pForestScaled
        elif (self.params.modelID == 21):
            self.sigmaCovar = np.zeros((self.nUniqueAnimal, 4))
            self.sigmaCovar[:, 1] = self.pScrubScaled
        elif (self.params.modelID == 22):
            self.sigmaCovar = np.zeros((self.nUniqueAnimal, 4))
            self.sigmaCovar[:, 1] = self.pFarmScaled
        elif (self.params.modelID == 23):
            self.sigmaCovar = np.zeros((self.nUniqueAnimal, 5))
            self.sigmaCovar[:, 1] = self.pFarmScaled
            self.sigmaCovar[:, 2] = self.pScrubScaled
        elif (self.params.modelID == 24):
            ## GRASS WITHOUT AGE SEX
            self.sigmaCovar = np.zeros((self.nUniqueAnimal, 2))
            self.sigmaCovar[:, 1] = self.pFarmScaled
        elif (self.params.modelID == 25):
            ## NO HABITAT WITH AGE SEX
            self.sigmaCovar = np.zeros((self.nUniqueAnimal, 3))
        elif (self.params.modelID == 26):
            ## INTERCEPT ONLY MODEL: NO HABITAT, NO AGE SEX
            self.sigmaCovar = np.zeros((self.nUniqueAnimal, 1))
        ## ADD INTERCEPT
        self.sigmaCovar[:,0] = 1.0
        self.sigma = np.zeros(self.nUniqueAnimal)
        idArray = np.arange(self.nUniqueArea, dtype = int)
        self.individAreaIndx = np.zeros(self.nUniqueAnimal, dtype = int)
        self.densityByIndivid = np.zeros(self.nUniqueAnimal)
        self.uniqueAnimalAge = np.zeros(self.nUniqueAnimal, dtype=int)
        self.uniqueAnimalSex = np.zeros(self.nUniqueAnimal, dtype=int)
        for i in range(self.nUniqueAnimal):
            animalMask = self.animalgps == self.uniqueAnimalGPS[i]
            age  = self.agegps[animalMask][0]
            if age == 'I':
                if not self.params.noAgeSex:
                    self.sigmaCovar[i, -1] = 1.0                          # Adult = 0; juv = 1
                self.uniqueAnimalAge[i] = 1
            sex  = self.sexgps[animalMask][0]
            if sex == 'F':
                if not self.params.noAgeSex:
                    self.sigmaCovar[i, -2] = 1.0                          # Male = 0; Female = 1
                self.uniqueAnimalSex[i] = 1
            area = self.areagps[animalMask][0]
            hab = self.habitatHabitat[self.areaHabitat == area]
            ### populate density
            self.individAreaIndx[i] = idArray[self.areaHabitat == area]
            self.densityByIndivid[i] = self.D[self.individAreaIndx[i]]
            areaIndx = self.individAreaIndx[i]
            self.nPossumByArea[areaIndx] += 1

            ### INITIAL SIGMA
            self.sigma[i] = np.exp(np.random.normal(np.log(
                self.params.areaSigma[self.individAreaIndx[i]]), 0.05))
#            print('uniqueanimal', i, 'area', area, 'sigma_i', self.sigma[i]//1,
#                'indAreaIndx', self.individAreaIndx[i])

#            print('id', i, 'sex', sex, 'age', age)
#            print('i', 'area', area, 'indx', self.individAreaIndx[i], 
#                'density', self.densityByIndivid[i], 
#                'animalID', self.uniqueAnimalGPS[i], 'sigCov', self.sigmaCovar[i])
#        ## SCALE SIGMA COVARIATES
#        self.sigmaCovar = scaleX(self.sigmaCovar)
        self.k = np.exp(np.dot(self.sigmaCovar, self.b))
        self.predSigma = self.k / np.sqrt(self.densityByIndivid)
        ### INITIAL SIGMA
#        self.sigma = np.exp(np.random.normal(np.log(self.params.areaSigma), 0.1))
#        self.sigma = np.exp(np.random.normal(np.log(self.predSigma), 0.8))
#        self.sigma = np.random.uniform(26.0, 160., self.nUniqueAnimal)
        print('SigmaCovariates', self.sigmaCovar[-50:])
        
    def getHRCentre(self):
        """
        ## Calculate homerange x and y values as means
        ## Get delta x and y from HRC for all individuals
        """
        self.xHrCentre = np.zeros(self.nUniqueAnimal)
        self.yHrCentre = np.zeros(self.nUniqueAnimal) 
        self.deltaXHRC = []     # np.zeros(self.nUniqueAnimal)
        self.deltaYHRC = []     # np.zeros(self.nUniqueAnimal)
        ## loop individuals
        for i in range(self.nUniqueAnimal):
            ind_i = self.uniqueAnimalGPS[i]
            iMask = self.animalgps == ind_i
            x_i = self.Xgps[iMask]
            y_i = self.Ygps[iMask]
            self.xHrCentre[i] = np.mean(x_i)
            self.yHrCentre[i] = np.mean(y_i)
            deltaX = self.xHrCentre[i] - x_i
            deltaY = self.yHrCentre[i] - y_i
            self.deltaXHRC.append(deltaX)
            self.deltaYHRC.append(deltaY)
#            if i == 0:
#                print('ind', ind_i, 'x', x_i[:10], 'xhr', self.xHrCentre[i], 
#                    'deltaX', deltaX[:10], 'deltaXHRC', self.deltaXHRC[0][:10])
#            print('ind', ind_i, 'xy', self.xHrCentre[i], self.yHrCentre[i])
#        print('xhrcentre', self.xHrCentre, self.yHrCentre)
#        print('deltXy', self.deltaXHRC[0], self.deltaYHRC[0])
        ## WRITE HR CENTRES TO TEXT FILE
        structured = np.empty((self.nUniqueAnimal,), dtype=[('Possum', np.float),
                ('xHR', np.float), ('yHR', np.float)])
        # copy data over
        structured['Possum'] = self.uniqueAnimalGPS
        structured['xHR'] = self.xHrCentre
        structured['yHR'] = self.yHrCentre
        np.savetxt(self.params.individHRCentreFname, structured, fmt=['%.1f', '%.2f', '%.2f'],  
                    comments = '', delimiter = ',', 
                    header='Possum, xHR, yHR')

        

   
                    
    def makeDistList(self):
        """
        ## make distance matrices for each site and put in list
        """
        onlySigmaData = [106, 107, 112, 113, 115, 117, 118, 121, 123, 125,
            136, 137, 140, 141, 142,
            274, 299, 300, 301] # no trap data for these possums

        self.onlySigmaMask = ~np.in1d(self.uniqueAnimalGPS, onlySigmaData)
        self.moveCaptMask = ~np.in1d(self.uniqueAnimalGPS, onlySigmaData)
        self.uniqueMoveCapt = self.uniqueAnimalGPS[self.moveCaptMask]
        self.nUniqueMoveCapt = len(self.uniqueMoveCapt)

        self.distByIndividList = []
        self.trapByIndividList = []
        self.groundLegList = []
        self.raisedLegList = []
        self.tauIndxList = []
        for i in range(self.nUniqueAnimal):
            if ~self.moveCaptMask[i]:
                continue

            xHRC_j = self.xHrCentre[i]
            yHRC_j = self.yHrCentre[i]
            ## trap info in site j
            areaIndx_i = self.individAreaIndx[i]
            area_i = self.areaHabitat[areaIndx_i]
            areaMask = self.areatrap == area_i
            ## Get trap data from area i
            all_j_traps = self.trapID[areaMask]     # all traps in area
            trap_j = np.unique(all_j_traps)             # unique traps in area
            nTraps_j = len(trap_j)                      # n unique in area
            xAreaTrap = self.Xtrap[areaMask]            # all x-y in area
            yAreaTrap = self.Ytrap[areaMask]
            groundLegAreaTrap = self.groundLeg[areaMask]
            raisedLegAreaTrap = self.raisedLeg[areaMask]
            tauIndxArea = self.tauIndxFull[areaMask]
            # loop thru traps to get x and y location
            xtrap = np.zeros(nTraps_j)
            ytrap = np.zeros(nTraps_j)
            groundLeg_ij = np.zeros(nTraps_j, dtype=float)
            raisedLeg_ij = np.zeros(nTraps_j, dtype=float)
            tau_ij = np.zeros(nTraps_j, dtype=int)
            for j in range(nTraps_j):
                j_trapMask = all_j_traps == trap_j[j]
                xtrap[j] = xAreaTrap[j_trapMask][0]     # get 1 X and Y for trap j
                ytrap[j] = yAreaTrap[j_trapMask][0] 
                groundLeg_ij[j] = groundLegAreaTrap[j_trapMask][0]
                raisedLeg_ij[j] = raisedLegAreaTrap[j_trapMask][0]
                tau_ij[j] = tauIndxArea[j_trapMask][0]
            distMat = distFX(xHRC_j, yHRC_j, xtrap, ytrap)      # dist betw i and all traps j
            keepDistMask = distMat <= self.params.maxTrapDist   # mask of dist to keep

            ## Make dist list
            distMat = distMat[keepDistMask]
            self.distByIndividList.append(distMat**2)              # dist ind to traps 

            ## Make trap list
            trapIndivid = trap_j[keepDistMask]
            self.trapByIndividList.append(trapIndivid)

            ## Make raised and ground leg list for alpha covariates
            raisedLegInd = raisedLeg_ij[keepDistMask]
            groundLegInd = groundLeg_ij[keepDistMask]
            tauIndxInd = tau_ij[keepDistMask]
            self.groundLegList.append(groundLegInd)
            self.raisedLegList.append(raisedLegInd)
            self.tauIndxList.append(tauIndxInd)
#            print('animal', i, 'area', area_i, 'nraised', np.sum(raisedLegInd),
#                'nground', np.sum(groundLegInd), 'self.raisedLeg', np.sum(self.raisedLeg))


        ## CONVERT TO NUMPY ARRAYS
#        self.distByIndividList = np.array(self.distByIndividList)
#        self.trapByIndividList = np.array(self.trapByIndividList)
#        self.groundLegList = np.array(self.groundLegList)
#        self.raisedLegList = np.array(self.raisedLegList)


    def makeCaptureArrays(self):
        """
        ## Make arrays to calc and store pCapt, capt data, nights, status, prev capt
        ## All by individual
        """
        self.pCaptList = []         # list of 2-d night by trap, add 1 col for not capt
        self.statusList = []        # list of 2-d trap status night by trap 
        self.captDataList = []      # list 2-d of capture data add 1 col for not capt
        self.prevCaptList = []      # list 1-d of previous capture by individ.
        self.MNP = []               # list multinomial probabilities
        self.nJulByIndivid = np.zeros(self.nUniqueMoveCapt, dtype = int)
        cc = 0
        for i in range(self.nUniqueAnimal):
            if ~self.moveCaptMask[i]:
                continue
            ntraps_i = len(self.distByIndividList[cc])
            ind_i = self.uniqueAnimalGPS[i]
            areaIndx_i = self.individAreaIndx[i]
            area_i = self.areaHabitat[areaIndx_i]
            areaMask = self.areatrap == area_i
            traps_i = self.trapByIndividList[cc]
            nTraps_i = len(traps_i)
            # find min jul that ind_i was captured
            captMask = self.animaltrap == ind_i
            recentJulCapt = self.julianDay[captMask]

            allTrapsMask = np.in1d(self.trapID, traps_i)
            uniqueJul_ij = np.unique(self.julianDay[allTrapsMask])  # all jul for area
            nJul_ij = len(uniqueJul_ij)
            self.nJulByIndivid[cc] = nJul_ij

            self.pCaptList.append(np.zeros((nJul_ij, nTraps_i)))   # add 1 non capt
            self.MNP.append(np.zeros((nJul_ij, (nTraps_i + 1))))   # add 1 non capt
            self.captDataList.append(np.zeros((nJul_ij, (nTraps_i + 1)), dtype = int))
            self.statusList.append(np.zeros((nJul_ij, nTraps_i), dtype = float))   
            self.prevCaptList.append(np.zeros((nJul_ij, nTraps_i), dtype = float))
#            self.lambda0.append(np.zeros(nTraps_i, dtype = float))
            ## LOOP THRU TRAP AND NIGHTS TO POPULATE ARRAYS
            for j in range(nTraps_i):
                trap_ij = traps_i[j]
                trapMask = self.trapID == trap_ij
                areaTrapMask = areaMask & trapMask
                jul_ij = self.julianDay[areaTrapMask]           #all jul for trap and area
                if (trap_ij == 672) and (ind_i == 288):
                    tindx = j
                if (trap_ij == 80) and (ind_i == 288):
                    tindx2 = j
                # LOOP THRU JUL IN AREA i AND TRAP J
                for k in jul_ij:
                    julAreaTrapMask = (self.julianDay == k) & areaTrapMask
                    rowMask = uniqueJul_ij == k

                    if np.sum(julAreaTrapMask) >1:
                        print('Prob', 'ind', ind_i, 'area', area_i, 'trap', trap_ij,
                            'jul', k, 'all jul', jul_ij)

                    if self.animaltrap[julAreaTrapMask] == ind_i:
                        self.captDataList[cc][rowMask, j] = 1     # pop. capture data
                    julDiff = (k - recentJulCapt)
                    selec = julDiff[julDiff > 0]
                    nSelec = len(selec)
                    if nSelec > 0:
                        minSelec = np.min(selec)
                        if (minSelec < self.params.minTimeIndep):
                            self.prevCaptList[cc][rowMask, j] = 1   # pop prevCapture data 
                    ### POPULATE THE STATUS ARRAYS
                    if self.trapstatus[julAreaTrapMask] == 'SS':
                        self.statusList[cc][(rowMask), j] = 1.0
                    ## IF CAPT IND I, THEN MAKE AVAILABLE ALL NIGHT
                    elif (self.animaltrap[julAreaTrapMask] == ind_i):
                        self.statusList[cc][rowMask, j] = 1.0     
                    elif self.trapstatus[julAreaTrapMask] == 'NT':
                        self.statusList[cc][(rowMask), j] = 0.5
                    elif self.trapstatus[julAreaTrapMask] == 'POS':
                        self.statusList[cc][(rowMask), j] = 0.5
                    elif self.trapstatus[julAreaTrapMask] == 'POS RECAP':
                        self.statusList[cc][(rowMask), j] = 0.5
                    elif self.trapstatus[julAreaTrapMask] == 'SP':
                        self.statusList[cc][(rowMask), j] = 0.5
                    elif self.trapstatus[julAreaTrapMask] == 'ESC':
                        self.statusList[cc][(rowMask), j] = 0.5
            cc += 1
#        ## CONVERT TO NUMPY ARRAYS
#        self.pCaptList = np.array(self.pCaptList)
#        self.statusList = np.array(self.statusList)
#        self.captDataList = np.array(self.captDataList)
#        self.prevCaptList = np.array(self.prevCaptList)
#        self.lambda0 = np.array(self.lambda0)

#        print('Ind 288 first capture ############################################')
#        print('traplist', self.trapByIndividList[indIndx][(tindx - 2): (tindx +2)])                        
#        print('prev', self.prevCaptList[indIndx][:, (tindx - 2): (tindx +2)], 
#            'capt', self.captDataList[indIndx][:, (tindx - 2): (tindx +2)],
#            'status',  self.statusList[indIndx][:, (tindx - 2): (tindx +2)])
#        print('Ind 288 Second capture ############################################')
#        print('traplist', self.trapByIndividList[indIndx][: (tindx2 +4)])                        
#        print('prev', self.prevCaptList[indIndx][:, : (tindx2 +4)], 
#            'capt', self.captDataList[indIndx][:, : (tindx2 +4)],
#            'status',  self.statusList[indIndx][:, : (tindx2 +4)])

    def getNonCaptData(self):
        """
        populate self.captDataList with non-captures (last column)
        """
        for i in range(self.nUniqueMoveCapt):
            capt_i = np.sum(self.captDataList[i], axis = 1)
#            print('i', i, 'poss', self.uniqueMoveCapt[i], 'capt_i', capt_i)
            if np.max(capt_i) > 1:
                print('problem: too many captures')
            self.captDataList[i][:, -1] = (1 - capt_i)
#        print('captdat', self.captDataList[0][:, -5:])

    def predictLambda0(self):
        """
        Calc a0 to scale sigma to lambda0
        intercept, raisedLeg, groundLeg
        """
        self.nCaptureInd = np.sum(self.moveCaptMask)
        ### INTIAL INDIVID EFFECT ON G0 VALUES BY INDIVIDUAL
        self.delta = np.random.uniform(-0.2, 0.2, self.nCaptureInd)
#        print('n captureInd', self.nUniqueMoveCapt)
        ## Array to hold the pre-tau pcapt
        self.preTauPCAPT = deepcopy(self.pCaptList) 
        # sigma squared for captured individuals
        self.sigmaTrapSq = (self.sigma[self.moveCaptMask])**2
        # sigma log for captured individuals
        self.logsigma = np.log(self.sigma[self.moveCaptMask])
        self.lambda0 = []
        ## for storage of likelihoods of alpha
        self.alphaLPMF = np.zeros(self.nCaptureInd)
        ## calculate initial prob of capture
        for i in range(self.nCaptureInd):
            # initial lambda0
            if (self.params.modelID >= 20):
                ### Include cage, raised and ground leg hold
                ll0 = np.array(self.alpha[0] + (self.logsigma[i] * self.alpha[1]) + 
                    (self.raisedLegList[i] * np.array(self.alpha[2])) + 
                    (self.groundLegList[i] * np.array(self.alpha[3])) + self.delta[i])
            ## Include ground vs raised only
            elif (self.params.modelID == 3) | (self.params.modelID == 6):
                ll0 = np.array(self.alpha[0] + (self.logsigma[i] * self.alpha[1]) + 
                    (self.raisedLegList[i] * self.alpha[2]) + self.delta[i])
            elif (self.params.modelID == 4) | (self.params.modelID == 5):
                ll0 = np.array(self.alpha[0] + (self.logsigma[i] * self.alpha[1]) + 
                    self.delta[i])
            self.lambda0.append(inv_logit(ll0))
            ## GET PROBABILITY OF CAPTURE
            pcapt = (self.lambda0[i] * np.exp(-self.distByIndividList[i] / 
                    2.0 / self.sigmaTrapSq[i])) 
            self.preTauPCAPT[i] = pcapt
            ## STATUS AFTER TAU EFFECT

            pcapt = (1 - (1- (pcapt**(self.tau * self.prevCaptList[i])) *
                (pcapt**(1.0 - self.prevCaptList[i])))**self.statusList[i])
#            pcapt = (1 - (1- (pcapt**
#                (self.tau[self.tauIndxList[i]] * self.prevCaptList[i])) *
#                (pcapt**(1.0 - self.prevCaptList[i])))**self.statusList[i])

            self.pCaptList[i] = pcapt
            ## GET MULTINOMIAL INITIAL PROBABILITIES
            # Total prob TP
            TP = np.expand_dims(1.0 - np.prod((1.0 - pcapt), axis = 1), 1)
            sumPCapt = np.sum(pcapt, axis =1)
            sumPCapt = np.where(sumPCapt < 1E-280, 1E-280, sumPCapt)
            # Normalised probabilities
            NP = pcapt / np.expand_dims(sumPCapt, 1)
            # Multinomial probabilities
            MNP = NP * TP
            # Prob of non-capture
            pNC = 1.0 - TP
            # populate array
            self.MNP[i][:, :-1] = MNP
            self.MNP[i][:, -1] = pNC[:, 0]
            captProbs = self.MNP[i][self.captDataList[i] == 1]
            captProbs = np.where(captProbs < 1E-280, 1E-280, captProbs)
            self.alphaLPMF[i] = np.sum(np.log(captProbs))
#        print('pcapt', np.shape(self.pCaptList[0]), self.pCaptList[0][:, :5], 
#                'alphaLPMF', self.alphaLPMF[:20])
        self.pCaptList_s = deepcopy(self.pCaptList)     
        self.MNP_s = deepcopy(self.MNP)
        self.preTauPCAPT_s = deepcopy(self.preTauPCAPT)
        self.alphaLPMF_s = np.zeros(self.nCaptureInd)
        self.lambda0_s = deepcopy(self.lambda0)
#        print('pCaptList', self.pCaptList[0][:,:6], 'lambda0', self.lambda0[0][:6])   

    def getNIndividByArea(self):
        self.nJulBySite = np.zeros(self.nUniqueArea, dtype = int)
        self.nIndividByArea = np.zeros(self.nUniqueArea, dtype=int)
        for i in range(self.nUniqueArea):
            mask_i = self.individAreaIndx == i
            n_i = np.sum(mask_i)
            self.nIndividByArea[i] = n_i
            ## GET N JUL BY SITE FOR UNOBSERVED DENSITY
            area_i = self.areaHabitat[i]
            areaMask = self.areatrap == area_i

            julArea_i = self.julianDay[areaMask]
            uniqueJul_i = np.unique(julArea_i)  # all jul for area
            nJul_i = len(uniqueJul_i)
            self.nJulBySite[i] = nJul_i
            print('area', i, 'n area ind', n_i, 'n Jul', nJul_i)



    def makeAreaRasters(self):
        """
        ## FOR RASTER CELLS CALC DISTANCE TO NEAREST TRAP
        ## USE FOR CALCULATING AREA IN DENSITY CALC
        """
        ## self.maxSigma = 340
        self.areaDistances = []
        for i in range(self.nUniqueArea):
            ## trap info in site j
#            areaIndx_i = self.individAreaIndx[i]
            area_i = self.areaHabitat[i]
            areaMask = self.areatrap == area_i
            x_area = self.Xtrap[areaMask]
            y_area = self.Ytrap[areaMask]
            ulx = np.min(x_area) - (2.0 * 4.0 * self.params.areaSigma[i])
            uly = np.max(y_area) + (2.0 * 4.0 * self.params.areaSigma[i])
            lrx = np.max(x_area) + (2.0 * 4.0 * self.params.areaSigma[i])
            lry = np.min(y_area) - (2.0 * 4.0 * self.params.areaSigma[i])
            area = (lrx - ulx) * (uly - lry) 
#            print('i', i, 'area', area / 1E4)
            ## LOOP THRU ALL CELLS TO GET DISTANCE
            y_j = uly
            area_i_Dist = []
#            cc = 0
            while y_j >= lry:
                x_j = ulx
                while x_j <= lrx:
                    d_ij = distFX(x_j, y_j, x_area, y_area)
                    minDist = np.min(d_ij)
                    area_i_Dist.append(minDist)
#                    if i == 1:
#                        print(cc, 'i', i, 'x', x_j, 'lrx', lrx, 'y_j', y_j, 
#                            'lry', lry, 'minD', minDist)
                    x_j += 100.0
#                    cc += 1
                y_j -= 100.0
            self.areaDistances.append(area_i_Dist) 
#            print('cc', cc, 'area_i', area_i, 'narea', self.nArea, 'uarea', 
#               self.nUniqueArea)

    def getInitialAreaBySite(self):
        """
        ## GET INITIAL AREA FOR ALL SITES USING SIGMA FROM PARAMS
        """
        self.nPresentBySite = np.zeros(self.nUniqueArea, dtype = int)
        self.nPresUnObserved = np.zeros(self.nUniqueArea, dtype = int)
        self.nTotalUnObserved = np.zeros(self.nUniqueArea, dtype = int)
        self.nCollaredBySite = np.zeros(self.nUniqueArea, dtype = int)
        self.areaBySite = np.zeros(self.nUniqueArea)
        self.areaBySite_s = np.zeros(self.nUniqueArea)
        self.areaMeanSigma = np.zeros(self.nUniqueArea)
        possMask = self.trapstatus == 'POS'
        idMask = np.isnan(self.animaltrap)            
        self.nUnCollaredCaptured = np.zeros(self.nUniqueArea, dtype = int)
        for i in range(self.nUniqueArea):
            sig_i = self.sigma[self.individAreaIndx == i]
            self.areaMeanSigma[i] = np.mean(sig_i)
            dist_i = 4.0 * self.areaMeanSigma[i]
            ha_i = np.sum(self.areaDistances[i] <= dist_i)
            self.areaBySite[i] = ha_i
            ## GET N COLLARED INDIVIDUALS BY SITE
            self.nCollaredBySite[i] = np.sum(self.individAreaIndx == i)
            ## GET NUMBER OF CAPTURED BUT UNCOLLARED POSSUMS BY SITE
            siteMask = self.areatrap == self.uniqueArea[i]
            nonCollarMask = siteMask & possMask & idMask
            self.nUnCollaredCaptured[i] = np.sum(nonCollarMask)
            ## GET NUMBER OF UNOBSERVED INDIVIDUALS BY SITE
            ## TOTAL PRESENT BY SITE
            self.nPresentBySite[i] = np.round(self.D[i] * self.areaBySite[i], 0)
            ## NUMBER PRESENT BUT UNOBSERVED
            self.nPresUnObserved[i] = (self.nPresentBySite[i] - self.nCollaredBySite[i] - 
                self.nUnCollaredCaptured[i])
            ## NUMBER ABSENT BUT POTENTIALLY PRESENT
            absentUnObs = np.round(0.7 * self.nPresentBySite[i], 0)
            ## TOTAL PRESENT AND POTENTIALLY PRESENT
            self.nTotalUnObserved[i] = self.nPresentBySite[i] + absentUnObs
#            print('area by site', ha_i, 'meanSig', self.areaMeanSigma[i]//1,
#                'paramsSig', self.params.areaSigma[i], 
#                'indxArea', i, 'area',  self.uniqueArea[i], 'nCollar', self.nCollaredBySite[i],
#                'nUnObs', self.nTotalUnObserved[i], 'dens', self.D[i],
#                'nUnCollar', self.nUnCollaredCaptured[i], 'pres unobser',
#                self.nPresUnObserved[i], 'nAbsent', absentUnObs)
            

    def getInitialPresence(self):
        """
        ## GET INITIAL PRES / ABS OF UNOBSERVED INDIVIDUALS.
        """
        self.presIndicator = []
        for i in range(self.nUniqueArea):
            pres_i = np.zeros(self.nTotalUnObserved[i])
            pres_i[:self.nPresUnObserved[i]] = 1
            self.presIndicator.append(pres_i)
#            print('i', i, 'pres ind', pres_i, 'npres', np.sum(pres_i), 'tot unObs', len(pres_i))
#        self.presIndicator = np.array(self.presIndicator)        

    def getTrapDistUnObserved(self):
        """
        ## GET DIST TO TRAPS FROM ALL UNOBSERVED INDIVIDUALS (WITHIN 6 SIGMA)
        ## MAKE ARRAYS WITH COVARIATES FOR LAMBDA0 FOR UNOBSERVED
        """
        ## EMPTY 2-D LIST TO POPULATE WITH DISTANCES
        self.groundLegUnObserved = []       ## IF NOT LEG, THEN CAGE
        self.raisedLegUnObserved = []
        self.minDistUnObserved = []
        self.distance2UnObserved = []
        self.trapNightUnObs = []
        self.statusUnObserved = []
        ## NUMBER CAPTURED BY DAY AT EACH SITE
        self.nCapturedAreaJul = []
        possMask = self.trapstatus == 'POS'
        ## LOOP AREAS
        for i in range(self.nUniqueArea):
            ## GET TRAP IN AREA
            area_i = self.areaHabitat[i]
            areaMask = self.areatrap == area_i
            ## Get trap data from area i
            all_j_traps = self.trapID[areaMask]         # all traps in area
            trap_j = np.unique(all_j_traps)             # unique traps in area
            nTraps_j = len(trap_j)                      # n unique in area
            xAreaTrap = self.Xtrap[areaMask]            # all x-y in area
            yAreaTrap = self.Ytrap[areaMask]
            groundLegAreaTrap = self.groundLeg[areaMask]
            raisedLegAreaTrap = self.raisedLeg[areaMask]
            # loop thru traps to get x and y location
            xtrap = np.zeros(nTraps_j)
            ytrap = np.zeros(nTraps_j)
            groundLeg_ij = np.zeros(nTraps_j)
            raisedLeg_ij = np.zeros(nTraps_j)
            for j in range(nTraps_j):
                j_trapMask = all_j_traps == trap_j[j]
                xtrap[j] = xAreaTrap[j_trapMask][0]     # get 1 X and Y for trap j
                ytrap[j] = yAreaTrap[j_trapMask][0] 
                groundLeg_ij[j] = groundLegAreaTrap[j_trapMask][0]
                raisedLeg_ij[j] = raisedLegAreaTrap[j_trapMask][0]
            ## ASSIGN FOCAL TRAPS TO EACH POTENTIAL UNOBSERVED POSSUM
            randPossTrap = np.random.choice(nTraps_j, self.nTotalUnObserved[i])
            deltaDist = 1.5 * 4.0 * self.params.areaSigma[i]
            deltaHypotenuse = np.sqrt(2.0 * deltaDist**2)
            deltaX = np.random.randint(-deltaDist, deltaDist, 
                    self.nTotalUnObserved[i])
            deltaY = np.random.randint(-deltaDist, deltaDist, 
                    self.nTotalUnObserved[i])
            minDist_k = []
            distTrap2_k = []
            groundLegArea_i = []
            raisedLegArea_i = []
            ## GET NUMBER CAPTURED BY AREA AND JULIAN 
            allTrapMask_i = np.in1d(self.trapID, trap_j)
            uniqueJul_i = np.unique(self.julianDay[allTrapMask_i])
            nJul_i = len(uniqueJul_i)
            nCaptArea_i = np.zeros(nJul_i)
            areaPossMask = areaMask & possMask
            julArea = self.julianDay[areaMask]
            for jul in range(nJul_i):
                julMask = self.julianDay == uniqueJul_i[jul]
                areaJulPossMask = areaPossMask & julMask 
                nCaptArea_i[jul] = np.sum(areaJulPossMask)
            ## APPEND N CAPT BY JUL FOR AREA (ARRAY IN LIST)
            self.nCapturedAreaJul.append(nCaptArea_i)
            ## trap status data for area i
            trapStatusArea_i = self.trapstatus[areaMask]
            ## EMPTY STATUS LIST FOR AREA I
            statusUnObsArea_i = []
            ## LOOP INDIVIDUALS AND AND GET DISTANCES
            for k in range(self.nTotalUnObserved[i]):
                randPossTrap_k = randPossTrap[k]
                xhrc_k = xtrap[randPossTrap_k] + deltaX[k]
                yhrc_k = ytrap[randPossTrap_k] + deltaY[k]
                ## DISTANCE FROM POSS K TO ALL TRAPS
                distMat = distFX(xhrc_k, yhrc_k, xtrap, ytrap) # dist betw k and traps
                keepDistMask = distMat <= deltaHypotenuse # mask of dist to keep
                distMat = distMat[keepDistMask]
                minDist_k.append(np.min(distMat))
                distTrap2_k.append(distMat**2)              # dist ind to traps 
                ## Make raised and ground leg list for individ k for alpha covariates
                raisedLegInd = raisedLeg_ij[keepDistMask]
                groundLegInd = groundLeg_ij[keepDistMask]
                raisedLegArea_i.append(raisedLegInd)
                groundLegArea_i.append(groundLegInd)
                ## GET TRAP STATUS DATA FOR UN-OBS
                trapArea_ik = trap_j[keepDistMask]
                uTrapArea_ik = np.unique(trapArea_ik)
                nUTrapArea_ik = len(uTrapArea_ik)

                ## APPEND 2-D ARRAY FOR STATUS OF TRAPS FOR INDIVID K
                statusUnObsArea_i.append(np.zeros((nJul_i, nUTrapArea_ik), dtype=float))
                ## LOOP THRU TRAPS IN AREA I FOR INDIVID K
                for j in range(nUTrapArea_ik):
                    j_trap = uTrapArea_ik[j]
                    j_trapMask = all_j_traps == j_trap
                    statusTrap_ij = trapStatusArea_i[j_trapMask]
                    julAreaTrap = julArea[j_trapMask]
                    if len(statusTrap_ij) > nJul_i:
                        print('DIFF JULIAN DAYS FROM N TRAPNIGHTS; AREA', i, 
                            'TRAP', j_trap, 'njulSite', nJul_i, 
                            'nTrapjul', len(statusTrap_ij))
                    # LOOP THRU JULIAN DAYS FOR SITE I
                    for mm in range(nJul_i):
                        jul_mm = uniqueJul_i[mm]
                        julTrapMask = julAreaTrap == jul_mm
                        if np.sum(julTrapMask) == 1:
                            status_mm = statusTrap_ij[julTrapMask]
                            if status_mm == 'SS':
                                statusUnObsArea_i[k][mm, j] = 1.0
                            if status_mm == 'NT':
                                statusUnObsArea_i[k][mm, j] = 0.5
                            if status_mm == 'POS':
                                statusUnObsArea_i[k][mm, j] = 0.5
                            if status_mm == 'POS RECAP':
                                statusUnObsArea_i[k][mm, j] = 0.5
                            if status_mm == 'SP':
                                statusUnObsArea_i[k][mm, j] = 0.5
                            if status_mm == 'ESC':
                                statusUnObsArea_i[k][mm, j] = 0.5
                        elif np.sum(julTrapMask) > 1:
                            print('Problem: multiple trap entries for one night')
            self.statusUnObserved.append(statusUnObsArea_i)
            self.minDistUnObserved.append(minDist_k)
            self.distance2UnObserved.append(distTrap2_k)    ## DISTANCE SQUARED TRAPS
            self.groundLegUnObserved.append(groundLegArea_i)
            self.raisedLegUnObserved.append(raisedLegArea_i)
            print('area i', i, 'last ind', j)

    def getIndJulStatusMask(self):
        """
        ## MAKE MASK OF DAYS TRAPS ARE AVAIL FOR INDIVIDUAL FOR D_UPDATE
        """
        self.indJulMask = []
        for i in range(self.nUniqueArea):
            areaMaskList = []
            for j in range(self.nTotalUnObserved[i]):
                julStatus = np.sum(self.statusUnObserved[i][j], axis = 1)
                mask_ij = julStatus > 0
                areaMaskList.append(mask_ij)
            self.indJulMask.append(areaMaskList)




    def getMeanSigmaSites(self):
        """
        ## GET MEAN SIGMA FOR EACH SITE TO BE USED IN DENSITY ESTIMATION
        ## VALUES APPLIED TO UNOBSERVED INDIVIDUALS.
        """
        self.meanSigmaArea = np.zeros(self.nUniqueArea)
        for i in range(self.nUniqueArea):
            ## AREA MASK LENGTH OF UNIQUE INDIVIDUALS
            areaMask = self.individAreaIndx == i
            ## get sigma values from individuals from area i
            sig_i = self.sigma[areaMask]         # all traps in area
            meanSig = np.mean(sig_i)
            ### populate meansig        ## ONE FOR EACH SITE/AREA
            self.meanSigmaArea[i] = meanSig
#            print('indx', i, 'len ind area indx', len(areaMask), 
#                'len sigma', len(self.sigma), 'mean sigma', meanSig)


    def initialPCaptUnObserved(self):
        """
        ## GET INITIAL PROB OF CAPT FOR UNOBSERVED POSSUMS
        """
        self.pNoCaptUnObserved = []     # np.zeros(np.sum(self.nTotalUnObserved))
        for i in range(self.nUniqueArea):
            ## LOG SIGMA FOR AREA I
            sig2_i = (self.areaMeanSigma[i])**2
            logsig_i = np.log(self.areaMeanSigma[i])
            pNoCaptArea_i = []
            for j in range(self.nTotalUnObserved[i]):
                ### Include cage, raised and ground leg hold
                ## LOGIT LAMBDA 0
                if (self.params.modelID >= 20):
                    ll0 = (self.alpha[0] + (logsig_i * self.alpha[1]) + 
                        (self.raisedLegUnObserved[i][j] * self.alpha[2]) + 
                        (self.groundLegUnObserved[i][j] * self.alpha[3]))
                elif (self.params.modelID == 3) | (self.params.modelID == 6):
                    ll0 = np.array(self.alpha[0] + (logsig_i * self.alpha[1]) + 
                        (self.raisedLegUnObserved[i][j] * self.alpha[2]))
                elif (self.params.modelID == 4) | (self.params.modelID == 5):
                    ll0 = np.array(self.alpha[0] + (logsig_i * self.alpha[1]))
                
                ## MAKE INTO A PROBABILITY
                lambda_0 = inv_logit(ll0)

                ## calculate initial prob of no capture
                pNoCapt = ((1.0 - (lambda_0 * np.exp(-self.distance2UnObserved[i][j] / 
                    2.0 / sig2_i)))**self.statusUnObserved[i][j])
#                pNoCapt = ((1.0 - (lambda_0 * np.exp(-self.distance2UnObserved[i][j] / 
#                    2.0 / sig2_i)))**self.trapNightUnObs[i][j])
                pNoCapt = np.prod(pNoCapt)
                pNoCaptArea_i.append(np.log(pNoCapt))
#                if j < 20:
#                    print('area', i, 'ind', j, 'pNoCapt', pNoCapt) 
            self.pNoCaptUnObserved.append(pNoCaptArea_i)
        self.llNoCapt = self.pNoCaptUnObserved
#        self.llNoCapt = np.log(self.pNoCaptUnObserved)
#        for i in range(self.nUniqueArea):
#            for j in range(self.nTotalUnObserved[i]):
#                print('i', i, 'j', j, 'llnocapt', self.llNoCapt[i][j])




########            Pickle results to directory
########

class GibbsResults(object):
    def __init__(self, modelparams, basicdata, mcmcobj):
        self.bgibbs = mcmcobj.bgibbs
        self.siggibbs = mcmcobj.siggibbs
        self.Dgibbs = mcmcobj.Dgibbs
        self.agibbs = mcmcobj.agibbs
        self.Egibbs = mcmcobj.Egibbs
        self.tgibbs = mcmcobj.tgibbs
        self.deltagibbs = mcmcobj.deltagibbs
        self.dicgibbs = mcmcobj.dicgibbs
        ## basicdata elements
        self.sigmaCovar = basicdata.sigmaCovar
        self.individAreaIndx = basicdata.individAreaIndx
        self.densityByIndivid = basicdata.densityByIndivid
        self.uniqueAnimalGPS = basicdata.uniqueAnimalGPS
        self.nUniqueAnimal = basicdata.nUniqueAnimal
        self.uniqueAnimalAge = basicdata.uniqueAnimalAge
        self.uniqueAnimalSex = basicdata.uniqueAnimalSex
        self.areaHabitat = basicdata.areaHabitat
        self.habitatHabitat = basicdata.habitatHabitat
        self.uniqueArea = basicdata.uniqueArea
        self.nUniqueArea = basicdata.nUniqueArea
        self.moveCaptMask = basicdata.moveCaptMask
        self.nCaptureInd = basicdata.nCaptureInd
        self.nFix = basicdata.nFix
        self.groundLegList = basicdata.groundLegList
        self.raisedLegList = basicdata.raisedLegList
        self.lambda0 = basicdata.lambda0
        self.areaDistances = basicdata.areaDistances
        self.modelID = modelparams.modelID
        ###





