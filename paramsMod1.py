#!/usr/bin/env python

########################################
########################################
# This file is part of TBfree funded project to quantify g0 and
# sigma parameters for possums across diverse habitats
# Copyright (C) 2016 Dean Anderson 
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# ADDITIONAL NOTES ABOUT THIS SCRIPT.
########################################
########################################

import os
import pickle
#import initiateModel
#import mcmcRun
import numpy as np


class ModelParams(object):
    """
    Contains the parameters for the mcmc run. This object is also required 
    for the pre-processing. 
    """
    def __init__(self):
        ############################################################
        ###################################### USER MODIFY HERE ONLY
        #################################
        
        ###################################################
        ## Set number of MCMC iterations, thin rate and burn in
        self.ngibbs = 5 #8000   # 2500      # number of estimates to save for each parameter
        self.thinrate = 1   #20   # 200      # thin rate
        self.burnin = 0          # burn in number of iterations

        ## If have not initiated parameters, set to 'True'   
        self.firstRun = True    ## True or False

        ###################################################


        ## array of iteration id to keep
        self.keepseq = np.arange(self.burnin, ((self.ngibbs * self.thinrate) + self.burnin),
            self.thinrate)

        # set paths to scripts and data
        self.inputDataPath = os.path.join(os.getenv('POFPROJDIR', default = '.'), 
            'possumg0sigma/g0sigmaData')
        self.outputDataPath = os.path.join(os.getenv('POFPROJDIR', default = '.'), 
            'possumg0sigma/mod1_Results')


#        if not os.path.isdir(self.outputDataPath):
#            os.mkdir(self.outputDataPath)
        ## set Data names
        self.basicdataFname = os.path.join(self.outputDataPath, 'basicdataMod1.pkl')
        self.gibbsFname = os.path.join(self.outputDataPath, 'gibbsMod1.pkl')
        self.paramsResFname = os.path.join(self.outputDataPath, 'parameterTableMod1.csv')
        self.sigG0TableFname = os.path.join(self.outputDataPath, 'goSigmaTableMod1.csv')
        self.densitySigPlotFname = os.path.join(self.outputDataPath, 'densitySigPlot.png')
        self.individAppendixFname = os.path.join(self.outputDataPath, 'individAppendix.csv')
        self.g0SigPlotFname = os.path.join(self.outputDataPath, 'g0SigPlot.png')
        self.onePossSseFname = os.path.join(self.outputDataPath, 'onePossSsePlot.png')
        self.onePossTrapPCaptFname = os.path.join(self.outputDataPath, 'onePossTrapPCaptPlot.png')

        ## Input data
        self.inputGPSFname = os.path.join(self.inputDataPath, 'moveData.csv')
        self.inputTrapFname = os.path.join(self.inputDataPath, 'trapData.csv')
        self.inputHabitatFname = os.path.join(self.inputDataPath, 'areaHabitats.csv')

        print('params basic path', self.basicdataFname) 




        ## Could add in a data dictionary for defining covariates - later

        
        ## Set initial parameter values
        ### sigma parameters: intercept, forest, farm, female, juvenile 
        self.b = np.array([5.0, 1.0, 1.0, -0.5,  -0.5])  # parameters on sigma
#        self.b = np.array([3.8, 1.0, -1.0, -0.5,  0.5])  # parameters on sigma
        self.nBeta = len(self.b)

#        # alpha para: intercept (cage), raised leg, ground leg
#        self.alpha = np.array([2.0, -0.75, 0.2, 0.2])           # alpha para for a0

        # alpha para: intercept (ground), raised
        self.alpha = np.array([2.0, -0.75, 0.2])           # alpha para for a0

        self.nAlpha = len(self.alpha)            
#        self.a = 900.0                   # intial mean a

        # prev capt parameter - tau
        self.tau = 1.7
        self.tauPriors = np.array([9.3333, 0.12])     # gamma priors
        self.tauSearch = 0.08    


        self.D = np.array([0.5, 1.5, 0.5, 7.0, 1.4, 5., 1.5, 2.5, 0.75,
                0.5, 0.75, 9.0, 5.0, .2, 7.0, 7.0, 7.0, 8.0])
        self.E = 50.0        # intial variance on sigma prediction

        ## Sigma Covariate Priors
        self.b_Priors = np.array([0.0, 10])
        self.searchBeta = 0.05
        self.alphaPriors = [0.0, 10.0]
#        self.alphaMeanPriors = [3800.0, 0.0, 0.0]
#        sd1000 = np.sqrt(1000)
#        self.alphaSDPriors = np.array([500.0, sd1000, sd1000])
        self.searchAlpha = 0.03    # [116.0, 35.0, 35.0]
#        self.searchAlpha = 116.0    # [116.0, 35.0, 35.0]
        self.searchSigma = 0.5
        self.E_Priors = [.01, 100.]                 # gamma priors on sigma error
        ###
        self.searchDelta = .075
        self.deltaPriors = [0.0, 0.75]

        ## Density GAMMA priors by area
        self.D_Priors = np.array([[3.0, 10.0], [4.0, 0.5], [3.0, 10.0], [18.5, 0.4],
            [4.0, 0.5], [11.0, 0.5], [4.0, 0.5], [6.0, 0.5], [4.33, 0.3], [2.2, 0.5],
            [11.0, 0.5], [23.5, 0.4], [11.0, 0.5], [3.0, 10.0], [18.5, 0.4], 
            [18.5, 0.4], [18.5, 0.4], [18.5, 0.4]])
        ## DENSITY NORMAL PRIORS
        self.dNormPrior = np.array([[0.5, 0.75], [1.5, 0.5], [0.5, 0.75], [7.0, 0.2],
            [1.4, 0.6], [5.0, 0.2], [1.5, 0.5], [2.5, 0.4], [0.75, 0.75], [0.5, 0.75],
            [0.75, 0.75], [9.0, .2], [5.0, 0.2], [0.2, 1.0], [7.0, .1], 
            [7.0, 0.1], [7.0, 0.1], [8.0, .1]])
        ## MAX SIGMA FOR MAKING BLANK RASTERS FOR CALCULATING DENSITY
        self.maxSigma = 340 
        
        



        # minimum number of gps fixes per individual
        self.minNumber = 12
        self.maxTrapDist = 1000.0  #550.0        
        # min time between 'independent trapping events'
        self.minTimeIndep = 20      # 2.5 weeks
        
        #################################
        ####################################### END USER MODIFICATION
        ##############################################################

