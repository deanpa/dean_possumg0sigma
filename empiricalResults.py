#!/usr/bin/env python


#import paramsPredators
import numpy as np
import pylab as P
from scipy.stats.mstats import mquantiles
import prettytable
import pickle
import os
import datetime
#from numba import jit

def logit(x):
    """
    Function (logit) to convert probability to real number
    """
    return np.log(x) - np.log(1 - x)

def inv_logit(x):
    """
    Function to convert real number to probability
    """
    return np.exp(x) / (1 + np.exp(x))

def distFX(x1, y1, x2, y2):
    """
    calc distance matrix between 2 sets of points
    resulting matrix dimensions = (len(x1), len(x2))
    """
    deltaX = np.add.outer(x1, -x2)
    deltaX_sq = deltaX**2
    deltaY = np.add.outer(y1, -y2)
    deltaY_sq = deltaY**2
    dist_2D = np.sqrt(deltaX_sq + deltaY_sq)
    return dist_2D


class ResultsProcessing(object):
    def __init__(self, gibbsobj, params, possumpath):

        self.params = params
        self.possumpath = possumpath
        self.gibbsobj = gibbsobj
        self.ndat = len(self.gibbsobj.siggibbs)
        self.results = np.hstack([self.gibbsobj.bgibbs,
                                self.gibbsobj.agibbs,
                                np.expand_dims(self.gibbsobj.Egibbs, 1),
                                np.expand_dims(self.gibbsobj.tgibbs, 1),
                                self.gibbsobj.siggibbs,
                                self.gibbsobj.Dgibbs])
        self.npara = self.results.shape[1]


        print('##############################################')
        print('######   DIC')
        print('Mean DIC =', np.mean(self.gibbsobj.dicgibbs))
        print('0.05, 0.5, 0.95 CI =', 
            mquantiles(self.gibbsobj.dicgibbs, prob=[0.05, 0.5, 0.95]))    
        print('##############################################')




        print('n paras', self.npara)
        print('total iter', ((self.ndat * self.params.thinrate) + 
                self.params.burnin))
        print('thin = ', self.params.thinrate, 'burn = ', self.params.burnin)

        print('delt priors', self.params.deltaPriors)
        self.siggibbs = self.gibbsobj.siggibbs

        print('Unique Areas', self.gibbsobj.uniqueArea)

    def makeTableFX(self):
        self.ncol = (np.shape(self.gibbsobj.bgibbs)[1] + 
                    np.shape(self.gibbsobj.agibbs)[1] + 2)
#        self.names = np.array(['Beta_inter', 'Forest', 'Drylands', 'Female', 'Infant',
#            'a0Inter', 'a1LogSig', 'a2RaisedLeg', 'Sigma_error', 'Tau'])

        if self.params.modelID == 20:       
            self.names = np.array(['Beta_inter', 'pForest', 'Female', 'Infant',
                'a0Inter', 'a1LogSig', 'a2RaisedLeg', 'a3GroundLeg', 'Sigma_error', 'Tau'])
        elif self.params.modelID == 21:       
            self.names = np.array(['Beta_inter', 'pScrub', 'Female', 'Infant',
                'a0Inter', 'a1LogSig', 'a2RaisedLeg', 'a3GroundLeg', 'Sigma_error', 'Tau'])
        if self.params.modelID == 22:       
            self.names = np.array(['Beta_inter', 'pFarm', 'Female', 'Infant',
                'a0Inter', 'a1LogSig', 'a2RaisedLeg', 'a3GroundLeg', 'Sigma_error', 'Tau'])
        if self.params.modelID == 23:       
            self.names = np.array(['Beta_inter', 'pFarm', 'pScrub', 'Female', 'Infant',
                'a0Inter', 'a1LogSig', 'a2RaisedLeg', 'a3GroundLeg', 'Sigma_error', 'Tau'])
        if self.params.modelID == 24:       
            self.names = np.array(['Beta_inter', 'pFarm',
                'a0Inter', 'a1LogSig', 'a2RaisedLeg', 'a3GroundLeg', 'Sigma_error', 'Tau'])
        if self.params.modelID == 25:       
            self.names = np.array(['Beta_inter', 'Female', 'Infant',
                'a0Inter', 'a1LogSig', 'a2RaisedLeg', 'a3GroundLeg', 'Sigma_error', 'Tau'])
        if self.params.modelID == 26:       
            self.names = np.array(['Beta_inter',
                'a0Inter', 'a1LogSig', 'a2RaisedLeg', 'a3GroundLeg', 'Sigma_error', 'Tau'])
        elif self.params.modelID == 3:       
            self.names = np.array(['Beta_inter', 'Female', 'Infant',
                'a0Inter', 'a1LogSig', 'a2RaisedLeg', 'Sigma_error', 'Tau'])
        elif self.params.modelID == 4:       
            self.names = np.array(['Beta_inter', 'Drylands', 'Female', 'Infant',
                'a0Inter', 'a1LogSig', 'Sigma_error', 'Tau'])
        elif self.params.modelID == 5:       
            self.names = np.array(['Beta_inter', 'Female', 'Infant',
                'a0Inter', 'a1LogSig', 'Sigma_error', 'Tau'])
        elif self.params.modelID == 6:       
            self.names = np.array(['Beta_inter', 'Drylands', 'Female', 'Infant',
                'a0Inter', 'a1LogSig', 'a2RaisedLeg', 'Sigma_error', 'Tau'])
#        elif self.params.modelID == 7:       
#            self.names = np.array(['Beta_inter', 'Drylands', 'Female', 'Infant',
#                'a0Inter', 'a1LogSig', 'a2RaisedLeg', 'a3GroundLeg', 'Sigma_error', 
#                'TauCage', 'TauGround', 'TauRaised'])

        resultTable = np.zeros(shape = (3, self.ncol))
        resultTable[0] = np.round(np.mean(self.results[:, :self.ncol], axis = 0), 3)
        resultTable[1:3] = np.round(mquantiles(self.results[:, :self.ncol], 
                prob=[0.0275, 0.975], axis = 0), 3)
        resultTable = resultTable.transpose()
        aa = prettytable.PrettyTable(['Names', 'Mean', 'Low CI', 'High CI'])
        for i in range(self.ncol):
            name = self.names[i]
            row = [name] + resultTable[i].tolist()
            aa.add_row(row)
        print(aa)
        self.summaryTable = resultTable.copy()
   

    def tracePlotFX(self):
        """
        plot diagnostic trace plots
        """
        sigIndx = np.array([0, 30, 50, 88, 122, 162])
        cc = 0
        plotNames = np.append(self.names, sigIndx.astype('str'))
        arrayIndx = np.append(np.arange(self.ncol), (sigIndx + self.ncol))
        plotArray = self.results[:, arrayIndx]
        nTotalFigs = len(arrayIndx)
        nfigures = np.int(np.ceil(nTotalFigs / 6.0))
        for i in range(nfigures):
            P.figure(i)
            lastFigure = i == (nfigures - 1)
            for j in range(6):
                P.subplot(2,3,j+1)
                if cc < nTotalFigs:
                    P.plot(plotArray[:, cc])
                    P.title(plotNames[cc])
                cc += 1
            P.show(block=lastFigure)

    def traceDeltaPlot(self):
        """
        plot diagnostic trace plots
        """
        deltSum = np.sum(self.gibbsobj.deltagibbs, axis = 1)
#        print('deltsum', deltSum)

        deltIndx = np.array([7, 44, 69, 98, 125, 157])
        plotNames = deltIndx.astype('str')
        plotArray = self.gibbsobj.deltagibbs[:, deltIndx]
        P.figure()
        for i in range(6):
            delt_i = plotArray[:, i]
            P.subplot(2,3, (i+1))
            P.plot(delt_i)
            P.title(plotNames[i])
        P.show()

    def traceDensityPlot(self):
        """
        plot diagnostic trace plots
        """
#        densityIndx = np.array([7, 44, 69, 98, 125, 157])
        plotNames = self.gibbsobj.uniqueArea
        plotArray = self.gibbsobj.Dgibbs.copy()
        nTotalPlots = np.shape(plotArray)[1]
        nfigures = np.int(np.ceil(nTotalPlots / 6.0))
        print('tot plot', nTotalPlots, 'nfig', nfigures)

        cc = 0
        for i in range(nfigures):
            P.figure(i)
            lastFigure = i == (nfigures - 1) 
            for j in range(6):
                P.subplot(2,3, j+1)
                if cc < nTotalPlots:
                    den_i = plotArray[:, cc]
                    P.plot(den_i)
                    P.title(plotNames[cc])
                cc += 1
            P.show(block=lastFigure)




    def plotDensitySigma(self):
        """
        Plot density vs sig; and sig pred vs sig in 2 sub plots
        """
#        meana0 = np.mean(self.gibbsobj.agibbs)
#        print('mean a0', meana0)
        self.meanSig = np.mean(self.siggibbs, axis = 0)
        self.sdSig = np.std(self.siggibbs, axis = 0)
        
        alpha = self.gibbsobj.agibbs
        logsigma = np.log(self.gibbsobj.siggibbs[:, self.gibbsobj.moveCaptMask])
#        # lambda for ground cage or leg traps.
#        lambda0Pred = inv_logit(np.expand_dims(alpha[:, 0], 1) + 
#             (np.expand_dims(alpha[:, 1], 1) * logsigma) + self.gibbsobj.deltagibbs) 

        lambda0Pred = np.zeros((self.ndat, self.gibbsobj.nCaptureInd))

        for i in range(self.gibbsobj.nCaptureInd):

            if (self.params.modelID >= 20):
                # average lambda for cage, and ground and leg traps.
                lambda0Pred[:, i] = inv_logit((alpha[:, 0]) + 
                     ((alpha[:, 1]) * logsigma[:, i]) + 
                    ((alpha[:, 2] / 3.0)) + 
                    ((alpha[:, 3] / 3.0)) + 
                    ((self.gibbsobj.deltagibbs[:, i]))) 
            elif (self.params.modelID == 3) | (self.params.modelID == 6):
                lambda0Pred[:, i] = inv_logit(np.expand_dims(alpha[:, 0], 1) + 
                     (np.expand_dims(alpha[:, 1], 1) * logsigma[:, i]) + 
                    (np.expand_dims(alpha[:, 2], 1)) + 
                    self.gibbsobj.deltagibbs[:, i]) 
            elif (self.params.modelID == 4) | (self.params.modelID == 5):
                lambda0Pred[:, i] = inv_logit(np.expand_dims(alpha[:, 0], 1) + 
                    (np.expand_dims(alpha[:, 1], 1) * logsigma[:, i]) + 
                    self.gibbsobj.deltagibbs[:, i]) 

        self.g0Mean = np.zeros(self.gibbsobj.nUniqueAnimal)
        self.g0SD = np.zeros(self.gibbsobj.nUniqueAnimal)
        self.poss = np.zeros(self.gibbsobj.nUniqueAnimal)
        self.Area = np.zeros(self.gibbsobj.nUniqueAnimal, dtype = 'U32')
        self.Habitat = np.zeros(self.gibbsobj.nUniqueAnimal, dtype = 'U12')
        self.Age = np.zeros(self.gibbsobj.nUniqueAnimal, dtype = 'U12')
        self.Sex = np.zeros(self.gibbsobj.nUniqueAnimal, dtype = 'U12')
        self.meanDensityInd = np.mean(self.gibbsobj.Dgibbs, axis = 0)
        self.DensityInd = self.meanDensityInd[self.gibbsobj.individAreaIndx]
        self.meanDeltaInd = np.mean(self.gibbsobj.deltagibbs, axis = 0)
        self.sdDeltaInd = np.std(self.gibbsobj.deltagibbs, axis = 0)
        maxDensity = np.max(self.meanDensityInd)

        print('sigcov', self.gibbsobj.sigmaCovar[-25:])

        cc = 0
        for i in range(self.gibbsobj.nUniqueAnimal):
            area_i = self.gibbsobj.individAreaIndx[i]
            self.poss[i] = self.gibbsobj.uniqueAnimalGPS[i]
            self.Area[i] = self.gibbsobj.areaHabitat[area_i]
            self.Habitat[i] = self.gibbsobj.habitatHabitat[area_i]
            if self.gibbsobj.uniqueAnimalAge[i] == 1:
                self.Age[i] = 'Juv'
            else:
                self.Age[i] = 'Ad'
            if self.gibbsobj.uniqueAnimalSex[i] == 1:
                self.Sex[i] = 'F'
            else:
                self.Sex[i] = 'M'

            if self.gibbsobj.moveCaptMask[i]:
                self.g0Mean[i] = np.mean(lambda0Pred[:, cc])
                self.g0SD[i] = np.std(lambda0Pred[:, cc])


                print('i', i, 'poss_i', self.poss[i], 
                    'nFix', self.gibbsobj.nFix[i], 
                    'age', self.Age[i], 'sex', self.Sex[i],
                    'area', self.Area[i], 
                    'hab', self.Habitat[i], 
                    'Density', np.round(self.DensityInd[i],2), 
                    'Sigmamean', np.round(self.meanSig[i],2), 
                    'sigmasd', np.round(self.sdSig[i],2), 
                    'g0Mean', np.round(self.g0Mean[i],2), 
                    'g0SD', np.round(self.g0SD[i],2),
                    'deltMn', np.round(self.meanDeltaInd[cc], 2),
                    'deltSD', np.round(self.sdDeltaInd[cc], 2))
                
                cc += 1
            ## make Individual appendix table
        # create new structured array with columns of different types
        structured = np.empty((self.gibbsobj.nUniqueAnimal,), dtype=[('Possum', np.float),
                ('Age', 'U12'), ('Sex', 'U12'), ('nFix', np.float), ('Area', 'U32'), 
                ('Habitat', 'U12'), ('Density', np.float), 
                ('MeanSigma', np.float), ('seSigma', np.float), 
                ('MeanLambda0', np.float), ('seLambda0', np.float)])
        # copy data over
        structured['Possum'] = self.poss
        structured['Age'] = self.Age
        structured['Sex'] = self.Sex
        structured['nFix'] = self.gibbsobj.nFix
        structured['Area'] = self.Area
        structured['Habitat'] = self.Habitat
        structured['Density'] = self.DensityInd
        structured['MeanSigma'] = self.meanSig
        structured['seSigma'] = self.sdSig
        structured['MeanLambda0'] = self.g0Mean
        structured['seLambda0'] = self.g0SD
        np.savetxt(self.params.individAppendixFname, structured, fmt=['%.1f', '%s', 
                    '%s', '%s', '%s', '%s', '%.2f',  
                    '%.2f', '%.2f', '%.2f', '%.2f'], 
                    comments = '', delimiter = ',', 
                    header='Possum, Age, Sex, nFix, Area, Habitat, Density, MeanSigma, SeSigma, MeanLambda0, seLambda0')

        ############## MAKE DENSITY SIGMA PLOT
        ## get pred mean prediction of sigma from density and k
        densityRange = np.arange(.1, (maxDensity + 0.2), 0.01)
        nDen = len(densityRange)
        denMatrix = np.tile(densityRange, self.ndat).reshape(self.ndat, nDen)
        print('shp denMatrix', np.shape(denMatrix))
        # account for Farm-bush matrix, and immature and adult effects
        if (self.params.modelID >= 20) & (self.params.modelID < 23):
            meanKAll = np.expand_dims(np.exp(self.gibbsobj.bgibbs[:,0] +
                (self.gibbsobj.bgibbs[:,1] * 0) + 
                (self.gibbsobj.bgibbs[:,-2] / 2.0) + 
                (self.gibbsobj.bgibbs[:, -1] / 2.0)), 1)
        elif (self.params.modelID == 23):
            meanKAll = np.expand_dims(np.exp(self.gibbsobj.bgibbs[:,0] +
                (self.gibbsobj.bgibbs[:,1] * 0) + 
                (self.gibbsobj.bgibbs[:,2] * 0) + 
                (self.gibbsobj.bgibbs[:,3] / 2.0) + 
                (self.gibbsobj.bgibbs[:, -1] / 2.0)), 1)
        elif (self.params.modelID == 24) | (self.params.modelID == 26):
            meanKAll = np.expand_dims(np.exp(self.gibbsobj.bgibbs[:,0]), 1)
        elif (self.params.modelID == 25):
            meanKAll = np.expand_dims(np.exp(self.gibbsobj.bgibbs[:,0] +
                (self.gibbsobj.bgibbs[:,-2] / 2.0) + 
                (self.gibbsobj.bgibbs[:, -1] / 2.0)), 1)


#        predSig = meanK / np.sqrt(denMatrix)
#        meanPredSig = np.mean(predSig, axis = 0)
#        quantSig = mquantiles(predSig, prob=[0.05, 0.95], axis = 0)

        predSigAll = meanKAll / np.sqrt(denMatrix)
        meanPredSigAll = np.mean(predSigAll, axis = 0)
        quantSigAll = mquantiles(predSigAll, prob=[0.025, 0.975], axis = 0)


        P.figure(figsize = (11,9))
        ax = P.gca()
#        lns1 = ax.plot(densityRange, meanPredSig, color='k', linewidth = 5)
        lns2 = ax.plot(densityRange, meanPredSigAll, color='k', linewidth = 2.5)
#        lns2 = ax.plot(densityRange, quantSigAll[0], color='k', linewidth = 3, ls='dashed')
#        lns3 = ax.plot(densityRange, quantSigAll[1], color='k', linewidth = 3, ls='dashed')
#        lns2 = ax.plot(densityRange, quantSig[0], color='r', linewidth = 3, ls='dashed')
#        lns3 = ax.plot(densityRange, quantSig[1], color='r', linewidth = 3, ls='dashed')
        maskJF = (self.Age == 'Juv') & (self.Sex == 'F')
        maskJM = (self.Age == 'Juv') & (self.Sex == 'M')
        maskAF = (self.Age == 'Ad') & (self.Sex == 'F')
        maskAM = (self.Age == 'Ad') & (self.Sex == 'M')


        print('self.DensityInd[maskAF]', self.DensityInd)


#        lns4 = ax.plot(self.DensityInd, self.meanSig, 'ko', 
#                markerfacecolor = 'k', ms=5, mew=1.2)
        lns7 = ax.plot(self.DensityInd[maskAF], self.meanSig[maskAF], 'kd', 
                markerfacecolor = 'k', ms=6, mew=1.2, label='Adult Female')
        lns8 = ax.plot(self.DensityInd[maskAM], self.meanSig[maskAM], 'ko', 
                markerfacecolor = 'k', ms=6, mew=1.2, label='Adult Male')
        lns5 = ax.plot(self.DensityInd[maskJF], self.meanSig[maskJF], 'rd', 
                markerfacecolor = 'r', ms=6, mew=1.2, label='Juvenile Female')
        lns6 = ax.plot(self.DensityInd[maskJM], self.meanSig[maskJM], 'ro', 
                markerfacecolor = 'r', ms=6, mew=1.2, label='Juvenile Male')
        lns = lns5 + lns6 + lns7 + lns8
        labs = [l.get_label() for l in lns]
        ax.legend(lns, labs, loc = 'upper right', fontsize = 14)

        minY = np.min(self.meanSig) - 5.0
        maxY = np.max(self.meanSig) + 5.0
        ax.set_ylim([minY, maxY])
        for tick in ax.xaxis.get_major_ticks():
            tick.label.set_fontsize(14)
        for tick in ax.yaxis.get_major_ticks():
            tick.label.set_fontsize(14)

        ax.set_xlabel('Density (possum ha$^{-1}$)', fontsize = 17)
        ax.set_ylabel('$\sigma$', rotation='horizontal', fontsize = 17)
        P.savefig(self.params.densitySigPlotFname, format='eps', dpi = 600)
        P.show()



    ########            Write data to file
    ########
    def writeToFileFX(self):
        (m, n) = self.summaryTable.shape
        # create new structured array with columns of different types
        structured = np.empty((m,), dtype=[('Names', 'U12'), ('Mean', np.float),
                    ('Low CI', np.float), ('High CI', np.float)])
        # copy data over
        structured['Mean'] = self.summaryTable[:, 0]
        structured['Low CI'] = self.summaryTable[:, 1]
        structured['High CI'] = self.summaryTable[:, 2]
        structured['Names'] = self.names
        np.savetxt(self.params.paramsResFname, structured, fmt=['%s', '%.4f', '%.4f', '%.4f'], 
                    comments = '', delimiter = ',', 
                    header='Names, Mean, Low_CI, High_CI')


    def siteTableFX(self):
        """
        table of site estimates of sigma, raised leg g0 and covariates
        """
        # create new structured array with columns of different types
        structured = np.empty((self.gibbsobj.nUniqueArea,), dtype=[('Area', 'U32'), 
                ('Habitat', 'U12'), ('Density', np.float), ('seDensity', np.float),
                ('NIndSigma', 'i8'), ('MeanSigma', np.float), ('seSigma', np.float), 
                ('NIndLambda0', 'i8'), ('MeanLambda0', np.float), ('seLambda0', np.float),
                ('meanDelta', np.float), ('seDelta', np.float)])
        sigmaMeanArea = np.zeros(self.gibbsobj.nUniqueArea)
        sigmaSEArea = np.zeros(self.gibbsobj.nUniqueArea)
        meanSE = np.empty(self.gibbsobj.nUniqueArea, dtype = 'U12')
        meanDeltaArea = np.zeros(self.gibbsobj.nUniqueArea)
        deltaSE = np.zeros(self.gibbsobj.nUniqueArea)
        ## DENSITY RESULTS
        self.summaryDensity = np.zeros((2, self.gibbsobj.nUniqueArea))
        self.summaryDensity[0] = np.round(np.mean(self.gibbsobj.Dgibbs, axis = 0), 2)
        self.summaryDensity[1] = np.round(np.std(self.gibbsobj.Dgibbs, axis = 0), 2)
        # lambda0 arrays
        l0MeanArea = np.zeros(self.gibbsobj.nUniqueArea)
        l0SEArea = np.zeros(self.gibbsobj.nUniqueArea)
        nIndSigmaArea =  np.zeros(self.gibbsobj.nUniqueArea, dtype = int)

        self.alpha = self.gibbsobj.agibbs
        logsigma = np.log(self.gibbsobj.siggibbs[:, self.gibbsobj.moveCaptMask])
        # lambda for ground leg or cage traps.
#        lambda0Pred = inv_logit(np.expand_dims(self.alpha[:, 0], 1) + 
#                (np.expand_dims(self.alpha[:, 1], 1) * logsigma) + 
#                self.gibbsobj.deltagibbs) 

        lambda0Pred = np.zeros((self.ndat, self.gibbsobj.nCaptureInd))
        for i in range(self.gibbsobj.nCaptureInd):

            # AVERAGE LAMBDA PRED
            if (self.params.modelID >= 20):
                lambda0Pred[:, i] = inv_logit(self.alpha[:, 0] + 
                    (self.alpha[:, 1] * logsigma[:, i]) +
                    (self.alpha[:, 2] / 3.0) + 
                    (self.alpha[:, 3] / 3.0) + self.gibbsobj.deltagibbs[:, i]) 
            elif (self.params.modelID == 3) | (self.params.modelID == 6):
                lambda0Pred[:, i] = inv_logit(self.alpha[:, 0] + 
                    (self.alpha[:, 1] * logsigma[:, i]) +
                    (self.alpha[:, 2] / 2.0) + 
                    self.gibbsobj.deltagibbs[:, i]) 
            elif (self.params.modelID == 4) | (self.params.modelID == 5):
                lambda0Pred[:, i] = inv_logit(self.alpha[:, 0] + 
                    (self.alpha[:, 1] * logsigma[:, i]) +
                    self.gibbsobj.deltagibbs[:, i]) 

        indAreaIndxReduced = self.gibbsobj.individAreaIndx[self.gibbsobj.moveCaptMask]
        nIndLambda0Area = np.zeros(self.gibbsobj.nUniqueArea, dtype = int)
        ## no move captured poss at Leader area 6, remove from g0 part of table
        # get sigma summary by site
        for i in range(self.gibbsobj.nUniqueArea):
            mask_i = self.gibbsobj.individAreaIndx == i
            nIndSigmaArea[i] = np.sum(mask_i)
            sigmaMeanInd = np.mean(self.gibbsobj.siggibbs[:, mask_i], axis = 0)
            sigmaMeanArea[i] = np.round(np.mean(sigmaMeanInd), 0)
            sigmaSEArea[i] = np.round(np.std(sigmaMeanInd), 1)



            # get g0 / lambda0 details
            maskIndArea_i = indAreaIndxReduced == i
#            print('index area i', i, indAreaIndxReduced[maskIndArea_i],
#                'shp sig i', np.shape(sig2[:, maskIndArea_i]))
            if i != 6:
                lambda0Mean = np.mean(lambda0Pred[:, maskIndArea_i], axis = 0)
                l0MeanArea[i] = np.round(np.mean(lambda0Mean), 3)
                l0SEArea[i] = np.round(np.std(lambda0Mean), 3)
                nIndLambda0Area[i] = np.sum(maskIndArea_i)

                deltaMeanInd = np.mean(self.gibbsobj.deltagibbs[:, maskIndArea_i], 
                        axis = 0)
                meanDeltaArea[i] = np.round(np.mean(deltaMeanInd), 2)
                deltaSE[i] = np.round(np.std(deltaMeanInd), 2)


            print('i', i, 'area', self.gibbsobj.areaHabitat[i], 
                'Den_i', self.summaryDensity[0, i], 'SE Den', self.summaryDensity[1, i],
                'nIndSig', nIndSigmaArea[i], 
                'sig m', sigmaMeanArea[i], 'sig sd', sigmaSEArea[i], 'nIndLambda', nIndLambda0Area[i],
                'l0 mean', l0MeanArea[i], 'l0 se', l0SEArea[i],
                'deltMean', meanDeltaArea[i], 'deltSE', deltaSE[i])
        # copy data over
        structured['Area'] = self.gibbsobj.areaHabitat
        structured['Habitat'] = self.gibbsobj.habitatHabitat
        structured['Density'] = self.summaryDensity[0]
        structured['seDensity'] = self.summaryDensity[1]
        structured['NIndSigma'] = nIndSigmaArea
        structured['MeanSigma'] = sigmaMeanArea
        structured['seSigma'] = sigmaSEArea
        structured['NIndLambda0'] = nIndLambda0Area
        structured['MeanLambda0'] = l0MeanArea
        structured['seLambda0'] = l0SEArea
        structured['meanDelta'] = meanDeltaArea
        structured['seDelta'] = deltaSE
    

        np.savetxt(self.params.sigG0TableFname, structured, fmt=['%s', '%s', '%.2f', 
                    '%.2f', '%i', '%.2f', '%.2f', '%i', '%.2f', '%.2f', '%.2f', '%.2f'], 
                    comments = '', delimiter = ',', 
                    header='Area, Habitat, Density, seDensity, NSigmaInd, MeanSigma, SeSigma, NLambda0Ind, MeanLambda0, SeLambda0, meanDelta SeDelta')



    def plotG0_Sigma(self):
        """
        plot g0 vs sigma, measured and predicted mean and 95%CI
        """
        sigRange = np.arange(np.min(self.meanSig), np.max(self.meanSig) +2., 0.5)
        # lambda for ground leg traps.
        logsig = np.log(sigRange)
        nSig = len(logsig)
        sigMatrix = np.tile(logsig, self.ndat).reshape(self.ndat, nSig)


        lambda0Pred = np.zeros((self.ndat, nSig))
        for i in range(nSig):

            if (self.params.modelID >= 20):
                lambda0Pred[:, i] = inv_logit(self.alpha[:, 0] + 
                    (self.alpha[:, 1] * sigMatrix[:, i]) + 
                    (self.alpha[:, 2] / 3.0) +         
                    (self.alpha[:, 3] / 3.0))         
            if (self.params.modelID == 3) | (self.params.modelID == 6):
                lambda0Pred[:, i] = inv_logit(self.alpha[:, 0] + 
                    (self.alpha[:, 1] * sigMatrix[:, i]) + 
                    (self.alpha[:, 2] / 2.0))         
            if (self.params.modelID == 4) | (self.params.modelID == 5):
                lambda0Pred[:, i] = inv_logit(self.alpha[:, 0] + 
                    (self.alpha[:, 1] * sigMatrix[:, i]))

        meanLambda0 = np.mean(lambda0Pred, axis = 0)
        quantsLambda0 = mquantiles(lambda0Pred, prob=[0.025, 0.975], axis = 0)
        
        P.figure(figsize = (11,9))
        ax = P.gca()
        lns1 = ax.plot(sigRange, meanLambda0, color='k', linewidth = 2.5)
#        lns2 = ax.plot(sigRange, quantsLambda0[0], color='k', linewidth = 1.5, ls='dashed')
#        lns3 = ax.plot(sigRange, quantsLambda0[1], color='k', linewidth = 1.5, ls='dashed')

        lns4 = ax.plot(self.meanSig[self.gibbsobj.moveCaptMask], 
            self.g0Mean[self.gibbsobj.moveCaptMask], 'ko', markerfacecolor = 'k', 
            ms=5.5, mew=1.)

        minY = np.min(self.g0Mean) - .01
        maxY = np.max(self.g0Mean) + .01
        ax.set_ylim([minY, maxY])
        for tick in ax.xaxis.get_major_ticks():
            tick.label.set_fontsize(14)
        for tick in ax.yaxis.get_major_ticks():
            tick.label.set_fontsize(14)

        ax.set_xlabel('$\sigma$', fontsize = 17)
        ax.set_ylabel('$g_0$', rotation='horizontal', fontsize = 17)
#        P.savefig("plot_g0Sig_No_CI.png", format='png', dpi = 1200)
        P.savefig(self.params.g0SigPlotFname, format='eps', dpi = 600)
        P.show()


    def onePossSensitivity(self):
        """
        test the se or pcapt of a single possum across a range of sigmas and g0
        """
        print("Run onePossSensitivity")
        self.maxX = 2000
        self.maxY = 2000
        self.areaHa = self.maxX * self.maxY / 10000 
        self.meanSig = np.mean(self.siggibbs, axis = 0)
        self.sigRange = np.arange(27.0, np.max(self.meanSig), 2.0)
#        self.sigRange = np.arange(np.min(self.meanSig), np.max(self.meanSig) +2., 0.5)

        # lambda for ground leg traps.
        logsig = np.log(self.sigRange)
        self.nSig = len(logsig)
        sigMatrix = np.tile(logsig, self.ndat).reshape(self.ndat, self.nSig)

        ## AVERAGE LAMBDA0 ACROSS TRAP TYPES
        if (self.params.modelID >= 20):
            self.lambda0Pred = inv_logit(np.expand_dims(self.alpha[:, 0], 1) + 
                (np.expand_dims(self.alpha[:, 1], 1) * sigMatrix) +
                np.expand_dims((self.alpha[:, 2] / 3.0), 1) + 
                np.expand_dims((self.alpha[:, 3] / 3.0), 1))
            ## COMPARE LEG RAISED VS LEG GROUND
            self.lambda0Raised = inv_logit(np.expand_dims(self.alpha[:, 0], 1) + 
                (np.expand_dims(self.alpha[:, 1], 1) * sigMatrix) +
                np.expand_dims(self.alpha[:, 2], 1))
            self.lambda0Ground = inv_logit(np.expand_dims(self.alpha[:, 0], 1) + 
                (np.expand_dims(self.alpha[:, 1], 1) * sigMatrix) +
                np.expand_dims(self.alpha[:, 3], 1))
            self.lambda0Cage = inv_logit(np.expand_dims(self.alpha[:, 0], 1) + 
                (np.expand_dims(self.alpha[:, 1], 1) * sigMatrix))
        elif (self.params.modelID == 3) | (self.params.modelID == 6):
            self.lambda0Pred = inv_logit(np.expand_dims(self.alpha[:, 0], 1) + 
                (np.expand_dims(self.alpha[:, 1], 1) * sigMatrix) +
                np.expand_dims((self.alpha[:, 2] / 2.0), 1))     
            ## COMPARE LEG RAISED VS GROUND LEG AND CAGE
            self.lambda0Raised = inv_logit(np.expand_dims(self.alpha[:, 0], 1) + 
                (np.expand_dims(self.alpha[:, 1], 1) * sigMatrix) +
                np.expand_dims(self.alpha[:, 2], 1))
            self.lambda0Ground = inv_logit(np.expand_dims(self.alpha[:, 0], 1) + 
                (np.expand_dims(self.alpha[:, 1], 1) * sigMatrix))
        elif (self.params.modelID == 4) | (self.params.modelID == 5):
            self.lambda0Pred = inv_logit(np.expand_dims(self.alpha[:, 0], 1) + 
                (np.expand_dims(self.alpha[:, 1], 1) * sigMatrix))

        self.meanLambda0 = np.mean(self.lambda0Pred, axis = 0)
        if (self.params.modelID < 4) | (self.params.modelID >= 6):
            self.meanLambda0Raised = np.mean(self.lambda0Raised, axis = 0)
            self.meanLambda0Ground = np.mean(self.lambda0Ground, axis = 0)
            self.meanLambda0Cage = np.mean(self.lambda0Cage, axis = 0)
        
        print('ndat', self.ndat, self.nSig)
        print('shp lambda0Pred', np.mean(self.lambda0Pred, axis = 0)[-30:], 
            'sig', sigMatrix[:20, -5:])
        maskSig = (self.sigRange == 27) | (self.sigRange == 101)

        self.lineSpace = [100.0, 200.0, 400.0]
        self.ttSpace = [50.0, 100.0, 200.0]
        self.nTT_Line = [9.0, 5.0, 3.0]
        self.nLines = [9.0, 5.0, 3.0]
        trapDen = [2.53, 0.78, 0.11]
        P.figure(figsize = (16,10))
#        ax = P.gca()
        col = ['k', 'b', 'r']
        lns = []
        ax = P.subplot(1,2,1)
        ## loop thru scenarios
        for scen in range(3):
            self.getTrapLoc(scen)
            self.getRandHRC()
            
#            sse = np.pi * 2.0 * self.lambda0Pred * self.sigRange**2 / 40000.       
            meanSse = np.mean(self.seMatrix, axis = 0)
            quantsSse = mquantiles(self.seMatrix, prob=[0.025, 0.975], axis = 0)
            lab_scen = str(self.trapDensity) + ' ' + 'traps' + ' ' + '$ha^{-1}$'
            lns1 = ax.plot(self.sigRange, meanSse, color=col[scen], linewidth = 3.5,
                label = lab_scen)
            lns2 = ax.plot(self.sigRange, quantsSse[0], color=col[scen], 
                linewidth = 2, ls='dashed')
            lns3 = ax.plot(self.sigRange, quantsSse[1], color=col[scen], 
                linewidth = 2, ls='dashed')
            lns += lns1

            print('Scen', scen, 'meanSSe', meanSse[maskSig])

        labs = [l.get_label() for l in lns]
        ax.legend(lns, labs, loc = 'upper left', fontsize = 14)
        minY = 0.0  # np.min(quantsSse[0]) - .01
        maxY = 1.0  # np.max(quantsSse[1]) + .01
        ax.set_ylim([minY, maxY])
        for tick in ax.xaxis.get_major_ticks():
            tick.label.set_fontsize(14)
        for tick in ax.yaxis.get_major_ticks():
            tick.label.set_fontsize(14)
        P.title("A.", loc = 'left', fontsize = 17)                
        ax.set_xlabel('$\sigma$', fontsize = 17)
        ax.set_ylabel('Probability of capture', fontsize = 17)
        
        ## MAKE COMPARISON PLOT BETWEEN RAISED AND GROUND TRAPS
        if (self.params.modelID <= 3) | (self.params.modelID >= 6):
            ax1 = P.subplot(1,2,2)
            self.getTrapLoc(1)
            self.getRandHRC()
            meanSseRaised = np.mean(self.seMatrixRaised, axis = 0)
            meanSseGround = np.mean(self.seMatrixGround, axis = 0)
            meanSseCage = np.mean(self.seMatrixCage, axis = 0)
            quantsSseRaised = mquantiles(self.seMatrixRaised, prob=[0.025, 0.975], axis = 0)
            quantsSseGround = mquantiles(self.seMatrixGround, prob=[0.025, 0.975], axis = 0)
            quantsSseCage = mquantiles(self.seMatrixCage, prob=[0.025, 0.975], axis = 0)
            lab_scen = str(trapDen[0]) + ' ' + 'traps' + ' ' + '$ha^{-1}$'
            lns1 = ax1.plot(self.sigRange, meanSseRaised, color='c', linewidth = 3.5,
                label = 'Raised leg')
            lns2 = ax1.plot(self.sigRange, quantsSseRaised[0], color='c', 
                linewidth = 2, ls='dashed')
            lns3 = ax1.plot(self.sigRange, quantsSseRaised[1], color='c', 
                linewidth = 2, ls='dashed')
            lns4 = ax1.plot(self.sigRange, meanSseGround, color='m', linewidth = 4,
                label = 'Ground leg')
            lns5 = ax1.plot(self.sigRange, quantsSseGround[0], color='m', 
                linewidth = 2, ls='dashed')
            lns6 = ax1.plot(self.sigRange, quantsSseGround[1], color='m', 
                linewidth = 2, ls='dashed')
            lns7 = ax1.plot(self.sigRange, meanSseCage, color='y', linewidth = 4,
                label = 'Cage')
            lns8 = ax1.plot(self.sigRange, quantsSseCage[0], color='y', 
                linewidth = 2, ls='dashed')
            lns9 = ax1.plot(self.sigRange, quantsSseCage[1], color='y', 
                linewidth = 2, ls='dashed')

            lns = lns1 + lns4 + lns7
            labs = [l.get_label() for l in lns]
            ax1.legend(lns, labs, loc = 'upper left', fontsize = 14)
            minY = np.min(quantsSseGround[0]) - .01
            maxY = np.max(quantsSseCage[1]) + .04
            ax1.set_ylim([minY, maxY])
            for tick in ax1.xaxis.get_major_ticks():
                tick.label.set_fontsize(14)
            for tick in ax1.yaxis.get_major_ticks():
                tick.label.set_fontsize(14)
            ax1.set_xlabel('$\sigma$', fontsize = 17)
            ax1.set_ylabel('')
            P.title("B.", loc = 'left', fontsize = 17)      


            print('Cage SSe', meanSseCage[maskSig])
            print('Ground SSe', meanSseGround[maskSig])
            print('Raised SSe', meanSseRaised[maskSig])


          
###        figName = os.path.join(self.params.outputDataPath, 'onePossSse_Compare.eps')
#        figName = os.path.join(self.params.outputDataPath, 'onePossSse_3Scenario.png')
#        figName = os.path.join(self.params.outputDataPath, 'onePossSse_100_50_9_9.png')
###        P.savefig(figName, format='eps', dpi = 400)
        P.savefig(self.params.onePossSseFname, format='eps', dpi = 600)
        P.show()

        



    def getTrapLoc(self, scen):
        """
        sim trap array
        """
        self.lineSpace_Scen = self.lineSpace[scen]
        self.ttSpace_Scen = self.ttSpace[scen]
#        nTT_Line = self.nTT_Line[scen]
#        nLines = self.nLines[scen]
#        self.xMaxTrap = ((nLines - 1.0) * self.lineSpace_Scen)
#        self.yMaxTrap = ((nTT_Line - 1.0) * self.ttSpace_Scen)
#        self.xRange = [0.0, (self.xMaxTrap + self.lineSpace_Scen)]
#        self.yRange = [-self.ttSpace_Scen, (self.ttSpace_Scen * (nTT_Line - 1.0))]
#        yArr = np.arange(self.yRange[1], self.yRange[0], -self.ttSpace_Scen)
#        xArr = np.arange(self.xRange[0], self.xRange[1], self.lineSpace_Scen)

        ## NEW APPROACH PUT INDIVID IN MIDDLE OF BIG TRAP ARRAY
        xArr = np.arange(0, 2000+self.lineSpace_Scen, self.lineSpace_Scen)
        yArr = np.arange(2000, -self.ttSpace_Scen, -self.ttSpace_Scen)
        

        self.xTrap = np.tile(xArr, len(yArr))
        self.yTrap = np.repeat(yArr, len(xArr))
        self.ntraps = len(self.xTrap)
        self.trapDensity = np.round(self.ntraps / self.areaHa, 2)
#        P.figure()
#        P.plot(self.xTrap, self.yTrap, 'ko')
#        P.show()
        print('Scen', scen, 'midX', np.mean(self.xTrap), 'max X', np.max(self.xTrap),
                'midY', np.mean(self.yTrap), 'max Y', np.max(self.yTrap),
                'ntraps', self.ntraps, 'trapDen', self.trapDensity)


    def getRandHRC(self):
        """
        sim random HRC location within trap grid
        """
        nInd = 10000 #20000
        self.seMatrix = np.zeros((nInd, self.nSig))
        
        self.seMatrixRaised = np.zeros((nInd, self.nSig))
        self.seMatrixGround = np.zeros((nInd, self.nSig))
        self.seMatrixCage = np.zeros((nInd, self.nSig))
        for i in range(self.nSig):
            deltX = self.lineSpace_Scen * 0.51   
            deltY = self.ttSpace_Scen * 0.51
            midX = np.mean(self.xTrap)
            midY = np.mean(self.yTrap)
            randX = (np.random.uniform((midX - deltX), 
                    (midX + deltX), nInd))
            randY = (np.random.uniform((midY - deltY), 
                    (midY + deltY), nInd))

#            randX = (np.random.uniform((self.xRange[0] - 20.0), 
#                    (self.xMaxTrap + 20.0), nInd))
#            randY = (np.random.uniform((self.yRange[0] - 20.0), 
#                    (self.yMaxTrap + 20.0), nInd))

#            randX = np.random.uniform((self.xRange[0] - 20.0), (self.xMaxTrap + 20.0), self.ndat)
#            randY = np.random.uniform((self.yRange[0] - 20.0), (self.yMaxTrap + 20.0), self.ndat)

#            randX = np.random.uniform((-3.0*self.sigRange[i]), (self.xMaxTrap + 
#                    (3.0*self.sigRange[i])), self.ndat)
#            randY = np.random.uniform((-3.0*self.sigRange[i]), (self.yMaxTrap + 
#                    (3.0*self.sigRange[i])), self.ndat)

            distTrap = distFX(randX, randY, self.xTrap, self.yTrap)
            distMask = distTrap <= (4.0 * self.sigRange[i])

#            print('i', i, 'shp distTrap', distTrap.shape)

            for j in range(nInd):
                dist_j = distTrap[j]
                dmask_j = distMask[j]
                dist_j = dist_j[dmask_j]
                expTerm = np.exp(-(dist_j**2) / 2.0 / (self.sigRange[i]**2))
                ## AVERAGE SE ACROSS TRAP TYPES
                se = (1.0 - np.prod(1.0 - self.meanLambda0[i] * expTerm))
                self.seMatrix[j, i] = se

                if (self.params.modelID == 4) | (self.params.modelID == 5):
                    continue
                ## RAISED LEG
                se = (1.0 - np.prod(1.0 - self.meanLambda0Raised[i] * expTerm))
                self.seMatrixRaised[j, i] = se
                ## GROUND
                se = (1.0 - np.prod(1.0 - self.meanLambda0Ground[i] * expTerm))
                self.seMatrixGround[j, i] = se
                ## CAGE
                se = (1.0 - np.prod(1.0 - self.meanLambda0Cage[i] * expTerm))
                self.seMatrixCage[j, i] = se
                
#            print('ntraps', self.ntraps, 'max randXY', np.min(self.randX), np.min(self.randY), 
#                'ceil X', (self.xMaxTrap + (4.0*self.sigRange)),
#                'ceil Y', (self.yMaxTrap + (4.0*self.sigRange)))

#        P.figure()
#        P.plot(self.xTrap, self.yTrap, 'ro')
#        P.plot(randX, randY, 'k*')
#        P.show()


    def compareG0_OneTrap(self):
        """
        ## COMPARE G0 ACROSS RANGE OF SIG FOR ONE POSS AND ONE TRAP
        """
        print("Run compareG0_OneTrap")
        DIST_j = 50.0
        self.meanSig = np.mean(self.siggibbs, axis = 0)
        self.sigRange = np.arange(27.0, np.max(self.meanSig), 1.0)
#        self.sigRange = np.arange(np.min(self.meanSig), np.max(self.meanSig) +2., 0.5)
        # lambda for ground leg traps.
        logsig = np.log(self.sigRange)
        self.nSig = len(logsig)
        sigMatrix = np.tile(logsig, self.ndat).reshape(self.ndat, self.nSig)

        ## AVERAGE LAMBDA0 ACROSS TRAP TYPES
        if (self.params.modelID == 2) | (self.params.modelID == 7):
            self.lambda0Pred = inv_logit(np.expand_dims(self.alpha[:, 0], 1) + 
                (np.expand_dims(self.alpha[:, 1], 1) * sigMatrix) +
                np.expand_dims((self.alpha[:, 2] / 3.0), 1) + 
                np.expand_dims((self.alpha[:, 3] / 3.0), 1))
            ## COMPARE LEG RAISED VS LEG GROUND VS CAGE
            self.lambda0Raised = inv_logit(np.expand_dims(self.alpha[:, 0], 1) + 
                (np.expand_dims(self.alpha[:, 1], 1) * sigMatrix) +
                np.expand_dims(self.alpha[:, 2], 1))
            self.lambda0Ground = inv_logit(np.expand_dims(self.alpha[:, 0], 1) + 
                (np.expand_dims(self.alpha[:, 1], 1) * sigMatrix) +
                np.expand_dims(self.alpha[:, 3], 1))
            self.lambda0Cage = inv_logit(np.expand_dims(self.alpha[:, 0], 1) + 
                (np.expand_dims(self.alpha[:, 1], 1) * sigMatrix))
        elif (self.params.modelID == 3) | (self.params.modelID == 6):
            self.lambda0Pred = inv_logit(np.expand_dims(self.alpha[:, 0], 1) + 
                (np.expand_dims(self.alpha[:, 1], 1) * sigMatrix) +
                np.expand_dims((self.alpha[:, 2] / 2.0), 1))     
            ## COMPARE LEG RAISED VS GROUND LEG AND CAGE
            self.lambda0Raised = inv_logit(np.expand_dims(self.alpha[:, 0], 1) + 
                (np.expand_dims(self.alpha[:, 1], 1) * sigMatrix) +
                np.expand_dims(self.alpha[:, 2], 1))
            self.lambda0Ground = inv_logit(np.expand_dims(self.alpha[:, 0], 1) + 
                (np.expand_dims(self.alpha[:, 1], 1) * sigMatrix))
        elif (self.params.modelID == 4) | (self.params.modelID == 5):
            self.lambda0Pred = inv_logit(np.expand_dims(self.alpha[:, 0], 1) + 
                (np.expand_dims(self.alpha[:, 1], 1) * sigMatrix))

        self.meanLambda0 = np.mean(self.lambda0Pred, axis = 0)
        if (self.params.modelID < 4) | (self.params.modelID >= 6):
            self.meanLambda0Raised = np.mean(self.lambda0Raised, axis = 0)
            self.meanLambda0Ground = np.mean(self.lambda0Ground, axis = 0)
            self.meanLambda0Cage = np.mean(self.lambda0Cage, axis = 0)
        
        print('ndat', self.ndat, self.nSig)
        print('shp lambda0Pred', np.mean(self.lambda0Pred, axis = 0)[-30:], 
            'sig', sigMatrix[:20, -5:])
        self.lineSpace = [100.0, 200.0, 400.0]
        self.ttSpace = [50.0, 100.0, 200.0]
        self.nTT_Line = [9.0, 5.0, 3.0]
        self.nLines = [9.0, 5.0, 3.0]
        trapDen = [2.53, 0.78, 0.11]
        P.figure(figsize = (16,10))
#        ax = P.gca()
        col = ['k', 'b', 'r']
        lns = []
        ax = P.subplot(1,2,1)
        ## loop thru scenarios
        for scen in range(3):
            self.getTrapLoc(scen)
            self.getRandHRC()
            meanSse = np.mean(self.seMatrix, axis = 0)
            quantsSse = mquantiles(self.seMatrix, prob=[0.025, 0.975], axis = 0)
            lab_scen = str(trapDen[scen]) + ' ' + 'traps' + ' ' + '$ha^{-1}$'
            lns1 = ax.plot(self.sigRange, meanSse, color=col[scen], linewidth = 4,
                label = lab_scen)
            lns2 = ax.plot(self.sigRange, quantsSse[0], color=col[scen], 
                linewidth = 2, ls='dashed')
            lns3 = ax.plot(self.sigRange, quantsSse[1], color=col[scen], 
                linewidth = 2, ls='dashed')
            lns += lns1
        labs = [l.get_label() for l in lns]
        ax.legend(lns, labs, loc = 'upper left')
        minY = 0.0  # np.min(quantsSse[0]) - .01
        maxY = 1.0  # np.max(quantsSse[1]) + .01
        ax.set_ylim([minY, maxY])
        for tick in ax.xaxis.get_major_ticks():
            tick.label.set_fontsize(14)
        for tick in ax.yaxis.get_major_ticks():
            tick.label.set_fontsize(14)
        ax.set_xlabel('$\sigma$', fontsize = 15)
        ax.set_ylabel('Probability of capture', fontsize = 15)

        #######################
        ## MAKE COMPARISON PLOT BETWEEN RAISED AND GROUND TRAPS
        #######################
        if (self.params.modelID <= 3) | (self.params.modelID >= 6):
            ax1 = P.subplot(1,2,2)
            expTerm = np.exp(-(DIST_j**2) / 2.0 / (self.sigRange**2))
#            ## AVERAGE SE ACROSS TRAP TYPES
#            se = (1.0 - np.prod(1.0 - self.meanLambda0[i] * expTerm))
#            self.seMatrix[j, i] = se
            ## RAISED LEG
            seRaisedOneTrap = self.lambda0Raised * expTerm
            ## GROUND
            seGroundOneTrap = self.lambda0Ground * expTerm
            ## CAGE
            seCageOneTrap = self.lambda0Cage * expTerm
                



            meanSseRaised = np.mean(seRaisedOneTrap, axis = 0)
            meanSseGround = np.mean(seGroundOneTrap, axis = 0)
            meanSseCage = np.mean(seCageOneTrap, axis = 0)
            quantsSseRaised = mquantiles(seRaisedOneTrap, prob=[0.025, 0.975], axis = 0)
            quantsSseGround = mquantiles(seGroundOneTrap, prob=[0.025, 0.975], axis = 0)
            quantsSseCage = mquantiles(seCageOneTrap, prob=[0.025, 0.975], axis = 0)
            lab_scen = str(trapDen[0]) + ' ' + 'traps' + ' ' + '$ha^{-1}$'
            lns1 = ax1.plot(self.sigRange, meanSseRaised, color='k', linewidth = 3)
            lns2 = ax1.plot(self.sigRange, quantsSseRaised[0], color='k', 
                linewidth = 2, ls='dashed')
            lns3 = ax1.plot(self.sigRange, quantsSseRaised[1], color='k', 
                linewidth = 2, ls='dashed')
            lns4 = ax1.plot(self.sigRange, meanSseGround, color='r', linewidth = 3)
            lns5 = ax1.plot(self.sigRange, quantsSseGround[0], color='r', 
                linewidth = 2, ls='dashed')
            lns6 = ax1.plot(self.sigRange, quantsSseGround[1], color='r', 
                linewidth = 2, ls='dashed')
            lns7 = ax1.plot(self.sigRange, meanSseCage, color='y', linewidth = 3)
            lns8 = ax1.plot(self.sigRange, quantsSseCage[0], color='y', 
                linewidth = 2, ls='dashed')
            lns9 = ax1.plot(self.sigRange, quantsSseCage[1], color='y', 
                linewidth = 2, ls='dashed')

#            lns += lns1
#           labs = [l.get_label() for l in lns]
#           ax1.legend(lns, labs, loc = 'upper left')
            minY = np.min(quantsSseGround[0]) - .01
            maxY = np.max(quantsSseCage[1]) + .04
            ax1.set_ylim([minY, maxY])
            for tick in ax1.xaxis.get_major_ticks():
                tick.label.set_fontsize(14)
            for tick in ax1.yaxis.get_major_ticks():
                tick.label.set_fontsize(14)
            ax1.set_xlabel('$\sigma$', fontsize = 15)
            ax1.set_ylabel('')
        P.title("One Poss One Trap"+ str(DIST_j))
        figName = os.path.join(self.params.outputDataPath, 'compare_OnePossTrap.png')
#        figName = os.path.join(self.params.outputDataPath, 'onePossSse_3Scenario.png')
#        figName = os.path.join(self.params.outputDataPath, 'onePossSse_100_50_9_9.png')
        P.savefig(figName, format='png', dpi = 600)
#        P.savefig(self.params.onePossSseFname, format='png', dpi = 1200)
        P.show()



    def onePossTrapPCapt(self):
        """
        test the se or pcapt of a single possum across a range of sigmas and g0
        """
        self.meanSig = np.mean(self.siggibbs, axis = 0)
        rad = 100   # np.max(self.meanSig)
        nRand = 200 #20000

        self.sigRange = np.arange(27.0, np.max(self.meanSig), 0.5)

        # lambda for ground leg traps.
        logsig = np.log(self.sigRange)
        self.nSig = len(logsig)
        sigMatrix = np.tile(logsig, self.ndat).reshape(self.ndat, self.nSig)
#        self.lambda0Pred = inv_logit(np.expand_dims(self.alpha[:, 0], 1) + 
#             (np.expand_dims(self.alpha[:, 1], 1) * sigMatrix))         

        self.lambda0Pred = inv_logit(np.expand_dims(self.alpha[:, 0], 1) + 
             (np.expand_dims(self.alpha[:, 1], 1) * sigMatrix) + 
             np.expand_dims(self.alpha[:, 3], 1))         



        self.meanLambda0 = np.mean(self.lambda0Pred, axis = 0)
#        print('nSig', self.nSig)
#        print('shp lambda0Pred', np.mean(self.lambda0Pred, axis = 0)[-30:], 'sig', sigMatrix[:20, -5:])
        meanPCapt = np.zeros(self.nSig)
        pCaptArray = np.zeros((self.nSig, 3))

        # random distances
        randPossDist = np.random.uniform(0.0, rad, nRand)

        # loop thru g0-sig combos
        for i in range(self.nSig):
#            # random distances
#            randPossDist = np.random.uniform(0.0, rad, self.ndat)
#            lambda0Pred = inv_logit(self.alpha[:, 0] + (self.alpha[:, 1] * logsig[i]))         
            # probability of capture


            pCapt = (self.meanLambda0[i] * np.exp(-(randPossDist**2) / 
                2.0 / (self.sigRange[i]**2)))
#            pCapt = lambda0Pred * np.exp(-(randPossDist**2) / 2.0 / (self.sigRange[i]**2))

            pCaptArray[i, 0] = np.mean(pCapt)
            pCaptArray[i, 1:] = mquantiles(pCapt, prob=[0.05, 0.95])


        print('pCaptArray', pCaptArray[:12])


        P.figure(figsize = (13,9))
        ax = P.gca()
        lns1 = ax.plot(self.sigRange, pCaptArray[:, 0], color='k', linewidth = 4)
        lns2 = ax.plot(self.sigRange, pCaptArray[:, 1], color='k', 
            linewidth = 2, ls='dashed')
        lns3 = ax.plot(self.sigRange, pCaptArray[:, 2], color='k', 
            linewidth = 2, ls='dashed')

        minY = np.min(pCaptArray[:, 1]) - .01
        maxY = np.max(pCaptArray[:, 2]) + .01
        ax.set_ylim([minY, maxY])
        for tick in ax.xaxis.get_major_ticks():
            tick.label.set_fontsize(14)
        for tick in ax.yaxis.get_major_ticks():
            tick.label.set_fontsize(14)

        ax.set_xlabel('$\sigma$', fontsize = 17)
        ax.set_ylabel('$Pr(Capture)$', rotation='horizontal', fontsize = 17)
        P.savefig(self.params.onePossTrapPCaptFname, format='png', dpi = 1200)
        P.show()


