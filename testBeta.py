#!/usr/bin/env python

import numpy as np
from scipy import stats
import pylab as P


def testBeta():
    """
    test parameters for beta distribution
    """
    gg = np.arange(.001,.99, .001)
    mode = 0.05
    a = 2     # alpha parameter: Decrease to increase variance
#    b = 8.0     # beta parameter
    b = ((a-1.0)/mode) - a + 2.0
    print("a: ", a, "b: ", b)
    print('mean beta', (a/(a+b)), 'sd =', np.sqrt(a*b / (a+b)**2 / (a+b+1)))
    print('mode', mode) 
    dpdf = stats.beta.pdf(gg, a, b)   #3.9, 191.)  # 1.0, 15.667)         # .095, 37.05)
    P.plot(gg, dpdf)
#        P.ylim(0., 15.0)
#    P.xlim(0.001, .99)        
    P.show()


def getBetaFX(mg0, g0Sd):
    a = mg0 * ((mg0 * (1.0 - mg0)) / g0Sd**2.0 - 1.0)
    b = (1.0 - mg0) * ((mg0 * (1.0 - mg0)) / g0Sd**2.0 - 1.0)
    return (a, b)


def TruncNormFX():
        lo = -5.0
        hi = 5.0
        mu, sigma = 4.98, 0.05
        arr = np.linspace(4.90, 5.0, 1000)
        X = stats.truncnorm((lo - mu) / sigma, (hi - mu) / sigma, 
            loc=mu, scale=sigma)
        Xstar = stats.truncnorm((lo - arr) / sigma, 
            (hi - arr) / sigma, loc=arr, scale=sigma)
        diff = arr - mu
        pnew_old = X.pdf(arr)
        pold_new = Xstar.pdf(mu)
        rand = X.rvs(10000)
        nabove = np.sum(rand > mu)
        nbelow = np.sum(rand <= mu)
        absDiff = np.abs(diff)
        mindiff = np.min(absDiff)
        print('nabove', nabove, 'nbelow', nbelow, 
            'IR at mu', (pold_new/pnew_old)[absDiff == mindiff])
        P.figure()
        P.plot(diff, pold_new/pnew_old)
#        ax = P.hist(rand)
        P.show()
        
        
def TruncNormIR():
        n = 1000
        lo = np.random.uniform(-5.0, -2., n)
        hi = np.random.uniform(4.9, 5., n)
        sigma = 0.05
        Y = np.linspace(3.0, 5.0, n)
        Y[Y>=hi] = hi[Y>=hi] - .05
        nowTNorm = stats.truncnorm((lo - Y) / sigma, (hi - Y) / sigma, 
            loc=Y, scale=sigma)
        Ynew = nowTNorm.rvs(n)
        newTNorm = stats.truncnorm((lo - Ynew) / sigma, 
            (hi - Ynew) / sigma, loc=Ynew, scale=sigma)
        pnew_old = nowTNorm.pdf(Ynew)
        pold_new = newTNorm.pdf(Y)
        IR = pold_new / pnew_old 
        P.figure()
        P.plot(Y, IR)
        P.show()
        


########            Main function
#######
def main():

#    testBeta()
    TruncNormFX()
    TruncNormIR()
    

if __name__ == '__main__':
    main()

