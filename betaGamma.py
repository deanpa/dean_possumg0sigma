#!/usr/bin/env python

import numpy as np
from scipy import stats
import pylab as P
#import starlings
#import params
from scipy.special import gamma
from scipy.special import gammaln
#from starlings import dwrpcauchy
from scipy.stats.mstats import mquantiles

def distFX(x1, y1, x2, y2):
    """
    calc distance matrix between 2 sets of points
    resulting matrix dimensions = (len(x1), len(x2))
    """
    deltaX = np.add.outer(x1, -x2)
    deltaX_sq = deltaX**2
    deltaY = np.add.outer(y1, -y2)
    deltaY_sq = deltaY**2
    dist_2D = np.sqrt(deltaX_sq + deltaY_sq)
    return dist_2D


def logit(x):
    return np.log(x) - np.log(1 - x)

def inv_logit(x):
    return np.exp(x) / (1 + np.exp(x))



def gamma_pdf(xx, shape, scale):
    gampdf = 1.0 / (scale**shape) / gamma(shape) * xx**(shape - 1) * np.exp(-(xx/scale))
    return gampdf

def dwrpcauchy(th, mu, rho):
    """
    wrapped cauchy pdf: direction is mu, and focus is rho.
    mu is real, and rho > 0
    Wikipedia pdf equation
    """
#    e_num = np.exp(-2*rho)
#    e_denom = 2 * np.exp(-rho)
#    sinh_rho = (1 - e_num) / e_denom
#    cosh_rho = (1 + e_num) / e_denom
    sinh_rho = np.sinh(rho)
    cosh_rho = np.cosh(rho)
    cos_mu_th = np.cos(th - mu)
    dwrpc = sinh_rho / 2 / np.pi / (cosh_rho - cos_mu_th)
    return dwrpc



class G0Test(object):
    def __init__(self):

        self.gg = np.arange(.001,.99, .001)
#        self.dpdf = stats.beta.pdf(self.gg, self.params.g0_alpha, self.params.g0_beta)
#        P.plot(self.gg, self.dpdf)
#        P.show()
#        P.cla()
        mode = .03
        a = 1.8 # 1.4 # 1.1
#        b = 3.4     # 84.38889
        b = ((a-1.0)/mode) - a + 2.0
        print("a and b", a, b)
        print('mean beta', (a/(a+b)), 'sd =', np.sqrt(a*b / (a+b)**2 / (a+b+1)))
#        print('mode', mode) 
#        randbeta = np.random.beta(a,b,100000)
#        print('nabove', len(randbeta[randbeta>.04]), 'nbelow', len(randbeta[randbeta < .04]))
        self.dpdf = stats.beta.pdf(self.gg, a, b)   #3.9, 191.)  # 1.0, 15.667)         # .095, 37.05)
        P.plot(self.gg, self.dpdf)
#        P.ylim(0., 15.0)
        P.xlim(0.001, .99)        
#        P.hist(randbeta)
        P.show()
#        P.cla()

    @staticmethod
    def getBetaFX(mg0, g0Sd):
        a = mg0 * ((mg0 * (1.0 - mg0)) / g0Sd**2.0 - 1.0)
        b = (1.0 - mg0) * ((mg0 * (1.0 - mg0)) / g0Sd**2.0 - 1.0)
        return (a, b)

class gammaTest(object):
    def __init__(self):

#        self.params = params
#        print(self.params.g0_alpha)
#        self.rr = np.arange(.005, 1.5, .001)
        self.rr = np.arange(.1, 5.0, .01)
        mode = 1.0
#        sh = 2.0    # 4.33 # 3.0    # 2.8        # 2.8750    # .001
        sc = .12    #.8 # .2 #1.0   # 0.8    # 1000.    #.1    # 1.0/ 5.4  #2.25   #1.6667  #3.    
        sh = (mode / sc) + 1.0
        print('sh =', sh, 'sc =', sc)
#        print('mode', (sh - 1.0) * sc)
#        sc = 3.0     # 1.5     # .66667    
#        rate = 1.0/sc    #0.5  #40.0     #1.1      #.5 #4  #1.      #2
        self.dpdf = gamma_pdf(self.rr, sh, (sc))           # use shape and scale: sc = 1/rate !!!
        randg = np.random.gamma(sh, sc, size = 2000)          # use shape and scale
        print('mean randg', np.mean(randg))
        print("mean", sh*sc, "sd", np.sqrt(sh*sc**2), "mode", (sh-1.)*sc)
#        print("parameter calc mean", sh * sc)
#        print("max LL r", self.rr[self.dpdf==np.max(self.dpdf)], ((sh-1)*sc))
#        print("max pdf", self.dpdf[self.dpdf==np.max(self.dpdf)])
        
        P.figure()
#        P.subplot(1,2,1)
#        P.hist(randg)
#        P.subplot(1,2,2)
        P.plot(self.rr, self.dpdf)
#        P.ylim(0, 0.5)
        P.show()
#        P.cla()

class wrpCTest(object):
    def __init__(self):
#        days = np.arange(52)
        weeks = np.arange(1., 53., 0.1) 
        aa = weeks / 52. * np.pi * 2.0

#        aa = days/364 * 2. 
#        aa = np.arange(0, (np.pi*2), .05)
        mu = 3.0    #-1.5 # 1.4    # np.pi*2
        rho = .5   # .44   # 3.   #.7
        dwrpc = dwrpcauchy(aa, mu, rho)
        st_dwrpc = (dwrpc/np.sum(dwrpc))
#        print(dwrpc)
#        print('st_dwrpc', st_dwrpc)
        P.figure(0)
        ax = P.gca()

        lns1 = ax.plot(weeks, st_dwrpc, linewidth = 3, color = 'k')
        ax.set_xlabel('weeks', fontsize = 16)
        ax.set_ylabel('Probability', fontsize = 16)

#        P.plot(aa, st_dwrpc, color = 'r')
#        P.ylim(0, 1.0)
        P.savefig('wrpC_weeks.png', format='png', dpi = 1000)
        P.show()
        P.cla()

class normalBeta(object):
    def __init__(self):
        M = .4
        V = .01
        dd = logit(np.arange(.001, 1, .001))
        ddpdf = stats.norm.pdf(dd, logit(M), .5)
        l_mu = np.random.normal(logit(M), .05, 2000)
        mu = inv_logit(l_mu)
#        print(np.std(mu))
        P.figure(0)
        P.subplot(1,2,1)
        P.hist(mu)
        P.xlim(0,1.0)
        P.subplot(1,2,2)
        P.plot(dd, ddpdf)
#        P.ylim(0, 2.0)
#        P.show()
#        P.cla()
        

class normalFX(object):
    def __init__(self):
        M = 250.
        S = 60.
        dd = np.arange(30, 470)
        ddpdf = stats.norm.pdf(dd, M, S)
        P.figure(0)
        P.plot(dd, ddpdf)
#        P.ylim(0, 2.0)
        P.show()

        


class Poisson(object):
    def __init__(self):
        dat = 91
        datGammaLn = gammaln(dat + 1)
        ll = 178.002847241 

        lpmf = self.ln_poissonPMF(dat, datGammaLn, ll)

        pois = stats.poisson.logpmf(dat, ll)
#        print('lpmf', lpmf, 'pois', pois)

    def ln_poissonPMF(self, dat, datGammaLn, ll):
        """
        calc log pmf for poission
        """
        ln_ppmf = ((dat * np.log(ll)) - ll) - datGammaLn
        return(ln_ppmf)



class halfNorm(object):
    def __init__(self):
        dat = np.sqrt(50)
#        dat = np.arange(400.0)
        sig = 90.0
        HNpdf = stats.halfnorm.pdf(dat,loc=0.0, scale = sig)
        
        print('hnpdf', np.sum(HNpdf))
        P.figure()
        P.plot(dat, HNpdf)
        P.show()


class multiVNorm(object):
    def __init__(self):
        dat = np.array([5., 5.])
#        dat = np.arange(400.0)
        dat = np.zeros((10, 2))
        sig = 90.0
        sigMat = np.expand_dims(sig, 1)
        ndat = len(dat)
        ndat = 6
        covMat = np.identity(ndat) * sigMat
        meanMat = np.zeros(ndat)
#        MVpdf = stats.multivariate_normal.logpdf(dat, mean = meanMat, cov = covMat)
        rMV1 = np.random.multivariate_normal(meanMat, covMat, size = 1)
        rMV2 = np.random.multivariate_normal(meanMat, covMat, size = 1)
        distmv = np.sqrt(rMV1**2 + rMV2**2)
        print('distmv', np.mean(distmv), np.std(distmv), np.max(distmv))  
        print('covMat', covMat, 'rMV1', rMV1)
#        print('mvpdf', np.sum(MVpdf))
#        P.figure()
#        P.plot(dat, MVpdf)
#        P.hist(rMV1)
#        P.show()

class TruncNormFX(object):
    def __init__(self):
        lo = -5.0
        hi = 5.0
        mu, sigma = 4.98, 0.05
        arr = np.linspace(4.90, 5.0, 1000)
        X = stats.truncnorm((lo - mu) / sigma, (hi - mu) / sigma, 
            loc=mu, scale=sigma)
        Xstar = stats.truncnorm((lo - arr) / sigma, 
            (hi - arr) / sigma, loc=arr, scale=sigma)
        diff = arr - mu
        pnew_old = X.pdf(arr)
        pold_new = Xstar.pdf(mu)
#        print('stat truncnorm', X)
        rand = X.rvs(10000)
        nabove = np.sum(rand > mu)
        nbelow = np.sum(rand <= mu)
        absDiff = np.abs(diff)
        mindiff = np.min(absDiff)
        print('nabove', nabove, 'nbelow', nbelow, 
            'IR at mu', (pnew_old/pold_new)[absDiff == mindiff])
        P.figure()
        P.plot(diff, pnew_old/pold_new)
#        ax = P.hist(rand)
        P.show()
        
        
class TruncNormIR(object):
    def __init__(self):
        n = 1000
        lo = -5.0
        hi = 5.0
        sigma = 0.05
        Y = np.linspace(3.0, 5.0, n)
        nowTNorm = stats.truncnorm((lo - Y) / sigma, (hi - Y) / sigma, 
            loc=Y, scale=sigma)
        Ynew = nowTNorm.rvs(n)
        newTNorm = stats.truncnorm((lo - Ynew) / sigma, 
            (hi - Ynew) / sigma, loc=Ynew, scale=sigma)
        pnew_old = nowTNorm.pdf(Ynew)
        pold_new = newTNorm.pdf(Y)
        IR = pnew_old / pold_new
        P.figure()
        P.plot(Y, IR)
#        ax = P.hist(rand)
        P.show()
        


class circHR(object):
    def __init__(self):
        normMean = 0.0
        normSig = 90.0
        n = 20000
        xnorm = np.random.normal(normMean, normSig, n)
        ynorm = np.random.normal(normMean, normSig, n)

        dat 
        LLxy

        normDist = np.sqrt(xnorm**2 + ynorm**2)
        normSD = np.std(normDist)
        print('normSD', normSD)
        normQuant = mquantiles(normDist, prob=[0.95, 0.99])

        print('norm 95%', normQuant, normQuant/normSig)


        distHN = stats.halfnorm.rvs(loc = 0., scale = normSig, size =n)
        HNSD = np.std(distHN)
        print('HNSD', HNSD)
        HNQuant = mquantiles(distHN, prob=[0.95, 0.99])

        print('HN 95%', HNQuant, HNQuant/normSig)


class WeibullPCapt(object):
    def __init__(self):
        g0 = 0.13
        sig = 90.0
        H = 3.0
        d = 40.
        pe = 0.9
        pIntEnc = .05

        K = np.arange(.01, 5.0, .01)
        Pcij = g0 * np.exp(-(d**2) / 2. / (sig**2))


        print('PCurrentOne', Pcij)

        P4 = 1.0 - (1.0 - Pcij)**4
        print('P4', P4)

        th = 1.0 - (1.0 - Pcij) * (np.exp(-(Pcij * H)**K))
        print('th k= 1', th[K==1.])

        thEI = 1.0 - (1.0 - Pcij) * (1.0 - (pe * pIntEnc))**H
        print('ThEI', thEI)


        P.figure()
        P.plot(K, th)
        P.xlabel('K')
        P.ylabel('Theta')
        P.axhline(y=P4, ls = '--')
        P.axhline(y=Pcij, color = 'k')
        plotFname = 'weibullCaptureDecay.png'
        P.savefig(plotFname, format='png')
        P.show()


class Rescale(object):
    def __init__(self):
        ## trial at rescaling and convert to (0,1) with inv_logit

        stoatLogitSc = [-4.0, 7.0]
        sKRange = [0.5, 8.0]
        rodentDensity = np.arange(.2, 26., .2)
        rDensityRange = [0.2, 26.0]

        ## Run two functions
        self.rescaleProportion(rodentDensity, stoatLogitSc, sKRange)

        self.getStoatK(12., rDensityRange, stoatLogitSc, sKRange)
        
        # run generic function
        newdat = self.rescaleFX(rDensityRange, stoatLogitSc, 12.)
        print('Generic rescale fx', inv_logit(newdat))



    def rescaleFX(self, oldRange, newRange, dat):
        """
        generic function for rescaling data to new value in specified range
        """
        numer = (newRange[1] - newRange[0]) * (dat - oldRange[0])
        denom = oldRange[1] - oldRange[0]
        newDat = (numer / denom) + newRange[0]
        print('newDat', newDat)
        return(newDat)

        

    def rescaleProportion(self, rodentDensity, stoatLogitSc, sKRange):
        """
        rescale variable and take inv_logit
        """
        minRodent = np.min(rodentDensity)
        maxRodent = np.max(rodentDensity)
        (minLogitSc, maxLogitSc) = stoatLogitSc
        numer = (maxLogitSc - minLogitSc) * (rodentDensity - minRodent)
        denom = maxRodent - minRodent
        sLogitDat = (numer / denom) + minLogitSc
        prpStoatK = inv_logit(sLogitDat)
        stoatK = sKRange[0] + (prpStoatK * (sKRange[1] - sKRange[0]))

        rdiff = np.abs(rodentDensity - 8.0)
        rmask = (rdiff == np.min(rdiff))
        horizLine = stoatK[rmask]

        P.Figure()
        P.plot(rodentDensity, stoatK, color ='k')
        P.xlabel('Rodent density $(individuals * ha^{-1})$')
        P.ylabel('Stoat K $(individuals * km^{-2})$')
        P.vlines(x = 8.0, ymin=0.0, ymax = horizLine, 
            linestyles = 'dashed', colors='k')
        P.hlines(y = horizLine, xmin=0.0, xmax = 8.0, 
            linestyles = 'dashed', colors='k')
        plotFname = 'stoatK.png'
        P.savefig(plotFname, format='png', dpi=400)
        P.show()        
        return(stoatK)

    def getStoatK(self, rodentDensity, rDensityRange, stoatLogitSc, sKRange):
        """
        # get stoat K as function of rodent density.
        """
        (minRodent, maxRodent) = rDensityRange
        (minLogitSc, maxLogitSc) = stoatLogitSc
        ## 
        numer = (maxLogitSc - minLogitSc) * (rodentDensity - minRodent)
        denom = maxRodent - minRodent
        sLogitDat = (numer / denom) + minLogitSc
        prpStoatK = inv_logit(sLogitDat)
        stoatK = sKRange[0] + (prpStoatK * (sKRange[1] - sKRange[0]))
        print('stoatK[rod = 12.', stoatK[rodentDensity == 12.], 'prpStoatK', prpStoatK)
        return(stoatK)

class encounterFX(object):
    def __init__(self):
        """
        Trial at relationship between number to toxic rodents and the number
        of stoats that get poisoned
        """
        T_rodent = np.arange(20.*100.)
        pEatEnc = 0.8
        encRate = 0.004
        pEnc = 1.0 - np.exp(-encRate * T_rodent)
        pTStoat = pEnc * pEatEnc
        P.figure()
        P.plot(T_rodent, pTStoat, color = 'k')
        P.xlabel('Toxic Rodents $(T^{[R]}_{k,t})$')
        P.ylabel('$Pr(eat|enc)Pr(enc)$')
        plotFname = 'pToxicStoat.png'
        P.savefig(plotFname, format='png', dpi=400)
        P.show()
        print('max ptstoat', np.max(pTStoat), pTStoat[T_rodent == 300])
#        P.figure()
#        P.plot(nS, r)
#        P.show()

class Emigration(object):
    def __init__(self):
        """
        Explore function and parameters for emigration
        """
        KRatio = np.arange(0.01, 2.0, 0.01)
#        gammaPara = (-4.0, 3.0)
#        PrEmig = inv_logit(gammaPara[0] + gammaPara[1] * KRatio)
        PrEmig = 1.0 - np.exp(-0.60 * KRatio)  
        horizLine = PrEmig[KRatio == 1.0]
        vertLine = 1.0
        print('prob at ratio = 1', horizLine)
        P.figure(figsize=(7,6))
        P.plot(KRatio, PrEmig, color = 'k')
        P.vlines(x = 1.0, ymin=0.0, ymax = horizLine, linestyles = 'dashed', 
            colors='k')
        P.hlines(y = horizLine, xmin=0.0, xmax = 1.0, linestyles = 'dashed', 
            colors='k')

        P.xlabel('Ratio $R_{i,t}$ : $K^{[R]}_{i,t}$')
        P.ylabel('$pEm^{[R]}_{i,t}$', rotation = 0, labelpad = 20)
        plotFname = 'pEmigration.png'
        P.savefig(plotFname, format='png', dpi=600)
        P.show()

        #### show prob of emigrating from i to j with distance and R
        R = np.array([.10, .40, .70, 1.1])
        tau = 1.4       # 1.4
        rr = np.exp(-R*tau)
        dij = np.arange(2200.)
        delt = 1200.0
        ax1 = P.gca()
#        ax1.figure(figsize=(8,6))
        relPr = rr[0] * np.exp(-(dij) / delt)
        lns0 = ax1.plot(dij, relPr, label = 'prop. K = ' + str(R[0]))
        relPr = rr[1] * np.exp(-(dij) / delt)
        lns1 = ax1.plot(dij, relPr, label = 'prop. K = ' + str(R[1]))
        relPr = rr[2] * np.exp(-(dij) / delt)
        lns2 = ax1.plot(dij, relPr, label = 'prop. K = ' + str(R[2]))
        relPr = rr[3] * np.exp(-(dij) / delt)
        lns3 = ax1.plot(dij, relPr, label = 'prop. K = ' + str(R[3]))
        lns = lns0 + lns1 + lns2 + lns3 
        labs = [l.get_label() for l in lns]
        ax1.legend(lns, labs, loc = 'upper right')
        ax1.set_xlabel('Distance cell $i$ to cell $j$')
        ax1.set_ylabel('$RelProbEm^{[R]}_{i,j,t}$')
        plotFname = 'relPrEm.png'
        P.savefig(plotFname, format='png', dpi=600)

        P.show()



class FunctionalResp(object):
    def __init__(self):
        """
        Explore functional responses
        """

        self.minK = 0.25
        self.N = np.arange(0.0, 40., .1)
        self.alpha = .2   
        self.theta = 1.0       
        self.handle = 1.0   #.1428      

#        self.afx(6.75, 40000.)
#        self.hfx(6.75, 40000)

        self.functRespIII()

    def afx(self, c, ns):
        self.alpha = c / ns / (1.0 - (c*self.handle))
        print('alpha', self.alpha, 'handle', self.handle)

    def hfx(self, c, ns):
        self.handle = ((self.alpha * ns) - c) / c / self.alpha / ns
        print('alpha', self.alpha, 'h', self.handle)


    def functRespIII(self):
        """
        calc type III functional respons
        """
        numer = self.alpha * self.N**self.theta
        denom = 1.0 + self.alpha * self.handle * self.N**self.theta                      
        self.C = self.minK + numer / denom
        rdiff = np.abs(self.N - 8.0)
        rmask = (rdiff == np.min(rdiff))
        horizLine = self.C[rmask]
        rdiff2 = np.abs(self.N - 5.0)
        rmask2 = (rdiff2 == np.min(rdiff2))
        horizLine2 = self.C[rmask2]
        print('min and max C', np.min(self.C), np.max(self.C))
        P.figure()
        P.plot(self.N, self.C)
        P.xlabel('Rodent density $(individuals * ha^{-1})$')
        P.ylabel('Stoat K $(individuals * km^{-2})$')
        P.vlines(x = 8.0, ymin=0.0, ymax = horizLine, 
            linestyles = 'dashed', colors='k')
        P.hlines(y = horizLine, xmin=0.0, xmax = 8.0, 
            linestyles = 'dashed', colors='k')
        ### line x == 5.0
        P.vlines(x = 5.0, ymin=0.0, ymax = horizLine2, 
            linestyles = 'dashed', colors='k')
        P.hlines(y = horizLine2, xmin=0.0, xmax = 5.0, 
            linestyles = 'dashed', colors='k')
        plotFname = 'stoatK.png'
#        P.savefig(plotFname, format='png', dpi=400)
  
        P.show()
  
class genLogisticFX(object):
    def __init__(self):
        """
        Explore 'generalised logistic function' for sigmoid shape
        """
        self.a = 1.0    # lower asymptote
        self.k = 12.0     # upper asymptote 6 - 8
        self.b = 0.25   # 0.3     # growth rate .25, .3
        self.shiftR = 1.2
        self.v = .45    # affects near which asymptote we obs greatest growth rate
        self.q = 3.   #self.shiftR + 1.0     # relates to inflection point

        self.N = np.arange(0.0, 26.0, 0.1)

        self.genLogFX()
        self.plotGLF()


    def genLogFX(self):
        """
        calc generalised logistic function
        """
        numer = self.k - self.a
        denom = (1.0 + (self.q * np.exp(-self.b * (self.N - self.shiftR))))**(1.0 / self.v)
        self.C = self.a + (numer / denom)

    def plotGLF(self):
        rdiff = np.abs(self.N - 8.0)
        rmask = (rdiff == np.min(rdiff))
        horizLine = self.C[rmask]
        rdiff2 = np.abs(self.N - 5.0)
        rmask2 = (rdiff2 == np.min(rdiff2))
        horizLine2 = self.C[rmask2]
        rdiff3 = np.abs(self.N - 2.5)
        rmask3 = (rdiff3 == np.min(rdiff3))
        horizLine3 = self.C[rmask3]
        print('stoatK at R = 2.5:', horizLine3,
                'stoatK at R = 5.0:', horizLine2,
                'stoatK at R = 8.0:', horizLine)
        print('min and max C', np.min(self.C), np.max(self.C))
        P.figure()
        P.plot(self.N, self.C, color='k', linewidth = 3.0)
        P.xlabel('Rodent density $(individuals * ha^{-1})$')
        P.ylabel('Stoat K $(individuals * km^{-2})$')
        P.vlines(x = 8.0, ymin=0.0, ymax = horizLine, 
            linestyles = 'dashed', colors='k')
        P.hlines(y = horizLine, xmin=0.0, xmax = 8.0, 
            linestyles = 'dashed', colors='k')
        ### line x == 5.0
        P.vlines(x = 5.0, ymin=0.0, ymax = horizLine2, 
            linestyles = 'dashed', colors='k')
        P.hlines(y = horizLine2, xmin=0.0, xmax = 5.0, 
            linestyles = 'dashed', colors='k')
        ### line x == 2.5
        P.vlines(x = 2.5, ymin=0.0, ymax = horizLine3, 
            linestyles = 'dashed', colors='k')
        P.hlines(y = horizLine3, xmin=0.0, xmax = 2.5, 
            linestyles = 'dashed', colors='k')
        plotFname = 'stoatK.png'
        P.savefig(plotFname, format='png', dpi=400)
        P.show()


 
class KiwiGrowth(object):
    def __init__(self):
        """
        Explore fx for realised kiwi population growth
        """
        self.BRmax = 0.1
        self.BRmin = -0.04
        self.psi = 0.5
        self.S = np.arange(0.0, 8.1, 0.1)
        self.kiwiRealisedGrowth()
        self.plotRealR()
    

    def kiwiRealisedGrowth(self):
        self.realR = self.BRmin + (np.exp(-self.psi*self.S)*(self.BRmax - self.BRmin))
        
    def plotRealR(self):
        rdiff = np.abs(self.realR - 0.0)
        rmask = (rdiff == np.min(rdiff))
        horizLine = 0.0
        vertLine = self.S[rmask]
        print('stoat density with 0 kiwi growth', vertLine)
        P.figure()
        P.plot(self.S, self.realR,  color='k', linewidth = 3.0)
        P.xlabel('Stoat density $(individuals * km^{-2})$')
        P.ylabel('Kiwi realised pop. growth rate $(individuals * year^{-1})$')
        P.vlines(x = vertLine, ymin=self.BRmin, ymax = horizLine, 
            linestyles = 'dashed', colors='k')
        P.hlines(y = horizLine, xmin=0.0, xmax = vertLine, 
            linestyles = 'dashed', colors='k')

        P.savefig('kiwiRealGrowth.png', format='png', dpi=400)
        P.show()

        P.show()


class RickerGrowth(object):
    def __init__(self):
        self.r = 1.2
        self.ps = .2
        self.k = 2.5
        self.nyears = 30
        self.years = np.arange(self.nyears, dtype = int)
        self.narray = np.zeros(self.nyears)
        self.iter = 200
        
        for i in range(200):
            self.n = 2
            for j in range(self.nyears):
                self.n = self.n * np.exp(self.r * (1.0 - (self.n / self.k)))
                self.n = np.round(np.exp(np.random.normal(np.log(self.n + 1.0), self.ps)) - 1.0, 0)
#                self.n = np.random.poisson(self.n)
                self.narray[j] += self.n
        self.narray = self.narray / self.iter

        P.figure()
        P.subplot(2,1,1)
        P.plot(np.arange(self.nyears), self.narray)

        self.growthRate = (self.narray[1:] - self.narray[:-1]) / self.narray[:-1]
        P.subplot(2,1,2)
        P.plot(self.years[1:], self.growthRate)
        P.show()











 
class PredationSurvival(object):
    def __init__(self):
        """
        Explore fx for realised kiwi survivorship with predation
        """
        self.stoat = np.arange(8.)
        self.kiwiSurv = .032
        self.r = 0.1
        self.s = np.exp(-self.kiwiSurv * self.stoat)
        self.s = np.where(self.s <.85, .85, self.s)
        predN = 5.0 * np.exp(self.r * ( 1.0 - (5.0 / 18))) * self.s
        print('example pop growth', predN)

        P.figure()
        P.subplot(2,1,1)
        P.plot(self.stoat, self.s, color = 'k', linewidth = 4)
        P.ylabel('Kiwi predation survivorship')
        
        P.subplot(2,1,2)
        growthrate = np.log(np.exp(self.r) * self.s)
        pArr = np.zeros((len(self.stoat), 3))
        pArr[:,0] = self.stoat
        pArr[:,1] = self.s
        pArr[:,2] = growthrate
        print('growthrate', pArr)
        P.plot(self.stoat, growthrate, color = 'k', linewidth = 4)
        P.xlabel('Stoat density $(individuals * km^{-2})$')
        P.ylabel('Kiwi growth rate')
        P.savefig('kiwiPredationSurvive.png', format='png', dpi=400)
        P.show()


    

class CVTest():
    def __init__(self):

        n = 15
        pbar = np.repeat(.35, n)
    
        p = np.random.uniform(0.0,1.0, n)
        x = np.random.binomial(1, p, n)
        devPred = (np.sum(stats.binom.logpmf(x, 1, p)))
        devMean = (np.sum(stats.binom.logpmf(x, 1, pbar)))
#        r = (np.sum(devPred) / np.sum(devMean))
        r = np.exp(devPred) / np.exp(devMean)

        print('p =', p)
        print('x', x)
        print('pbar =', pbar)
        print('devPred', devPred)
        print('devMean', devMean)

        print('dev diff', devPred - devMean)
        print('r = ', r)



def meanDensity():
    D_Priors = np.array([[3.0, 10.0], [4.0, 0.5], [3.0, 10.0], [18.5, 0.4],
            [4.0, 0.5], [11.0, 0.5], [4.0, 0.5], [6.0, 0.5], [4.33, 0.3], [2.2, 0.5],
            [11.0, 0.5], [23.5, 0.4], [11.0, 0.5], [3.0, 10.0], [18.5, 0.4], 
            [18.5, 0.4], [18.5, 0.4], [18.5, 0.4]])

    D = np.array([0.5, 1.5, 0.5, 7.0, 1.4, 5., 1.5, 2.5, 0.75,
                0.5, 0.75, 9.0, 5.0, .2, 7.0, 7.0, 7.0, 8.0])


    dNormPrior = np.array([[0.5, 0.75], [1.5, 0.5], [0.5, 0.75], [7.0, 0.2],
            [1.4, 0.6], [5.0, 0.2], [1.5, 0.5], [2.5, 0.4], [0.75, 0.75], [0.5, 0.75],
            [0.75, 0.75], [9.0, .2], [5.0, 0.2], [0.2, 1.0], [7.0, .1], 
            [7.0, 0.1], [7.0, 0.1], [8.0, .1]])

    nsite = np.shape(D_Priors)[0]
    for i in range(nsite):
        rand_i = np.random.gamma(D_Priors[i, 0], D_Priors[i, 1], 10000)
        meanden = np.mean(rand_i)
        medianDen = mquantiles(rand_i, prob = 0.5)
        print('i', 'meanDen', meanden, 'median', medianDen)

    for i in range(nsite):
        rand_i = np.exp(np.random.normal(np.log(dNormPrior[i, 0]), dNormPrior[i, 1], 10000))
        meanden = np.mean(rand_i)
        medianDen = mquantiles(rand_i, prob = [0.025, 0.5, 0.975])
        print('i', 'meanDen', meanden, 'median', medianDen)



    
########            Main function
#######
def main():

#    print('Run Main')


#    G0Test()
#    gamtest = gammaTest()
#    wrpCTest()
#    normalBeta()
#    normalFX()
#    Poisson()
#    halfNorm()
#    multiVNorm()
#    circHR()
#    TruncNormFX()
#    TruncNormIR()
#    WeibullPCapt()
#    Rescale()
#    encounterFX()
#    Emigration()

#    FunctionalResp()
#    genLogisticFX()
#    KiwiGrowth()
#    CVTest()
#    PredationSurvival()
#    RickerGrowth()
    meanDensity()

if __name__ == '__main__':
    main()

