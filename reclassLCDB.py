#!/usr/bin/env python

import sys
import numpy as np
from rios import applier

infiles = applier.FilenameAssociations()
infiles.lcdb = sys.argv[1]

outfiles = applier.FilenameAssociations()
outfiles.newArray = sys.argv[2]

def indxLCDB(info, inputs, outputs):
    # empty array to population
    newClassArray = np.zeros_like(inputs.lcdb).astype(np.uint8)
    # specify classes as list
    nonHab = [1, 2, 5, 6, 10, 12, 14, 16, 20, 21, 22, 45, 46, 70]
    farm = [15, 30, 33, 40, 41]
    scrubShrub = [43, 44, 47, 50, 51, 52, 55, 56, 58, 64, 80, 81]
    forest = [54, 68, 69, 71]
    # put into a tuple and loop thru to reclass lcdb
    habClass = (nonHab, farm, scrubShrub, forest)
    for i in range(len(habClass)):
        # find values in lcdb == habClass_i
        mask_i = np.in1d(inputs.lcdb, habClass[i]).reshape(np.shape(inputs.lcdb))
        newClassArray[mask_i] = i + 1
    outputs.newArray = newClassArray


applier.apply(indxLCDB, infiles, outfiles)

##  call on command line:
##  ./reclassLCDB.py possumg0sigma/g0sigmaData/lcdb_Reduced.tif possumg0sigma/g0sigmaData/habitat.img
