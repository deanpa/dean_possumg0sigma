#!/bin/bash

#SBATCH --job-name=g0_mod25
#SBATCH --account=landcare00045 
#SBATCH --mail-type=end
#SBATCH --mail-user=deanpa@protonmail.com
#SBATCH --time=160:00:00

#SBATCH --mem=2000  
#SBATCH --cpus-per-task=1

module load TuiView/1.2.4-gimkl-2018b-Python-3.7.3

./startMod25.py